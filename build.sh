#!/usr/bin/env bash

EMSCRIPTEN_PATH="/home/marek/Workspace/emascripten/emsdk_portable/emscripten/master"

cd cpp
#make -f ./frams/Makefile-SDK
${EMSCRIPTEN_PATH}/emmake make -f ./frams/Makefile-SDK-Emscripten
cp ./frams_sdk.js ../js/framsticks/frams_sdk.js