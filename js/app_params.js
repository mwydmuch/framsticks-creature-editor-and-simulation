var AppParams = (function () {

  var params = {
    "db-url": '',
    "viewer-mode": false,
    "show-axes": true,
    "autorotate": false,
    "show-grid": true,
    "camera-input": true
  };

  function parseBoolean( string ) {
    if ( string.toLowerCase() === 'true' ) {
      return true;
    } else if ( string.toLowerCase() === 'false' ) {
      return false;
    }
  }

  function tryReadURLParams() {

    var paramsString = window.location.search;
    if( paramsString.length > 1 ) {

      paramsString = paramsString.substring( 1 );
      var splited = paramsString.split( '&' );

      for( var i=0; i<splited.length; ++i ) {
        var param = splited[ i ].split( '=' );

        if( param.length == 2 ) {

          var key = param[ 0 ];

          if( typeof params[ key ] === 'boolean' ) {
            var value = parseBoolean( param[ 1 ] );
            if( typeof value !== 'undefined' ) {
              params[ key ] = value;
            }
          }
          else {
            params[ key ] = param[ 1 ];
          }
        }
      }
    }
  }

  function tryReadIframeParams() {

    var frame = window.frameElement;
    if( frame ) {
      for( var key in params ) {
        if( params.hasOwnProperty( key ) && frame.hasAttribute( key ) ) {
          var value = frame.getAttribute( key );

          if( typeof params[ key ] === 'boolean' ) {
            var parsedValue = parseBoolean( value );
            if( typeof parsedValue !== 'undefined' ) {
              params[ key ] = parsedValue;
            }
          }
          else {
            params[ key ] = value;
          }

        }
      }
    }

  }

  function init() {
    tryReadIframeParams()
    tryReadURLParams();

    params[ 'db-url' ] = decodeURIComponent( params[ 'db-url' ] );
  }

  init();
  return params;

}());
