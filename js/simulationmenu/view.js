var SimulationMenuView = (function() {

  var mainMenu = new AppMenu( "menu", "main-menu", [
                                                       {
                                                          header: "File",
                                                          options: [
                                                            { id: "file-load", name: AppResources.get( "fileLoad_label" ), isFileInput: true },
                                                            { id: "file-save", name: AppResources.get( "fileSave_label" )  }
                                                          ]
                                                       },
                                                       {
                                                          header: "Content",
                                                          options: [
                                                            { id: "content-manage", name: AppResources.get( "contentManage_label" ) },
                                                            { id: "content-details", name: AppResources.get( "contentDetails_label" ) }
                                                          ]
                                                       },
                                                       {
                                                          header: "Help",
                                                          options: [ { id: "help-guide",  name: AppResources.get( "helpGuide_label" ) },
                                                                     { id: "help-about",  name: AppResources.get( "helpAbout_label" ) } ]
                                                       },
                                                   ]);

  var aboutDialog = new AboutDialog( 'about-dialog-container' );
  var downloadDialog = new DownloadDialog( 'download-dialog-container' );
  var controller = new SimulationMenuController( mainMenu, aboutDialog, downloadDialog );

})();
