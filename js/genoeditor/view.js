var GenoEditorView = (function () {

  var config = FredConfig;

  var editor = ace.edit("geno-editor");
  editor.setTheme("ace/theme/tomorrow_night_bright");
  editor.getSession().setMode("ace/mode/f0");
  editor.getSession().$validator = Framsticks;
  editor.setFontSize( 16 );
  editor.setShowFoldWidgets( false );
  editor.$blockScrolling = Infinity;

  function resize() {
    editor.resize();
  }

  function handleGenotypeChangeByUser ( changes ) {
    Framsticks.updateGenotype( '//0\n' + editor.getValue() );
  }

  editor.on( 'change', handleGenotypeChangeByUser );

  function loadGenotypeToEditor( data ) {
    editor.off( 'change', handleGenotypeChangeByUser );
    if( data.isNewGenotype ) {
      editor.session.setValue( data.genotype ); //This will automaticly reset undo/redo stack.
    } else {
      editor.setValue( data.genotype );
    }

    editor.clearSelection()
    editor.on( 'change', handleGenotypeChangeByUser );
  }

  function undo() {
    var manager = editor.getSession().getUndoManager();
    if( manager.hasUndo() ) {
      manager.undo();
      editor.clearSelection();
    } else {
      alert( AppResources.get( "undo_error" ) );
    }
  }

  function redo() {
    var manager = editor.getSession().getUndoManager();
    if( manager.hasRedo() ) {
      manager.redo();
      editor.clearSelection();
    } else {
      alert( AppResources.get( "redo_error" ) );
    }
  }

  $( "#geno-editor" ).css( "left", 0 ).css( "top", 0 ).css( "right", 0 ).css( "bottom", 0 );
  resize();

  AppEvents.register( "modelUpdated", loadGenotypeToEditor );
  AppEvents.register( "undo", undo );
  AppEvents.register( "redo", redo );

  return {
    resize: resize
  }

}());
