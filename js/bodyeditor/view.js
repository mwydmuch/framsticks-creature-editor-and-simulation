var BodyEditorView = (function(config, controller, partShapes, jointShapes)
{

  var container = document.getElementById("editor-container");
  var renderer = new THREE.WebGLRenderer( {
    antialias: true,
    devicePixelRatio: window.devicePixelRatio || 1
  } );
  renderer.setSize( window.innerWidth, window.innerHeight );
  container.appendChild( renderer.domElement );
  renderer.domElement.id = config.canvas.id;

  var scene = new Scene(config.simulation, partShapes, jointShapes);
  var camera = new Camera( config.camera, renderer.domElement, AppParams["autorotate"] );

  if( !AppParams['viewer-mode'] ) {

    var partPropertiesEditPanel = new PropertiesEditPanel('parteditor', 'editor-controls-container', [ [
                                                                                                            [ { id: "posLabel", name: AppResources.get( "pos_label" ) }, { id: "posX", name: "x", inputType: "float" },  { id: "posY", name: "y", inputType: "float" }, { id: "posZ", name: "z", inputType: "float" } ],
                                                                                                            [ { id: "rotLabel", name: AppResources.get( "rot_label" ) },  { id: "rotX", name: "x", inputType: "float" }, { id: "rotY", name: "y", inputType: "float" }, { id: "rotZ", name: "z", inputType: "float" } ],
                                                                                                            [ { id: "scaleLabel", name: AppResources.get( "scale_label" ) },  { id: "scaleX", name: "x", inputType: "float" }, { id: "scaleY", name: "y", inputType: "float" }, { id: "scaleZ", name: "z", inputType: "float" } ],
                                                                                                            [ { id: "vcolLabel", name: AppResources.get( "vcol_label" ) }, { id: "vcolR", name: "r", inputType: "float" }, { id: "vcolG", name: "g", inputType: "float" }, { id: "vcolB", name: "b", inputType: "float" } ],
  ],

                                                                                                          [
                                                                                                            [ { id: "shape", name: AppResources.get( "shape_label" ), inputType: "enum", options: partShapes } ]
                                                                                                          ],

                                                                                                          [
                                                                                                            [ { id: "mass", name: AppResources.get( "mass_label" ), inputType: "float" },  { id: "size", name: AppResources.get( "size_label" ), inputType: "float" } ],
                                                                                                            [ { id: "density", name: AppResources.get( "density_label" ), inputType: "float" }, { id: "friction", name: AppResources.get( "friction_label" ), inputType: "float" } ],
                                                                                                            [ { id: "ingest", name: AppResources.get( "ingest_label" ), inputType: "float" }, { id: "assim", name: AppResources.get( "assim_label" ), inputType: "float" } ],
                                                                                                            [ { id: "hollow", name: AppResources.get( "hollow_label" ), inputType: "float" } , { id: "vsize", name:  AppResources.get( "vsize_label" ), inputType: "float" } ],
                                                                                                          ]
                                                                                                      ]);

    var jointPropertiesEditPanel = new PropertiesEditPanel('jointeditor', 'editor-controls-container',  [ [
                                                                                                                [ { id: "posDeltaLabel", name: AppResources.get( "posDelta_label" ) },  { id: "posDeltaX", name: "x", inputType: "float" }, { id: "posDeltaY", name: "y", inputType: "float" }, { id: "posDeltaZ", name: "z", inputType: "float" } ],
                                                                                                                [ { id: "rotDeltaLabel", name: AppResources.get( "rotDelta_label" ) },  { id: "rotDeltaX", name: "x", inputType: "float" }, { id: "rotDeltaY", name: "y", inputType: "float" }, { id: "rotDeltaZ", name: "z", inputType: "float" } ],
                                                                                                                [ { id: "vcolLabel", name: AppResources.get( "vcol_label" ) }, { id: "vcolR", name: "r", inputType: "float" }, { id: "vcolG", name: "g", inputType: "float" }, { id: "vcolB", name: "b", inputType: "float" } ],
                                                                                                             ],

                                                                                                             [
                                                                                                               [ { id: "shape", name: AppResources.get( "shape_label" ), inputType: "enum", options: jointShapes } ]
                                                                                                             ],

                                                                                                             [
                                                                                                               [ { id: "stamina", name: AppResources.get( "stamina_label" ), inputType: "float" },  { id: "stif", name: AppResources.get( "stif_label" ), inputType: "float" } ],
                                                                                                               [ { id: "rotstif", name: AppResources.get( "rotstif_label" ), inputType: "float" },  { id: "usedelta", name:  AppResources.get( "usedelta_label" ), inputType: "boolean" } ],
                                                                                                             ]
                                                                                                           ]);

    var sceneActionMenu = new ActionMenu( "scene-action-menu", "editor-controls-container", [ { id: "addPart", text: AppResources.get( "addPart_label" ) }, { id: "addPartOnGrid", text: AppResources.get( "addPartOnGrid_label" ) } ] );
    var focusedPartActionMenu = new ActionMenu( "focused-part-action-menu", "editor-controls-container", [ { id: "delete", text: AppResources.get( "deletePart_label" ) } ] );
    var focusedJointActionMenu = new ActionMenu( "focused-joint-action-menu", "editor-controls-container", [ { id: "delete", text: AppResources.get( "deleteJoint_label" ) } ] );
    var joinPartsActionMenu = new ActionMenu( "join-parts-action-menu", "editor-controls-container", [ { id: "join", text: AppResources.get( "join_label" ) } ] );

    var cameraMenu = new CameraMenu( "editor-controls-container", "camera-menu", config.canvas.id, camera, scene );

    var simulationMenu = new SimulationMenu( "editor-controls-container", "simulation-menu", config.canvas.id, scene, this );

    var logConsole = new LogConsole( "logconsole-container", "logconsole" );

    var sceneMouseInput = new SceneMouseInput( renderer );

    var transformControl = new THREE.TransformControls( camera.getPerspectiveCamera(), renderer.domElement );
    var bodyElementEditTools = new BodyElementEditTools( "editor-controls-container", scene, transformControl );

    var raycaster = new Raycaster(config.raycaster, scene, camera.getPerspectiveCamera());

    camera.getCameraControl().addEventListener( 'change', function() {
      transformControl.update();
      sceneActionMenu.close();
      focusedPartActionMenu.close();
      focusedJointActionMenu.close();
      joinPartsActionMenu.close();
    } );

  }

  if( !AppParams['camera-input'] ) {
    camera.getCameraControl().enabled = false;
  }

  if( AppParams['show-grid'] ) {

    var grid = new Grid( config.grid.size, config.grid.interval, config.grid.centerLineColor, config.grid.color, AppParams['show-axes'] );
    scene.add( grid );

  }

  var ambientLight = new THREE.AmbientLight( config.light.ambient.color );
  scene.add( ambientLight );

  var topDirectionalLight = new THREE.DirectionalLight( config.light.directional.top.color, config.light.directional.top.intensity );
  topDirectionalLight.position.set( 0, 0, config.light.directional.top.positionZ );
  topDirectionalLight.castShadow = config.light.shadow;
  topDirectionalLight.shadow.mapSize.width = 2048;
  topDirectionalLight.shadow.mapSize.height = 2048;
  scene.add( topDirectionalLight );

  var bottomDirectionalLight = new THREE.DirectionalLight( config.light.directional.bottom.color, config.light.directional.bottom.intensity );
  bottomDirectionalLight.position.set( 0, 0, config.light.directional.bottom.positionZ );
  scene.add( bottomDirectionalLight );

  renderer.shadowMap.type = THREE.PCFSoftShadowMap;
  renderer.shadowMap.enabled = true;

  var resizeInEditorMode;

  if( !AppParams['viewer-mode'] ) {

    resizeInEditorMode = function( width, height, offsetTop ) {

      renderer.context.canvas.width = width;
      renderer.context.canvas.height = height;
      camera.getPerspectiveCamera().aspect = width / height;
      camera.getPerspectiveCamera().updateProjectionMatrix();
      renderer.setSize( width, height );
      renderer.setPixelRatio( window.devicePixelRatio || 1 );

      sceneMouseInput.setOffsetTop( offsetTop );
      partPropertiesEditPanel.resize();
      jointPropertiesEditPanel.resize();
      sceneActionMenu.close();
      focusedPartActionMenu.close();
      focusedJointActionMenu.close();
      joinPartsActionMenu.close();
      cameraMenu.setPosition( height );
      simulationMenu.setPosition(height);

    }

  } else {

    function resizeWindow ( event ) {

      renderer.context.canvas.width = window.innerWidth;
      renderer.context.canvas.height = window.innerHeight;
      camera.getPerspectiveCamera().aspect = window.innerWidth / window.innerHeight;
      camera.getPerspectiveCamera().updateProjectionMatrix();
      renderer.setSize( window.innerWidth, window.innerHeight );
      renderer.setPixelRatio( window.devicePixelRatio || 1 );

    }

    $( window ).resize( resizeWindow );

  }

  function render() {
    camera.getCameraControl().update();
    scene.render(renderer, camera.getPerspectiveCamera());
    simulationMenu.update();

    setTimeout( function() {
      requestAnimationFrame(render);
    }, 1000 / config.fps );
  }

  function clearView() {
    if( !AppParams['viewer-mode'] ) {
      raycaster.clear();
      bodyElementEditTools.close();
      partPropertiesEditPanel.hide();
      jointPropertiesEditPanel.hide();
      sceneActionMenu.close();
      focusedPartActionMenu.close();
      focusedJointActionMenu.close();
      joinPartsActionMenu.close();
    }

  }

  this.clearView = clearView;

  function updateBody( model ) {

    clearView();
    var meshes = scene.updateCreature(model);

    if( model.isNewGenotype ) {
      camera.zoomAll( meshes );
    }

    if( meshes.length > 0 ) {
      cameraMenu.showZoomAllBtn();
      simulationMenu.showBtn();
    } else {
      cameraMenu.hideZoomAllBtn();
      simulationMenu.hideBtn();
    }
  }

  function updateMessagesInfo ( messages ) {
    logConsole.update( messages );
  }

  if( !AppParams['viewer-mode'] ) {

    controller( raycaster
      , scene
      , partPropertiesEditPanel
      , jointPropertiesEditPanel
      , sceneActionMenu
      , focusedPartActionMenu
      , focusedJointActionMenu
      , joinPartsActionMenu
      , bodyElementEditTools
      , sceneMouseInput
      , cameraMenu );

    AppEvents.register( "modelLogReaded", updateMessagesInfo );
  }

  AppEvents.register( "genotypeChanged_bodyData", updateBody );
  render();

  return {
    resizeInEditorMode: resizeInEditorMode
  }

})(FredConfig.editor, FramstickBodyEditorController, Framsticks.getPartShapes(), Framsticks.getJointShapes());
