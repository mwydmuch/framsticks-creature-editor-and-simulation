function PropertiesEditPanel (panelId, containerId, properties) {

  function validateFloat(event) {
    if( event && event.target ) {
      var val = event.target.value.replace( ',', '.' ).trim();
      var numVal = (val) ? Number.parseFloat( val ) : 0;
      if( !isNaN( numVal ) ) {
        event.target.classList.remove( "properties-input-invalid" );
        if ( typeof inputs[ event.target.id ].onvaluechange === "function" && inputs[ event.target.id ].oldNumValue !== numVal) {
          inputs[ event.target.id ].oldNumValue = numVal;
          inputs[ event.target.id ].onvaluechange( numVal );
        }
      } else {
        event.target.classList.add( "properties-input-invalid" );
      }
    }
  }

  function selectionChanged(event) {
    if( event && event.target ) {
      if ( typeof inputs[ event.target.id ].onvaluechange === "function" ) {
        inputs[ event.target.id ].onvaluechange( event.target.value );
      }
    }
  }

  function booleanValueChanged(event) {
    if( event && event.target ) {
      if ( typeof inputs[ event.target.id ].onvaluechange === "function" ) {
        inputs[ event.target.id ].onvaluechange( event.target.checked );
      }
    }
  }

  function handleFloatInputBlur(event) {
    if( event && event.target ) {
        if ( typeof inputs[ event.target.id ].onvalueload === "function" ) {
           loadInputValue( inputs[ event.target.id ] );
        }
    }
  }

  function loadInputValue( input ) {
    input.onvalueload( input );
    input.domElement.classList.remove( "properties-input-invalid" );
  }

  var inputs = [];
  var mainDiv;
  var headerDiv;
  var absolutePosition;
  var isDisplayed = false;

  function init() {
    mainDiv = document.createElement( "div" );
    mainDiv.id = panelId;
    mainDiv.className = "properties-container";

    headerDiv = document.createElement( "div" );
    headerDiv.className = "properties-header";
    mainDiv.appendChild( headerDiv );

    for(var i=0; i<properties.length; ++i) {

      var table = document.createElement( "div" );
      table.className = "properties-datatable";

      for(var j=0; j<properties[ i ].length; ++j) {

        var row = document.createElement( "div" );
        row.className = "properties-datatable-row";
        table.appendChild( row );

        for(var k=0; k<properties[ i ][ j ].length; ++k) {
          var prop = properties[ i ][ j ][ k ];

          var nameCell = document.createElement( "div" );
          nameCell.id = String(panelId) + '-' + String(prop.id);
          nameCell.className = "properties-datatable-cell";
          nameCell.textContent = prop.name;
          row.appendChild( nameCell );

          if( prop.inputType === "float" ) {
            var valueCell = document.createElement( "div" );
            valueCell.className = "properties-datatable-cell";
            row.appendChild( valueCell );

            var input = document.createElement( "input" );
            input.id = nameCell.id + "-input";
            inputs[ input.id ] = { domElement: input,
                                   setEditorValue: function(val) {
                                                    this.oldNumValue = val;
                                                    this.domElement.value = String(val);
                                                  } };

            input.type = "text";
            input.className = "properties-float-input"
            input.oninput = validateFloat;
            input.onblur = handleFloatInputBlur;
            input.onkeydown = function(event) {
              event.stopPropagation();
            }

            valueCell.appendChild( input );
          }
          else if( prop.inputType === "enum" ) {
            var valueCell = document.createElement( "div" );
            valueCell.className = "properties-datatable-cell";
            row.appendChild( valueCell );

            var input = document.createElement( "select" );
            input.id = nameCell.id + "-input";

            inputs[ input.id ] = { domElement: input,
                                   setEditorValue: function(val) {
                                                    this.domElement.value = String(val);
                                                  } };

            if( prop.options ) {
              for(var key in prop.options) {
                var opt = document.createElement( "option" );
                opt.value = prop.options[ key ].value;
                opt.textContent = prop.options[ key ].name;
                input.appendChild( opt );
              }
            }

            input.onchange = selectionChanged;
            input.className = "properties-enum-input"
            valueCell.appendChild( input );
          }
          else if( prop.inputType === "boolean" ) {
            var valueCell = document.createElement( "div" );
            valueCell.classList.add("properties-datatable-cell");
            valueCell.classList.add("checkbox-cell");
            row.appendChild( valueCell );

            var input = document.createElement( "input" );
            input.id = nameCell.id + "-input";
            inputs[ input.id ] = { domElement: input,
                                   setEditorValue: function(val) {
                                                    this.domElement.checked = val;
                                                  }  };

            input.type = "checkbox";
            input.className = "properties-boolean-input"
            input.onchange = booleanValueChanged;

            valueCell.appendChild( input );
          }
        }
      }
      mainDiv.appendChild( table );
    }

    var container = document.getElementById( containerId );
    if(container) {
      container.appendChild( mainDiv );
      hide();
    }
  };

  function reloadInputValue( key ) {
    if( inputs.hasOwnProperty( key ) && typeof inputs[ key ].onvalueload === "function" ) {
      loadInputValue( inputs[ key ] );
    }
  }

  function refreshValues() {
    for( var key in inputs ) {
      reloadInputValue( key );
    }
  }

  function calculatePosition() {
    absolutePosition = $( "#" + String(containerId) ).width() - $( "#"+ String(panelId) ).width();
  }

  function updatePanelPosition() {
    mainDiv.setAttribute( 'style', 'position: absolute; left:' + String(absolutePosition) + "px;" );
  }

  function displayPanel() {
    updatePanelPosition();
    isDisplayed = true;
  }

  function show( text ) {
    resize();

    headerDiv.textContent = text;
    refreshValues();
    displayPanel();
  };

  function hide() {
    mainDiv.setAttribute( 'style', 'display: none;' );
    isDisplayed = false;
  };

  function getInput ( inputId ) {
    return inputs[ inputId ];
  }

  function resize( ) {
    calculatePosition( );
    if( isDisplayed ) {
      updatePanelPosition();
    }
  }

  init();

  return {
    show: show,
    hide: hide,
    getInput: getInput,
    resize: resize,
    reloadInputValue: reloadInputValue
  };

};
