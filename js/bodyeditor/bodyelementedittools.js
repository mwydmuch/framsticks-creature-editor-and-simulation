var BodyElementEditTools = function( containerId, scene, transformControl ) {

  var config = FredConfig.editor.selectedElement;
  var elementWireframe;
  var element;

  var isMouseDown = false;
  var partPositionChangeIsBlocked = false;
  var handlers = {
    ontranslate: null,
    ontranslated: null
   }

  function init( ) {

    setTransformationMode( "translate" );
    transformControl.addEventListener( "change", handleChange );
    transformControl.addEventListener( "mouseDown", handleMouseDown );
    transformControl.addEventListener( "mouseUp", handleMouseUp );
    scene.add( transformControl );

  }

  function setTransformationMode( mode ) {
    transformControl.setMode( mode );
    transformControl.setSpace( "world" );
  }

  function handleChange ( event ) {

      //var mode = transformControl.getMode();
      //if( mode === "translate" ) {

        if( typeof handlers.ontranslate === "function" ) {
          if( isMouseDown ) {
            handlers.ontranslate( element );
          }
        }
    //  }
  }
  function handleMouseDown ( event ) {
    isMouseDown = true;
  }
  function handleMouseUp  ( event ) {
    isMouseDown = false;
    if( typeof handlers.ontranslated === "function" ) {
        handlers.ontranslated( element );
    }
  }

  function open( bodyElement ) {

    if( bodyElement === element ) {
      return;
    }

    close();

    if( bodyElement && bodyElement.mesh ) {
      element = bodyElement;
      elementWireframe = new THREE.EdgesHelper( bodyElement.mesh, config.wireframeColor );
      scene.add( elementWireframe );

      if( bodyElement.type === 'p' ) {
        partPositionChangeIsBlocked = false;
        for(var i=0; i<bodyElement.connectedJoints.length; ++i) {
          if( bodyElement.connectedJoints[i].connectedParts[0] !== bodyElement && bodyElement.connectedJoints[i].data.get_usedelta() ) {
            partPositionChangeIsBlocked = true;
            break;
          }
        }

        if( !partPositionChangeIsBlocked ) {
          transformControl.attach( bodyElement.mesh );
        }
      }

    }

  }

  function close() {

    if( elementWireframe && element ) {
      scene.remove( elementWireframe );
      if( (element.type === 'p' && !partPositionChangeIsBlocked) ) {
        transformControl.detach( element );
      }
    }
    elementWireframe = null;
    element = null;

  }

  function getEventHandlers() {
    return handlers;
  }

  function update() {
    transformControl.update();
  }

  function updateAfterMeshChange() {
    if( elementWireframe ) {
      scene.remove( elementWireframe );
    }

    elementWireframe = new THREE.EdgesHelper( element.mesh, config.wireframeColor );
    scene.add( elementWireframe );

    if( element.type === 'p' && !partPositionChangeIsBlocked ) {
      transformControl.attach( element.mesh );
      transformControl.update();
    }
  }

  function detachTransformControls() {
    if( element && element.mesh ) {
      transformControl.detach( element.mesh );
    }
  }

  init();

  return {
    open: open,
    close: close,
    getEventHandlers: getEventHandlers,
    update: update,
    updateAfterMeshChange: updateAfterMeshChange,
    detachTransformControls: detachTransformControls
  };

};
