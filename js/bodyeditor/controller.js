var FramstickBodyEditorController = function( raycaster
  , scene
  , partPropertiesEditPanel
  , jointPropertiesEditPanel
  , sceneActionMenu
  , focusedPartActionMenu
  , focusedJointActionMenu
  , joinPartsActionMenu
  , bodyElementEditTools
  , sceneMouseInput
  , cameraMenu )
{

  function handleValueChange(value, fields, updateView, updateElementData) {
    var selectedElement = raycaster.getSelectedBodyElementData();
    if( selectedElement ) {
      var elem = selectedElement.data;
      for(var i=0; i<fields.length-1; ++i) {
        elem = elem[ fields[i] ]();
      }
      elem[ fields[fields.length-1] ]( value );
      if( typeof updateElementData === "function" ) {
        updateElementData( selectedElement.data );
      }
      Framsticks.updateModel();
      if( typeof updateView === "function" ) {
        updateView( selectedElement );
      }
    }
  }

  function handleValueLoad(input, fields) {
    var selectedElement = raycaster.getSelectedBodyElementData();
    if( selectedElement ) {
      var value = selectedElement.data;
      for(var i=0; i<fields.length; ++i) {
        if( typeof value[ fields[i] ] === "function" ) {
          value = value[ fields[i] ]();
        }
      }
      input.setEditorValue( value );
    }
  }

  partPropertiesEditPanel.getInput( "parteditor-posX-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_p", "set_x"], function( bodyElement ) {
                                                    Transformations.applyPartPosition(bodyElement, 'x');
                                                  });
    bodyElementEditTools.update();
  }
  partPropertiesEditPanel.getInput( "parteditor-posX-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_p", "get_x"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-posY-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_p", "set_y"], function( bodyElement ) {
                                                    Transformations.applyPartPosition(bodyElement, 'y')
                                                  });
    bodyElementEditTools.update();
  }
  partPropertiesEditPanel.getInput( "parteditor-posY-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_p", "get_y"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-posZ-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_p", "set_z"], function( bodyElement ) {
                                                    Transformations.applyPartPosition(bodyElement, 'z')
                                                  });
    bodyElementEditTools.update();
  }
  partPropertiesEditPanel.getInput( "parteditor-posZ-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_p", "get_z"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-rotX-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_rot", "set_x"], function( selectedElement ) {
                                                      Transformations.applyPartRotation( selectedElement );
                                                    },
                                                    function( selectedElementData ) {
                                                      selectedElementData.setRot( selectedElementData.get_rot() );
                                                    });
  }
  partPropertiesEditPanel.getInput( "parteditor-rotX-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_rot", "get_x"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-rotY-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_rot", "set_y"], function( selectedElement ) {
                                                      Transformations.applyPartRotation( selectedElement );
                                                    },
                                                    function( selectedElementData ) {
                                                      selectedElementData.setRot( selectedElementData.get_rot() );
                                                    });
  }
  partPropertiesEditPanel.getInput( "parteditor-rotY-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_rot", "get_y"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-rotZ-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_rot", "set_z"], function( selectedElement ) {
                                                      Transformations.applyPartRotation( selectedElement );
                                                    },
                                                    function( selectedElementData ) {
                                                      selectedElementData.setRot( selectedElementData.get_rot() );
                                                    });
  }
  partPropertiesEditPanel.getInput( "parteditor-rotZ-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_rot", "get_z"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-scaleX-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_scale", "set_x"], function(bodyElement) {
                                                        Transformations.applyPartScale( bodyElement );
                                                    });
  }
  partPropertiesEditPanel.getInput( "parteditor-scaleX-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_scale", "get_x"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-scaleY-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_scale", "set_y"], function(bodyElement) {
                                                        Transformations.applyPartScale( bodyElement );
                                                    });
  }
  partPropertiesEditPanel.getInput( "parteditor-scaleY-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_scale", "get_y"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-scaleZ-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_scale", "set_z"], function(bodyElement) {
                                                        Transformations.applyPartScale( bodyElement );
                                                    });
  }
  partPropertiesEditPanel.getInput( "parteditor-scaleZ-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_scale", "get_z"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-vcolR-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_vcolor", "set_x"], function(bodyElement) {
                                                        Transformations.applyColor(bodyElement, 'r', bodyElement.data.get_vcolor().get_x());
                                                    });
  }
  partPropertiesEditPanel.getInput( "parteditor-vcolR-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_vcolor", "get_x"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-vcolG-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_vcolor", "set_y"], function(bodyElement) {
                                                        Transformations.applyColor(bodyElement, 'g', bodyElement.data.get_vcolor().get_y());
                                                    });
  }
  partPropertiesEditPanel.getInput( "parteditor-vcolG-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_vcolor", "get_y"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-vcolB-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_vcolor", "set_z"], function(bodyElement) {
                                                        Transformations.applyColor(bodyElement, 'b', bodyElement.data.get_vcolor().get_z());
                                                    });
  }
  partPropertiesEditPanel.getInput( "parteditor-vcolB-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_vcolor", "get_z"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-shape-input" ).onvaluechange = function (value) {
    bodyElementEditTools.detachTransformControls();
    handleValueChange(value, ["set_shape"], function(bodyElement) {
                                              Transformations.applyPartShape( bodyElement );
                                            });
    raycaster.updateAfterMeshChange( );
    bodyElementEditTools.updateAfterMeshChange();
  }
  partPropertiesEditPanel.getInput( "parteditor-shape-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_shape"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-mass-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["set_mass"]);
  }
  partPropertiesEditPanel.getInput( "parteditor-mass-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_mass"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-size-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["set_size"]);
  }
  partPropertiesEditPanel.getInput( "parteditor-size-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_size"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-density-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["set_density"]);
  }
  partPropertiesEditPanel.getInput( "parteditor-density-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_density"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-friction-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["set_friction"]);
  }
  partPropertiesEditPanel.getInput( "parteditor-friction-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_friction"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-ingest-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["set_ingest"]);
  }
  partPropertiesEditPanel.getInput( "parteditor-ingest-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_ingest"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-assim-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["set_assim"]);
  }
  partPropertiesEditPanel.getInput( "parteditor-assim-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_assim"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-hollow-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["set_hollow"]);
  }
  partPropertiesEditPanel.getInput( "parteditor-hollow-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_hollow"]);
  }

  partPropertiesEditPanel.getInput( "parteditor-vsize-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["set_vsize"]);
  }
  partPropertiesEditPanel.getInput( "parteditor-vsize-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_vsize"]);
  }


  jointPropertiesEditPanel.getInput( "jointeditor-posDeltaX-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_d", "set_x"], function(bodyElement) {
                                                    Transformations.applyJointDeltaChange( bodyElement );
                                                  });
    raycaster.updateAfterMeshChange( );
    bodyElementEditTools.updateAfterMeshChange();
  }
  jointPropertiesEditPanel.getInput( "jointeditor-posDeltaX-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_d", "get_x"]);
  }

  jointPropertiesEditPanel.getInput( "jointeditor-posDeltaY-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_d", "set_y"], function(bodyElement) {
                                                    Transformations.applyJointDeltaChange( bodyElement );
                                                 });
    raycaster.updateAfterMeshChange( );
    bodyElementEditTools.updateAfterMeshChange();
  }
  jointPropertiesEditPanel.getInput( "jointeditor-posDeltaY-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_d", "get_y"]);
  }

  jointPropertiesEditPanel.getInput( "jointeditor-posDeltaZ-input" ).onvaluechange = function (value) {
      handleValueChange(value, ["get_d", "set_z"], function(bodyElement) {
                                                      Transformations.applyJointDeltaChange( bodyElement );
                                                    });
      raycaster.updateAfterMeshChange( );
      bodyElementEditTools.updateAfterMeshChange();
  }
  jointPropertiesEditPanel.getInput( "jointeditor-posDeltaZ-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_d", "get_z"]);
  }

  jointPropertiesEditPanel.getInput( "jointeditor-rotDeltaX-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_rot", "set_x"], function(bodyElement) {
                                                    Transformations.applyJointDeltaChange( bodyElement );
                                                  });
    raycaster.updateAfterMeshChange( );
    bodyElementEditTools.updateAfterMeshChange();
  }
  jointPropertiesEditPanel.getInput( "jointeditor-rotDeltaX-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_rot", "get_x"]);
  }

  jointPropertiesEditPanel.getInput( "jointeditor-rotDeltaY-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_rot", "set_y"], function(bodyElement) {
                                                    Transformations.applyJointDeltaChange( bodyElement );
                                                  });
    raycaster.updateAfterMeshChange( );
    bodyElementEditTools.updateAfterMeshChange();
  }
  jointPropertiesEditPanel.getInput( "jointeditor-rotDeltaY-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_rot", "get_y"]);
  }

  jointPropertiesEditPanel.getInput( "jointeditor-rotDeltaZ-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_rot", "set_z"], function(bodyElement) {
                                                    Transformations.applyJointDeltaChange( bodyElement );
                                                  });
    raycaster.updateAfterMeshChange( );
    bodyElementEditTools.updateAfterMeshChange();
  }
  jointPropertiesEditPanel.getInput( "jointeditor-rotDeltaZ-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_rot", "get_z"]);
  }

  jointPropertiesEditPanel.getInput( "jointeditor-vcolR-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_vcolor", "set_x"], function(bodyElement) {
                                                        Transformations.applyColor(bodyElement, 'r', bodyElement.data.get_vcolor().get_x());
                                                    });
  }
  jointPropertiesEditPanel.getInput( "jointeditor-vcolR-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_vcolor", "get_x"]);
  }

  jointPropertiesEditPanel.getInput( "jointeditor-vcolG-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_vcolor", "set_y"], function(bodyElement) {
                                                        Transformations.applyColor(bodyElement, 'g', bodyElement.data.get_vcolor().get_y());
                                                    });
  }
  jointPropertiesEditPanel.getInput( "jointeditor-vcolG-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_vcolor", "get_y"]);
  }

  jointPropertiesEditPanel.getInput( "jointeditor-vcolB-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["get_vcolor", "set_z"], function(bodyElement) {
                                                        Transformations.applyColor(bodyElement, 'b', bodyElement.data.get_vcolor().get_z());
                                                    });
  }
  jointPropertiesEditPanel.getInput( "jointeditor-vcolB-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_vcolor", "get_z"]);
  }

  jointPropertiesEditPanel.getInput( "jointeditor-shape-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_shape"]);
  }
  jointPropertiesEditPanel.getInput( "jointeditor-shape-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["set_shape"], function(bodyElement) {
                                                        Transformations.applyJointShape( bodyElement );
                                                    });

    raycaster.updateAfterMeshChange( );
    bodyElementEditTools.updateAfterMeshChange();
  }

  jointPropertiesEditPanel.getInput( "jointeditor-stamina-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["set_stamina"]);
  }
  jointPropertiesEditPanel.getInput( "jointeditor-stamina-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_stamina"]);
  }

  jointPropertiesEditPanel.getInput( "jointeditor-stif-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["set_stif"]);
  }
  jointPropertiesEditPanel.getInput( "jointeditor-stif-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_stif"]);
  }

  jointPropertiesEditPanel.getInput( "jointeditor-rotstif-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["set_rotstif"]);
  }
  jointPropertiesEditPanel.getInput( "jointeditor-rotstif-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_rotstif"]);
  }

  jointPropertiesEditPanel.getInput( "jointeditor-usedelta-input" ).onvaluechange = function (value) {
    handleValueChange(value, ["set_usedelta"], function(bodyElement) {
                                                  Transformations.applyJointDeltaChange( bodyElement );
                                                });
    raycaster.updateAfterMeshChange( );
    bodyElementEditTools.updateAfterMeshChange();
  }
  jointPropertiesEditPanel.getInput( "jointeditor-usedelta-input" ).onvalueload = function (input) {
    handleValueLoad(input, ["get_usedelta"]);
  }

  sceneActionMenu.getMenuItem( "addPart" ).onitemclick = function( clickRay ) {
    var position = clickRay.at( FredConfig.editor.edit.newPartDistanceFromCamera );
    var newPart = Framsticks.addNewPart( position.x, position.y, position.z );
    if( newPart ) {
      var partMeshFactory = new PartMeshFactory( Framsticks.getPartShapes() );
      scene.add( partMeshFactory.create( newPart ) );
    }
    cameraMenu.showZoomAllBtn();
  }

  sceneActionMenu.getMenuItem( "addPartOnGrid" ).onitemclick = function( clickRay ) {
    var gridHalfSize = FredConfig.editor.grid.size / 2;

    var plane = new THREE.Plane( new THREE.Vector3(0,0,1), 0);
    var position = clickRay.intersectPlane( plane );

    if( !position || position.x < -gridHalfSize || position.x > gridHalfSize || position.y < -gridHalfSize || position.y > gridHalfSize ) {
      alert( AppResources.get( "addPartOnGrid_error" ) );
      return;
    }

    var gridInterval = FredConfig.editor.grid.interval;
    var minValX = Math.floor( position.x / gridInterval ) * gridInterval;
    var maxValX = minValX + gridInterval;
    position.x = ( ((position.x - minValX) / gridInterval) < 0.5) ? minValX : maxValX;

    var minValY = Math.floor( position.y / gridInterval ) * gridInterval;
    var maxValY = minValY + gridInterval;
    position.y = ( ((position.y - minValY) / gridInterval) < 0.5) ? minValY : maxValY;

    position.z = 0.0;

    var newPart = Framsticks.addNewPart( position.x, position.y, position.z );
    if( newPart ) {
      var partMeshFactory = new PartMeshFactory( Framsticks.getPartShapes() );
      scene.add( partMeshFactory.create( newPart ) );
    }

    cameraMenu.showZoomAllBtn();
  }

  function deletePart( element ) {
    Framsticks.removePart( element.data );
    if( Framsticks.getPartCount() == 0 ) {
      cameraMenu.hideZoomAllBtn();
    }

    for(var i=0; i<element.connectedJoints.length; ++i) {
      var joint = element.connectedJoints[i];

      for(var j=0; j<joint.connectedParts.length; ++j) {

        var part = joint.connectedParts[ j ];

        if( part !== element ) {

          var partJoints = [];
          for(var k=0; k<part.connectedJoints.length; ++k) {
            if( part.connectedJoints[ k ] !== joint ) {
              partJoints.push( part.connectedJoints[ k ] );
            }
          }
          part.connectedJoints = partJoints;

        }

      }

      scene.remove( joint.mesh );
    }

    scene.remove( element.mesh );
  }

  focusedPartActionMenu.getMenuItem( "delete" ).onitemclick = function( element ) {
    var result = window.confirm( AppResources.get( "partDelete_question" ) );
    if( result ) {
      deletePart( element );
    }
  }

  function deleteJoint( element ) {
    Framsticks.removeJoint( element.data );
    for( var i=0; i<element.connectedParts.length; ++i ) {
      var newJoints = [];
      var currentPart = element.connectedParts[ i ];
      for( var j=0; j<currentPart.connectedJoints.length; ++j ) {
        if( currentPart.connectedJoints[ j ] !== element ) {
          newJoints.push( currentPart.connectedJoints[ j ] );
        }
      }
      currentPart.connectedJoints = newJoints;
    }
    scene.remove( element.mesh );
  }

  focusedJointActionMenu.getMenuItem( "delete" ).onitemclick = function( element ) {
    var result = window.confirm( AppResources.get( "jointDelete_question" ) );
    if( result ) {
      deleteJoint( element );
    }
  }

  joinPartsActionMenu.getMenuItem( "join" ).onitemclick = function( parts ) {
    var joint = Framsticks.addNewJoint( parts[0].data, parts[1].data );
    if( joint ) {
      var jointMeshFactory = new JointMeshFactory( Framsticks.getJointShapes() );
      scene.add( jointMeshFactory.create( joint, parts ) );
    }
  }

  bodyElementEditTools.getEventHandlers().ontranslate = function ( bodyElement ) {
    bodyElement.data.get_p().set_x( bodyElement.mesh.position.x );
    bodyElement.data.get_p().set_y( bodyElement.mesh.position.y );
    bodyElement.data.get_p().set_z( bodyElement.mesh.position.z );
    Framsticks.updateModel( true );
    Transformations.updateAfterPartTranslation( bodyElement );
  }

  bodyElementEditTools.getEventHandlers().ontranslated = function ( bodyElement ) {
    partPropertiesEditPanel.reloadInputValue( "parteditor-posX-input" );
    partPropertiesEditPanel.reloadInputValue( "parteditor-posY-input" );
    partPropertiesEditPanel.reloadInputValue( "parteditor-posZ-input" );
    Framsticks.publishModelUpdated();
  }

  sceneMouseInput.getEventHandlers().onLeftButtonClick = function ( mouseInput ) {
    if(!scene.isSimulationRunning()) {
      raycaster.update(mouseInput);
      raycaster.selectFocusedElement();

      var selectedElement = raycaster.getSelectedBodyElementData();

      if (selectedElement) {
        if (selectedElement.type === 'p') {
          jointPropertiesEditPanel.hide();
          partPropertiesEditPanel.show(String("Part no. " + selectedElement.data.get_refno() + " properties"));
          bodyElementEditTools.open(selectedElement);
        }
        else if (selectedElement.type === 'j') {
          partPropertiesEditPanel.hide();
          jointPropertiesEditPanel.show(String("Joint no. " + selectedElement.data.get_refno() + " properties"));
          bodyElementEditTools.open(selectedElement);
        }
      }
      else {
        partPropertiesEditPanel.hide();
        jointPropertiesEditPanel.hide();
        bodyElementEditTools.close();
      }
    }

  }

  sceneMouseInput.getEventHandlers().onRightButtonClick = function ( mouseInput ) {
    if(!scene.isSimulationRunning()) {
      raycaster.update( mouseInput );
      var focusedElement = raycaster.getFocusedBodyElementData();
      var selectedElement = raycaster.getSelectedBodyElementData();
      var clickRay = raycaster.getCurrentRay();

      if (!focusedElement && !selectedElement) {
        sceneActionMenu.open(mouseInput.clientX, mouseInput.clientY, clickRay);
      }
      else if (focusedElement && !selectedElement) {
        if (focusedElement.type === 'p') {
          focusedPartActionMenu.open(mouseInput.clientX, mouseInput.clientY, focusedElement);
        } else if (focusedElement.type === 'j') {
          focusedJointActionMenu.open(mouseInput.clientX, mouseInput.clientY, focusedElement);
        }
      }
      else if (focusedElement && selectedElement && (focusedElement !== selectedElement)) {
        if (focusedElement.type === 'p' && selectedElement.type === 'p') {
          if (Framsticks.partsNotJoined(focusedElement.data, selectedElement.data)) {
            joinPartsActionMenu.open(mouseInput.clientX, mouseInput.clientY, [selectedElement, focusedElement]);
          }
        }
      }
    }
  }

  sceneMouseInput.getEventHandlers().onPositionChanged = function ( mouseInput ) {
    if(!scene.isSimulationRunning()) raycaster.update( mouseInput );
  }

  sceneMouseInput.getEventHandlers().onButtonDown = function () {
    if(!scene.isSimulationRunning()) {
      sceneActionMenu.close();
      focusedPartActionMenu.close();
      focusedJointActionMenu.close();
      joinPartsActionMenu.close();
    }
  }

};
