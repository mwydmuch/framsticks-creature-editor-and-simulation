var LogConsole = function ( containerId, id ) {

  var mainDiv;
  var headerDiv;
  var tableDiv;

  var emptyConsoleMsg = AppResources.get( "validModel_label" );

  function init() {

    var container = document.getElementById( containerId );
    if( container ) {
      mainDiv = document.createElement( 'div' );
      mainDiv.id = id;
      mainDiv.className = "logconsole";

      headerDiv = document.createElement( 'div' );
      headerDiv.className = "logconsole-header";
      headerDiv.textContent = emptyConsoleMsg;
      mainDiv.appendChild( headerDiv );

      tableDiv = document.createElement( 'div' );
      tableDiv.className = "logconsole-table";
      mainDiv.appendChild( tableDiv );

      container.appendChild( mainDiv );
    }

  }

  function clear() {
    while ( tableDiv.firstChild ) {
      tableDiv.removeChild( tableDiv.firstChild );
    }
    headerDiv.textContent = emptyConsoleMsg;
  }

  function getNewCellDiv( className ) {
    var cell = document.createElement( 'div' );
    cell.classList.add( "logconsole-row-cell" );
    cell.classList.add( className );
    return cell;
  }

  function getNewCellTextContentDiv( text ) {
    var content = document.createElement( 'div' );
    content.classList.add( "logconsole-rowcell-textcontent" );
    content.textContent = text;
    return content;
  }

  function getNewErrorRowDiv( errorData ) {
    var errorRow = document.createElement( 'div' );
    errorRow.classList.add( "logconsole-row" );


    var typeCell = getNewCellDiv( "logconsole-typecell" );
    var typeCellTextContent = getNewCellTextContentDiv( errorData.sdkType );
    typeCell.appendChild( typeCellTextContent );
    typeCell.classList.add( "rowtype-"+errorData.type );
    errorRow.appendChild( typeCell );

    var nameCell = getNewCellDiv( "logconsole-namecell" );
    var nameCellTextContent = getNewCellTextContentDiv( errorData.shortText );
    nameCell.appendChild( nameCellTextContent );
    errorRow.appendChild( nameCell );

    return errorRow;
  }

  function update( messages ) {

    clear();

    for( var i=0; i<messages.length; ++i ) {

      var errorRow = getNewErrorRowDiv( messages[ i ] );
      tableDiv.appendChild( errorRow );

    }

    var issuesCount = messages.length;
    if( issuesCount > 0 ) {
      headerDiv.textContent = AppResources.get( "invalidModel_label", [ issuesCount ] );
    }

  }

  init();

  return {
    update: update
  };

};
