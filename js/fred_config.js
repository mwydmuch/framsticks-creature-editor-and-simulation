var FredConfig = (function() {

  return {

    editor : {

      fps: 60,

      canvas: {
        id: "editor-canvas"
      },

      grid: {
        size: 50,
        interval: 1,
        color: 0x202020,
        centerLineColor: 0x282828
      },

      axis: {
        size: 50
      },

      camera: {
        fieldOfView: 75,
        near: 0.1,
        far: 1000,
        autoRotateSpeed: 2,
        defaultSettings: {
          target: {
            x: 0,
            y: 0,
            z: 0
          },
          position: {
            x: 0.36,
            y: 0.36,
            z: 0.36
          },
        }
      },

      light: {
        shadow: true,
        ambient: {
          color: 0x303030
        },
        directional: {
          top: {
            color: 0x808080,
            intensity: 1.0,
            positionZ: 20
          },
          bottom: {
            color: 0x808080,
            intensity: 0.5,
            positionZ: -20
          }
        }
      },

      raycaster: {
        focusedElement: {
          color: 0x900090
        }
      },

      selectedElement: {
        wireframeColor: 0xFFFF00
      },

      geometry: {
        part: {
          density: 0.5,
          defaultShape: {
            radius: 0.2,
            segments: 16,
          },
          ellipsoidShape: {
            radius: 0.2,
            segments: 32
          },
          boxShape:{
            size: 0.2
          }
        },
        joint: {
          density: 0.5,
          cylinderShape: {
            radius: 0.1,
            radiusSegments: 10,
            isTransparent: false,
            opacity: 1
          },
          linkShape: {
            radius: 0.04,
            radiusSegments: 10,
            isTransparent: true,
            opacity: 0.2
          }
        }
      },

      edit: {
        newPartDistanceFromCamera: 4.5
      },

      simulation: {
        gravity: {
          x: 0,
          y: 0,
          z: -10
        },
        offset: {
          x: 0,
          y: 0,
          z: 1
        },
        timeStep: 1.0/60.0,
        stabilityThreshold: 0.001,
        startingSteps: 10,
        timeout: 30.0
      }

    },

    genoEditor : {
      initialWidthRatio: 0.3
    },

    fileHandler: {
      maxSizeInMB: 1,
      defaultFileName: "framsticks.gen"
    },

    startupFile: {
      name: "New creature",
      genotype: "//0\np:",
      info: ""
    },

    neuroViewer: {
      layoutType: 2,
      canvas: {
        id: "neuro-viewer-canvas",
        drawMarginInPx: 20,
        font: "14px Lucida Grande, sans-serif",
        strokeStyle: '#DEDEDE'
      },
    },

    population:{
      maxPopulation: 10
    },

    SDK: {
      validation: {
        minimalLogLevel: -1
      }
    }

  };

}());
