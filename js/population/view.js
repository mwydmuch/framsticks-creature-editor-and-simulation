var PopulationView = (function () {

  // DOM
  var populationListDiv;
  var container = document.getElementById("population-container");

  // testing
  var currentTested = 0;
  var testing = false;
  var newPopulationAfterTesting = false;

  var handlers = {
    onAddNewCreature: null,
    onDeleteCreature: null,
    onOpenCreature: null
  };

  function testNext(){
    refreshData();
    AppEvents.publish("stopSimulation");
    if(testing) {
      ++currentTested;
      var creatures = Framsticks.getAllCreatures().creatures;
      if (currentTested < creatures.length){
        Framsticks.selectCreature(creatures[currentTested].id);
        AppEvents.publish("startSimulation");
      }
      else{
        testing = false;
        if(newPopulationAfterTesting){
          newPopulationAfterTesting = false;
          genNewPopulation();
        }
      }
    }
  }

  function testPopulation() {
    testing = true;
    currentTested = -1;
    testNext();
  }

  function genNewPopulation(){
    var creatures = Framsticks.getAllCreatures().creatures;
    var allTested = true;
    for( var i=0; i < creatures.length; ++i ) {
      if(typeof creatures[i]._score === "undefined"){
        allTested = false;
        break;
      }
    }

    if(!allTested){
      newPopulationAfterTesting = true;
      testPopulation();
    }
    else {
      Framsticks.crossOverPopulation();
      Framsticks.mutatePopulation();
      newPopulationAfterTesting = false;
    }
  }

  function clear() {
    while ( populationListDiv.firstChild ) {
      populationListDiv.removeChild( populationListDiv.firstChild );
    }
  }

  function init(){

    var testBtnContainer = document.createElement( 'div' );
    testBtnContainer.id = "population-test-btn";
    container.appendChild( testBtnContainer );

    $( '#population-test-btn' ).button( {
      icons: { primary: "ui-icon-seek-next" },
      label: AppResources.get( "populationTestBtn_label" ) }
    ).click( testPopulation );

    var newGenBtnContainer = document.createElement( 'div' );
    newGenBtnContainer.id = "population-new-gen-btn";
    container.appendChild( newGenBtnContainer );

    $( '#population-new-gen-btn' ).button( {
      icons: { primary: "ui-icon-shuffle" },
      label: AppResources.get( "populationNewGenBtn_label" ) }
    ).click( genNewPopulation );

    // var addBtnContainer = document.createElement( 'div' );
    // addBtnContainer.id = "population-add-btn";
    // container.appendChild( addBtnContainer );
    //
    // $( '#population-add-btn' ).button( {
    //   icons: { primary: "ui-icon-plus" },
    //   label: AppResources.get( "add_label" ) }
    // ).click( addNewCreature );

    populationListDiv = document.createElement( 'div' );
    populationListDiv.id = "populationlist";
    container.appendChild( populationListDiv );

  }

  function parseCreatureID( event ) {
    if( event && event.currentTarget && event.currentTarget.id ) {
      var result = event.currentTarget.id.split( '-' );
      if( result.length > 1 ) {
        var result = parseInt( result[ result.length-1 ] );
        if( !isNaN(result) && result > -1 ) {
          return result;
        }
      }
    }
  }

  function openCreature( event ) {
    var creatureID = parseCreatureID( event );
    if( typeof handlers.onOpenCreature === 'function' && creatureID ) {
      handlers.onOpenCreature( creatureID );
    }
  }

  function getNewCellDiv( className ) {
    var cell = document.createElement( 'div' );
    cell.classList.add( "populationlist-row-cell" );
    cell.classList.add( className );
    return cell;
  }

  function getNewCellTextContentDiv( text ) {
    var content = document.createElement( 'div' );
    content.classList.add( "populationlist-textcontent" );
    content.textContent = text;
    return content;
  }

  function getNewRow(id, name, genotype, score){
    var creatureRow = document.createElement( 'div' );
    creatureRow.classList.add( "populationlist-row");

    var rowNumCell = getNewCellDiv( "populationlist-rownumcell" );
    var rowNumContent = getNewCellTextContentDiv( id );
    rowNumCell.appendChild( rowNumContent );
    creatureRow.appendChild( rowNumCell );

    var nameCell = getNewCellDiv( "populationlist-namecell" );
    var nameContent = getNewCellTextContentDiv( name );
    nameCell.appendChild( nameContent );
    creatureRow.appendChild( nameCell );

    var genoCell = getNewCellDiv( "populationlist-genocell" );
    var genoContent = getNewCellTextContentDiv( genotype );
    genoCell.appendChild( genoContent );
    creatureRow.appendChild( genoCell );

    var scoreCell = getNewCellDiv( "populationlist-scorecell" );
    var scoreContent = getNewCellTextContentDiv( score );
    scoreCell.appendChild( scoreContent );
    creatureRow.appendChild( scoreCell );

    return creatureRow;
  }

  function getCreatureRow(creature, currentRow) {

    var creatureRow = getNewRow(
        creature.id,
        creature.name,
        creature._genotype,
        typeof creature._score !== "undefined" ? creature._score.toFixed(3) : "-");

    if(currentRow) creatureRow.classList.add( "current");
    creatureRow.id = "populationlist-select-" + String( creature.id );
    populationListDiv.appendChild( creatureRow );

    $( '#'+creatureRow.id ).click( openCreature );

    return creatureRow;
  }

  function refreshData() {
    clear();
    var creaturesData = Framsticks.getAllCreatures();
    var currentCreature = creaturesData.currentCreature;
    var creatures = creaturesData.creatures;

    var headerRow = getNewRow(
        AppResources.get( "populationCreatureId_label" ),
        AppResources.get( "populationCreatureName_label" ),
        AppResources.get( "populationCreatureGeno_label" ),
        AppResources.get( "populationCreatureScore_label" ));
    populationListDiv.appendChild( headerRow );

    for( var i=0; i < creatures.length; ++i ) {
      var rowNum = i+1;

      var creatureRow = getCreatureRow( creatures[ i ], currentCreature && currentCreature.id == creatures[ i ].id);
      populationListDiv.appendChild( creatureRow );

    }
  }

  this.refreshData = refreshData;

  init();

  AppEvents.register( "creaturesChanged", refreshData );
  AppEvents.register( "simulationEnded", testNext );
  var controller = new PopulationController( this, handlers);

}());
