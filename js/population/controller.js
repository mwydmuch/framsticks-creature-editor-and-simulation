var PopulationController = function ( view, handlers ) {

  handlers.onAddNewCreature = function () {
    var newCreature = Framsticks.addNewGenotype();
    view.refreshData();
  }

  handlers.onOpenCreature = function ( creatureID ) {
    Framsticks.selectCreature( creatureID );
  }

  handlers.onDeleteCreature = function ( creatureID ) {
    var confirmed = confirm( AppResources.get( "creatureDelete_question" ) );
    if( confirmed ) {
      Framsticks.deleteCreature( creatureID );
      view.refreshData();
    }
  }

};
