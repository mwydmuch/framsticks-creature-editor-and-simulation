var Framsticks = (function () {

  var gcm = new Module.DefaultGenoConvManager();
  gcm.addDefaultConverters();
  var dummyGeno = new Module.Geno().useConverters(gcm);
  Module.destroy(dummyGeno);

  var validators = new Module.Validators();
  var dummyGeno = new Module.Geno().useValidators(validators);
  Module.destroy(dummyGeno);
  var modelValidator = new Module.ModelGenoValidator();
  validators.append(modelValidator);

  var framsFileSystem = new Module.StdioFileSystem_autoselect();
  var saveFileHelper = new Module.SaveFileHelper();
  var neuroLayoutFunctionHelper = new Module.NNLayoutFunctionHelper();

  var currentCreature;
  var creatures = [];

  var model;
  var neuroLayoutModel;

  var logger = new FramsticksSDKLogParser( );
  logger.setModel( model );

  function readLogAndGetGenotypeMessages() {
    var result = logger.readLogMessages();

    if( result ) {
      AppEvents.publish( "modelLogReaded", result.allMessages );
      return result.genoLineMsgs;
    }
  }

  function loadModel( genotype ) {
    var stringObj = new Module.SString();
    stringObj.set(genotype);

    var genoObj = new Module.Geno(stringObj);
    Module.destroy(stringObj);

    if (model) {
      Module.destroy(model);
    }
    model = new Module.Model(genoObj, true);
    logger.setModel( model );

    updateNeuroLayoutModel();

    model.open();
    Module.destroy(genoObj);

    AppEvents.publish( "creaturesChanged" );
  }

  function updateNeuroLayoutModel() {
    if( neuroLayoutModel ) {
      Module.destroy( neuroLayoutModel );
    }
    neuroLayoutModel = new Module.NNLayoutState_Model_Fred( model );
    neuroLayoutFunctionHelper.doLayout( FredConfig.neuroViewer.layoutType , neuroLayoutModel );
  }

  function publishBrainDataChange ( eventName ) {
    AppEvents.publish( eventName, {
      getElements: function () {
        return neuroLayoutModel.GetElements();
      },
      getValueXYWH: function ( i ) {
        return neuroLayoutModel.GetValueXYWH( i );
      },
      getNeuro: function ( i ) {
        return model.getNeuro( i );
      }
    } );
    AppEvents.publish( "creaturesChanged" );
  }

  function publishGenotypeChanged( isNew ) {

    AppEvents.publish("genotypeChanged_bodyData", {
      getPartCount: function () {
        return model.getPartCount()
      },
      getJointCount: function () {
        return model.getJointCount()
      },
      getPart: function (i) {
        return model.getPart(i)
      },
      getJoint: function (i) {
        return model.getJoint(i)
      },
      isNewGenotype: isNew
    });

    publishBrainDataChange( "genotypeChanged_brainData" );
    AppEvents.publish( "creaturesChanged" );
  }

  function loadCreatureAsNewFile( creatureData ) {

    if (typeof creatureData.genotype === "string") {
      loadModel( creatureData.genotype );

      currentCreature = {
        id: 1,
        name: creatureData.name,
        info: creatureData.info,
        genotype: creatureData.genotype,
        _genotype: creatureData.genotype,
      };
      creatures = [ currentCreature ];

      publishGenotypeChanged( true );
      publishModelUpdated( true );
    }

  }

  function loadModelFromGenotype( genotype, isNew ) {
    if (typeof genotype === "string") {
      loadModel( genotype );
      publishGenotypeChanged( isNew );
    }
  };

  function loadCreaturesFromFileData(fileData, selectCreature) {
    selectCreature = (typeof selectCreature !== 'undefined') ?  selectCreature : true;
    FS.writeFile('/current.gen', fileData);
    var loadedGenotypes = [];

    var loader = new Module.MiniGenotypeLoader('/current.gen');
    var geno = loader.loadNextGenotype();

    var nextID = 1;
    while(geno && geno.ptr != 0) {
      loadedGenotypes.push({
        id: nextID++,
        name: geno.get_name().c_str(),
        info: geno.get_info().c_str(),
        genotype: geno.get_genotype().c_str(),
        _genotype: geno.get_genotype().c_str(),
      });
      geno = loader.loadNextGenotype();
    }

    Module.destroy(loader);
    AppEvents.publish( "creaturesChanged" );

    if( loadedGenotypes.length > 0 ) {
      creatures = loadedGenotypes;

      if( loadedGenotypes.length > 1 && selectCreature) {
        currentCreature = null;
        AppEvents.publish( "openCreatureListDialog" );
        return true;
      }

      currentCreature = loadedGenotypes[0];
      loadModelFromGenotype( loadedGenotypes[0].genotype, true );
      publishModelUpdated( true );
      return true;
    }
    return false;
  };

  function updateModel( preventEventPublishing ) {

    model.close();
    model.open();

    logger.saveSDKLog();

    if( !preventEventPublishing ) {
      publishModelUpdated( false );
    }

  };

  function publishModelUpdated( isNew ) {

    AppEvents.publish("modelUpdated", {
      genotype: getModelGenotype(),
      isNewGenotype: isNew
    });

  };

  function getModelGenotype() {

    var geno = new Module.Geno();
    model.makeGeno( geno );
    var result = geno.getGenes().c_str();
    Module.destroy( geno );
    return result;

  }

  function getPartShapes() {
    var shapes = [ ];
    shapes["SHAPE_BALL_AND_STICK"] = { name: "Ball & Stick", value: Module.Part["SHAPE_BALL_AND_STICK"] };
    shapes["SHAPE_ELLIPSOID"] = { name: "Elipsoid", value: Module.Part["SHAPE_ELLIPSOID"] };
    shapes["SHAPE_CUBOID"] = { name: "Cuboid", value: Module.Part["SHAPE_CUBOID"] };
    shapes["SHAPE_CYLINDER"] = { name: "Cylinder", value: Module.Part["SHAPE_CYLINDER"] };
    return shapes;
  }

  function getJointShapes() {
    var shapes = [ ];
    shapes["SHAPE_BALL_AND_STICK"] = { name: "Ball & Stick", value: Module.Joint["SHAPE_BALL_AND_STICK"] };
    shapes["SHAPE_FIXED"] = { name: "Fixed", value: Module.Joint["SHAPE_FIXED"] };
    return shapes;
  }

  function getNeuroClassHints() {
    var hints = [ ];
    hints["Invisible"] = { name: "Invisible", value: Module.NeuroClass["Invisible"] };
    hints["DontShowClass"] = { name: "DontShowClass", value: Module.NeuroClass["DontShowClass"] };
    hints["AtFirstPart"] = { name: "AtFirstPart", value: Module.NeuroClass["AtFirstPart"] };
    hints["AtSecondPart"] = { name: "AtSecondPart", value: Module.NeuroClass["AtSecondPart"] };
    hints["EffectorClass"] = { name: "EffectorClass", value: Module.NeuroClass["EffectorClass"] };
    hints["ReceptorClass"] = { name: "ReceptorClass", value: Module.NeuroClass["ReceptorClass"] };
    hints["V1BendMuscle"] = { name: "V1BendMuscle", value: Module.NeuroClass["V1BendMuscle"] };
    hints["V1RotMuscle"] = { name: "V1RotMuscle", value: Module.NeuroClass["V1RotMuscle"] };
    hints["LinearMuscle"] = { name: "LinearMuscle", value: Module.NeuroClass["LinearMuscle"] };
    return hints;
  }

  function partsNotJoined( part1, part2 ) {
    return ( model.findJoint( part1, part2 ) < 0 ) && ( model.findJoint( part2, part1 ) < 0 );
  }

  function addNewPart( posX, posY, posZ )  {
    var part;

    part = model.addNewPart( Module.Part["SHAPE_BALL_AND_STICK"] );
    if( part ) {
      part.get_p().set_x( posX );
      part.get_p().set_y( posY );
      part.get_p().set_z( posZ );
    }

    updateModel();
    return part;
  }

  function addNewJoint( part1, part2 ) {
    var joint = model.addNewJoint( part1, part2, Module.Joint["SHAPE_BALL_AND_STICK"] );
    updateModel();
    return joint;
  }

  function removePart( part ) {
    if( part ) {
      model.removePart( part.get_refno() );
      updateModel();
      publishBrainDataChange( "bodyElementRemoved_brainData" );
    }
  }

  function removeJoint( joint ) {
    if( joint ) {
      model.removeJoint( joint.get_refno() );
      updateModel();
      publishBrainDataChange( "bodyElementRemoved_brainData" );
    }
  }

  function getPartCount( ) {
    if( model ) {
      return model.getPartCount();
    }
  }

  function saveCurrentGenotype() {
    if( currentCreature ) {
      currentCreature.genotype = "//0\n" + getModelGenotype();
    }
  }

  function generateFileData() {

    saveCurrentGenotype();

    var f = saveFileHelper.Vfopen("/current.gen", "w");
    var miniGeno = new Module.MiniGenotype();
    var param = new Module.Param(saveFileHelper.getMinigenotype_paramtab(), miniGeno);
    miniGeno.clear();

    for (var i = 0; i < creatures.length; i++)
    {
      miniGeno.get_name().set( creatures[i].name );
      miniGeno.get_info().set( creatures[i].info );
      miniGeno.get_genotype().set( creatures[i].genotype );
      param.save(f, "org");
    }

    Module.destroy(f);
    Module.destroy(param);
    Module.destroy(miniGeno);

    return FS.readFile('/current.gen', { encoding: 'utf8' } );

  }

  function getAllCreatures() {
    return { creatures: creatures,
             currentCreature: currentCreature };
  }

  function addNewGenotype() {
    var id = 0;
    for(var i=0; i<creatures.length; ++i) {
      id = Math.max(creatures[i].id, id);
    }
    id++;

    var newGenotype = {
      id: id,
      name: FredConfig.startupFile.name + " " + String(id),
      genotype: FredConfig.startupFile.genotype,
      _genotype: FredConfig.startupFile.genotype,
      info: FredConfig.startupFile.info
    };

    creatures.push( newGenotype );
    AppEvents.publish( "creaturesChanged" );

    return newGenotype;
  }

  function selectCreature( id ) {
    saveCurrentGenotype();

    for(var i=0; i<creatures.length; ++i) {
      if( creatures[i].id == id ) {
        selectCurrentCreature( creatures[i] );
      }
    }
  }

  function selectCurrentCreature( creature ) {
    currentCreature = creature;
    loadModelFromGenotype( creature.genotype, true );
    publishModelUpdated( true );
  }

  function deleteCreature( id ) {

    var newCreaturesList = [];

    for(var i=0; i<creatures.length; ++i) {

      if( creatures[i].id !== id ) {
        newCreaturesList.push( creatures[i] );
      } else if ( currentCreature && currentCreature.id == creatures[i].id ) {
        currentCreature = null;
      }

    }

    creatures = newCreaturesList;
    AppEvents.publish( "creaturesChanged" );
  }

  function updateGenotype( genotype ) {
    loadModelFromGenotype( genotype, false );
  }

  function getCurrentCreature() {
    return currentCreature;
  }

  function getGenotypePrefix(genotype){
    return genotype.substring(0, 5);
  }

  function removeGenotypePrefix(genotype){
    return genotype.substring(5);
  }

  function mutateGenotype( genotype ){

    var genoPrefix = getGenotypePrefix(genotype);
    var _genotype = removeGenotypePrefix(genotype);

    var genoOper = new Module.Geno_f4();
    var genoOperHelper = new Module.GenoOperatorsHelper(genoOper);

    if(genoPrefix === "/*4*/") {
      var status = genoOperHelper.mutate(_genotype);
      genotype = genoPrefix + genoOperHelper.getLastMutateGeno().c_str();
    }

    Module.destroy(genoOper);
    Module.destroy(genoOperHelper);

    return genotype;
  }

  function mutatePopulation(){
    var creaturesLength = creatures.length;
    //var toRemove = [];
    for(var i=0; i < creaturesLength; ++i) {
      creatures[i]._genotype = mutateGenotype(creatures[i]._genotype);
      creatures[i].genotype = creatures[i]._genotype;
      //toRemove.push(creatures[i].id);
      //addNewGenotype();
      //creatures[creatures.length - 1]._genotype = mutateGenotype(creatures[i]._genotype);
      //creatures[creatures.length - 1].genotype = creatures[creatures.length - 1]._genotype;
    }

    // for(var i=0; i < toRemove.length; ++i) {
    //   deleteCreature(toRemove[i]);
    // }

    selectCurrentCreature(creatures[0]);
    AppEvents.publish( "creaturesChanged" );
  }

  function crossOverCreatures (creature1, creature2) {
    var genoOper = new Module.Geno_f4();
    var genoOperHelper = new Module.GenoOperatorsHelper(genoOper);

    var geno1Pref = getGenotypePrefix(creature1._genotype);
    var geno2Pref = getGenotypePrefix(creature2._genotype);
    var _geno1 = removeGenotypePrefix(creature1._genotype);
    var _geno2 = removeGenotypePrefix(creature2._genotype);

    if(geno1Pref === geno2Pref && geno1Pref === "/*4*/"){
      var status = genoOperHelper.crossOver(_geno1, _geno2);

      if(status == 0) {
        addNewGenotype();
        creatures[creatures.length - 1]._genotype = geno1Pref + genoOperHelper.getLastCrossGeno1().c_str();
        creatures[creatures.length - 1].genotype = creatures[creatures.length - 1]._genotype;

        addNewGenotype();
        creatures[creatures.length - 1]._genotype = geno2Pref + genoOperHelper.getLastCrossGeno2().c_str();
        creatures[creatures.length - 1].genotype = creatures[creatures.length - 1]._genotype;
      }
    }

    Module.destroy(genoOper);
    Module.destroy(genoOperHelper);
  }

  function crossOverPopulation () {
    var creaturesLength = creatures.length;
    if(creaturesLength > 1) {
      var toRemove = [];

      for (var i = 0; i < creaturesLength; ++i) {
        toRemove.push(creatures[i].id);
        for (var j = 0; j < creaturesLength; ++j) {
          if (i != j && creatures.length - creaturesLength < FredConfig.population.maxPopulation)
            crossOverCreatures(creatures[i], creatures[j]);
        }
      }

      for (var i = 0; i < toRemove.length; ++i) {
        deleteCreature(toRemove[i]);
      }

      selectCurrentCreature(creatures[0]);
      AppEvents.publish("creaturesChanged");
    }
  }

  return {
    updateGenotype: updateGenotype,
    loadCreaturesFromFileData: loadCreaturesFromFileData,
    loadCreatureAsNewFile: loadCreatureAsNewFile,
    updateModel: updateModel,
    publishModelUpdated: publishModelUpdated,
    getPartShapes: getPartShapes,
    getJointShapes: getJointShapes,
    partsNotJoined: partsNotJoined,
    addNewPart: addNewPart,
    addNewJoint: addNewJoint,
    removePart: removePart,
    removeJoint: removeJoint,
    readLogAndGetGenotypeMessages: readLogAndGetGenotypeMessages,
    generateFileData: generateFileData,
    getAllCreatures: getAllCreatures,
    addNewGenotype: addNewGenotype,
    selectCreature: selectCreature,
    deleteCreature: deleteCreature,
    getCurrentCreature: getCurrentCreature,
    getNeuroClassHints: getNeuroClassHints,
    getPartCount: getPartCount,
    mutatePopulation: mutatePopulation,
    crossOverPopulation: crossOverPopulation
  };
})();
