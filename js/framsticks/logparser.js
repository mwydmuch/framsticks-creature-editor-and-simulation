var FramsticksSDKLogParser = function( ) {

  var logLevel = [];
  logLevel["[DEBUG]"] = { msgType: "info", sdkValue: -1 };
  logLevel[""] = { msgType: "info", sdkValue: 0 };
  logLevel["[WARN]"] = { msgType: "warning", sdkValue: 1 };
  logLevel["[ERROR]"] = { msgType: "error", sdkValue: 2 };
  logLevel["[CRITICAL]"] = { msgType: "error", sdkValue: 3 };

  var logger = new Module.LoggerToMemory( Module.LoggerBase["Enable"] + Module.LoggerToMemory["StoreAllMessages"], FredConfig.SDK.validation.minimalLogLevel );
  var model = null;

  var savedMessages = null;

  function getObjectNumbers( objStringNums ) {
    var nums = [];
    if( objStringNums && objStringNums.length ) {
      for( var i=0; i<objStringNums.length; ++i ) {
        var index = objStringNums[ i ].indexOf( '#' );
        if( index > -1 ) {
          var num = Number.parseFloat( objStringNums[ i ].substring(index+1) );
          if( !isNaN( num ) ) {
            nums.push( num );
          }
        }
      }
    }
    return nums;
  }

  function getLogOutput() {
    var msgs = logger.getMessages().c_str();
    logger.reset();
    return msgs;
  }

  function parseLogOutput( ) {
    if( model ) {

      var logOutput = getLogOutput();

      var result = {
        genoLineMsgs: [],
        allMessages: [],
      }

      var lines = logOutput.split('\n');
      var distinctLines = [];

      for(var i=0; i<lines.length; ++i) {

        if( distinctLines.indexOf( lines[i] ) === -1 ) {
          distinctLines.push( lines[i] );

          var type;
          var sdkType;
          var shortText;

          var line = lines[i].trim();
          var msgSDKTypeEndIndex = line.indexOf( ']' );
          if( msgSDKTypeEndIndex > -1 ) {
            sdkType = line.substring( 0, msgSDKTypeEndIndex+1 );
            shortText = line.substring( msgSDKTypeEndIndex+1 ).trim();
            var msgType = logLevel[ sdkType ].msgType;
            if( msgType ) {
              type = msgType;
            }
          } else {
            sdkType = "";
            shortText = line;
            type = logLevel[ sdkType ].msgType;
          }

          var genoLineNums = getObjectNumbers( line.toLowerCase().match( /line[ ]{1,}#([0-9]{1,})/g ) );
          for( var g=0; g<genoLineNums.length; ++g ) {
            result.genoLineMsgs.push( {
              text: line,
              column: 0,
              row: genoLineNums[g]-1,
              type: type
            } );
          }

          var message = {
            shortText: shortText,
            type: type,
            sdkType: sdkType,
            bodyElementTargets: []
          };
/*
          var partNums = getObjectNumbers( line.toLowerCase().match( /part[ ]{1,}#([0-9]{1,})/g ) );
          for( var p=0; p<partNums.length; ++p ) {
            var part = model.getPart( partNums[p] );
            if( part && part.ptr != 0 ) {
              message.bodyElementTargets.push( {
                name: "Part #" + String( partNums[p] ),
                object: part
              } );
            }
          }

          var jointNums = getObjectNumbers( line.toLowerCase().match( /joint[ ]{1,}#([0-9]{1,})/g ) );
          for( var j=0; j<jointNums.length; ++j ) {
            var joint = model.getJoint( jointNums[j] );
            if( joint && joint.ptr != 0 ) {
              message.bodyElementTargets.push( {
                name: "Joint #" + String( jointNums[j] ),
                object: joint
              } );
            }
          }
*/
          //var neuronNums = getObjectNumbers( line.toLowerCase().match( /neuron[ ]{1,}#([0-9]{1,})/g ) );
          /*for( var n=0; n<neuronNums.length; ++n ) {
            message.bodyElementTargets.push( {
              name: "neuron #" + String( neuronNums[n] ),
              //object
            } );
          }*/

          //var connectionNums = getObjectNumbers( line.toLowerCase().match( /neural connection[ ]{1,}#([0-9]{1,})/g ) );
          /*for( var c=0; c<connectionNums.length; ++c ) {
            message.bodyElementTargets.push( {
              name: "neuron connection #" + String( connectionNums[c] ),
              //object
            } );
          }*/

          if( shortText ) {
            result.allMessages.push( message );
          }

        }

      }
      return result;
    }
  }

  function saveSDKLog() {
    savedMessages = parseLogOutput();
  }


  function readLogMessages() {
    var result;

    if ( savedMessages != null ) {
      result = savedMessages;
    } else {
      result = parseLogOutput();
    }

    savedMessages = null;
    return result;
  }

  function setModel( value ) {
    model = value;
  }

  return {
    saveSDKLog: saveSDKLog,
    readLogMessages: readLogMessages,
    setModel: setModel
  };

};
