function PartMeshFactory(partShapes)
{
  var config = FredConfig.editor.geometry.part;

  function getNewSphereMesh(part, sphereRadius, segments, applyScale) {
    var geometry = new THREE.SphereGeometry( sphereRadius, segments, segments );
    var material = Transformations.getNewMaterial( part.get_vcolor().get_x(), part.get_vcolor().get_y(), part.get_vcolor().get_z() );

    var mesh = new Physijs.SphereMesh( geometry, Physijs.createMaterial(material, 0.1, 0.5)); //new THREE.Mesh( geometry, material );

    mesh.position.set(part.get_p().get_x(), part.get_p().get_y(), part.get_p().get_z());
    if(applyScale) {
      mesh.scale.set(part.get_scale().get_x(), part.get_scale().get_y(), part.get_scale().get_z());
    }

    var angles = part.get_o().getAngles();
    mesh.rotation.copy( Transformations.getPartRotation( angles.get_x(), angles.get_y(), angles.get_z() ) );

    mesh.userData = { isBodyElement: true, type: 'p', data: part, mesh: mesh, connectedJoints: [] };
    return mesh;
  }

  function getNewBoxMesh(part, boxSize) {
    var geometry = new THREE.CubeGeometry(boxSize, boxSize, boxSize);
    var material = Transformations.getNewMaterial( part.get_vcolor().get_x(), part.get_vcolor().get_y(), part.get_vcolor().get_z() );

    var mesh = new Physijs.BoxMesh( geometry, Physijs.createMaterial(material) ); //new THREE.Mesh( geometry, material );

    mesh.position.set(part.get_p().get_x(), part.get_p().get_y(), part.get_p().get_z());
    mesh.scale.set(part.get_scale().get_x(), part.get_scale().get_y(), part.get_scale().get_z());

    var angles = part.get_o().getAngles();
    mesh.rotation.copy( Transformations.getPartRotation( angles.get_x(), angles.get_y(), angles.get_z() ) );

    mesh.userData = { isBodyElement: true, type: 'p', data: part, mesh: mesh, connectedJoints: [] };
    return mesh;
  }

  function getNewCylinderMesh(part) {
    var geometry = new THREE.CylinderGeometry(1, 1, 2, 32);
    var material = Transformations.getNewMaterial( part.get_vcolor().get_x(), part.get_vcolor().get_y(), part.get_vcolor().get_z() );
    var mesh = Physijs.CylinderMesh( geometry, Physijs.createMaterial(material) ); //new THREE.Mesh( geometry, material );
    mesh.position.set(part.get_p().get_x(), part.get_p().get_y(), part.get_p().get_z());
    mesh.scale.set(part.get_scale().get_y(), part.get_scale().get_x(), part.get_scale().get_z());

    var angles = part.get_o().getAngles();
    var m = Transformations.getCylinderPartRotationMatrix( angles.get_x(), angles.get_y(), angles.get_z() );
    mesh.rotation.setFromRotationMatrix( m );

    mesh.userData = { isBodyElement: true, type: 'p', data: part, mesh: mesh, connectedJoints: [] };
    return mesh;
  }

  function create(part) {

    var result;
    var shape = part.get_shape();
    if( partShapes['SHAPE_ELLIPSOID'].value == shape ) {
      result = getNewSphereMesh(part, config.ellipsoidShape.radius, config.ellipsoidShape.segments, true);
    }
    else if( partShapes['SHAPE_CUBOID'].value == shape ) {
      result = getNewBoxMesh(part, config.boxShape.size);
    }
    else if( partShapes['SHAPE_CYLINDER'].value == shape ) {
      result = getNewCylinderMesh(part);
    }
    else{
      result = getNewSphereMesh(part, config.defaultShape.radius, config.defaultShape.segments, false);
    }

    result._physijs.mass *= config.density;
    result.castShadow = true;
    result.receiveShadow = true;
    return result;
  }

  return {
    create: create
  };
}
