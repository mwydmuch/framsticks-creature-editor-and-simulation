function JointMeshFactory(jointShapes)
{

  var config = FredConfig.editor.geometry.joint;

  function getNewJointMesh( joint, shapeConfig ) {

    var firstPartPosVec  = new THREE.Vector3( joint.get_part1().get_p().get_x(), joint.get_part1().get_p().get_y(), joint.get_part1().get_p().get_z() );
    var secondPartPosVec = new THREE.Vector3( joint.get_part2().get_p().get_x(), joint.get_part2().get_p().get_y(), joint.get_part2().get_p().get_z() );

    var direction = new THREE.Vector3().subVectors(secondPartPosVec, firstPartPosVec);
    var orientation = new THREE.Matrix4();

    var geometry = new THREE.CylinderGeometry(shapeConfig.radius, shapeConfig.radius, direction.length(), shapeConfig.radiusSegments);
    var material = Transformations.getNewMaterial(joint.get_vcolor().get_x(), joint.get_vcolor().get_y(), joint.get_vcolor().get_z());

    material.transparent = shapeConfig.isTransparent;
    material.opacity = shapeConfig.opacity;

    var mesh = new THREE.Mesh( geometry, material ); //new Physijs.CylinderMesh( geometry, Physijs.createMaterial(material) ); //new THREE.Mesh( geometry, material );

    orientation.lookAt(firstPartPosVec, secondPartPosVec, new THREE.Object3D().up);
    orientation.multiply(new THREE.Matrix4().set(1, 0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1));

    mesh.applyMatrix(orientation);
    mesh.position.x = (secondPartPosVec.x + firstPartPosVec.x) / 2;
    mesh.position.y = (secondPartPosVec.y + firstPartPosVec.y) / 2;
    mesh.position.z = (secondPartPosVec.z + firstPartPosVec.z) / 2;
    mesh.userData = { isBodyElement: true, type: 'j', data: joint, mesh: mesh, connectedParts: [], showTransparent: shapeConfig.isTransparent };

    return mesh;

  }

  function addConnectionInfo( jointMesh, partMeshes ) {

    var p1 = jointMesh.data.get_part1();
    var p2 = jointMesh.data.get_part2();
    var count = 0;
    for(var i=0; i < partMeshes.length && count<2; ++i) {
      if( partMeshes[i].data === p1 || partMeshes[i].data === p2 ) {
        jointMesh.connectedParts.push( partMeshes[i] );
        partMeshes[i].connectedJoints.push( jointMesh );
        ++count;
      }
    }
  }

  function create(joint, partMeshes) {
    var result;
    var shape = joint.get_shape();

    if( jointShapes['SHAPE_FIXED'].value == shape ) {
      result = getNewJointMesh( joint, config.linkShape );
    } else {
      result = getNewJointMesh( joint, config.cylinderShape );
    }

    if( partMeshes ) {
      addConnectionInfo( result.userData, partMeshes );
    }

    //result._physijs.mass *= config.density;
    result.castShadow = true;
    result.receiveShadow = true;
    return result;
  }

  return {
    create: create
  };
}
