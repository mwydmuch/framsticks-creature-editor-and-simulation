var CameraMenu = function ( containerId, id, canvasId, camera, scene ) {

  var menu;

  function onZoomAll() {
    camera.zoomAll( scene.getMeshes() );
  }

  function init () {

    var container = document.getElementById( String( containerId ) );

    menu = document.createElement( "div" );
    menu.id = String( id );

    var zoomAllBtn = document.createElement( "div" );
    zoomAllBtn.id = "zoom-all-btn";
    menu.appendChild( zoomAllBtn );

    container.appendChild( menu );

    $( "#zoom-all-btn" ).button( {
      icons: { primary: "ui-icon-arrow-4" },
      label: AppResources.get( "zoomAll_label" )
    }).click( onZoomAll );

  }

  this.setPosition = function( canvasHeight ) {
    menu.setAttribute( 'style', 'position: absolute; top:' + String(canvasHeight - menu.clientHeight) + "px;" );
  };

  this.hideZoomAllBtn = function() {
    $( "#zoom-all-btn" ).hide();
  };

  this.showZoomAllBtn = function() {
    $( "#zoom-all-btn" ).show();
    this.setPosition( $( "#" + String( canvasId ) ).height() );
  };

  init();

};