var SimulationMenu = function ( containerId, id, canvasId, scene, view) {

    var menu;
    var change = true;

    function onSimulationStart() {
        view.clearView();
        scene.startSimulation();
    }

    function onSimulationStop() {
        view.clearView();
        scene.stopSimulation();
    }

    function onSimulationReset() {
        view.clearView();
        scene.resetSimulation();
    }

    function buildHTMLInfo(status, steps, time, velocity, height){
        var info = '<div id="simulation-title">' + AppResources.get( "simulation_label" )
            + '</div><div>Status: <span id="simulation-status" class="' + status + '">' + status + '</span>'
            + '</div><div>Steps: ' + steps.toString()
            + '</div><div>Time: ' + time.toFixed(3) + 's'
            + '</div><div>Velocity: ' + velocity.toFixed(3)
            + '</div><div>Height: ' + height.toFixed(3) + '</div>';
        return info;
    }

    this.update = function() {
        if(scene.isSimulationRunning()){
            change = true;
            $( "#simulation-info" ).html(buildHTMLInfo(
                scene.isSimulationStable() ? "stable" : "running",
                scene.getSimulationSteps(),
                scene.getSimulationTime(),
                scene.getCreatureVelocity(),
                scene.getCreatureHeight()));
        }
        else if(change){
            change = false;
            $( "#simulation-info" ).html(buildHTMLInfo(
                "stopped",
                scene.getSimulationSteps(),
                scene.getSimulationTime(),
                scene.getCreatureVelocity(),
                scene.getCreatureHeight()));
        }
    };

    function init () {

        var container = document.getElementById( String( containerId ) );

        menu = document.createElement( "div" );
        menu.id = String( id );

        var simulationInfo = document.createElement( "div" );
        simulationInfo.id = "simulation-info";
        menu.appendChild( simulationInfo );

        var simulationStartBtn = document.createElement( "div" );
        simulationStartBtn.id = "simulation-start-btn";
        menu.appendChild( simulationStartBtn );

        var simulationStopBtn = document.createElement( "div" );
        simulationStopBtn.id = "simulation-stop-btn";
        menu.appendChild( simulationStopBtn );

        var simulationResetBtn = document.createElement( "div" );
        simulationResetBtn.id = "simulation-reset-btn";
        menu.appendChild( simulationResetBtn );

        container.appendChild( menu );

        $( "#simulation-start-btn" ).button( {
            icons: { primary: "ui-icon-play" },
            label: AppResources.get( "simulationStart_label" )
        }).click( onSimulationStart );

        $( "#simulation-stop-btn" ).button( {
            icons: { primary: "ui-icon-stop" },
            label: AppResources.get( "simulationStop_label" )
        }).click( onSimulationStop );

        $( "#simulation-reset-btn" ).button( {
            icons: { primary: "ui-icon-arrowrefresh-1-e" },
            label: AppResources.get( "simulationReset_label" )
        }).click( onSimulationReset );
    }

    this.setPosition = function( canvasHeight ) {
        menu.setAttribute( 'style', 'right: 0px; position: absolute; top:' + String(canvasHeight - menu.clientHeight) + "px;" );
    };

    this.hideBtn = function() {
        $( "#simulation-start-btn" ).hide();
        $( "#simulation-stop-btn" ).hide();
        $( "#simulation-reset-btn" ).hide();
    };

    this.showBtn = function() {
        $( "#simulation-start-btn" ).show();
        $( "#simulation-stop-btn" ).show();
        $( "#simulation-reset-btn" ).show();
        this.setPosition( $( "#" + String( canvasId ) ).height() );
    };

    init();

};