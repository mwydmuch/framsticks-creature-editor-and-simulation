function Creature(config, partShapes, jointShapes) {
    this.objects = []; // all creature meshes
    this.constraints = [];
    this.joints = [];
    this.parts = [];
    this.model = null;

    this.add = function (obj) {
        if (obj instanceof THREE.Mesh) {
            if (obj.userData.type === 'j') {
                // var jointConstraintFactory = new JointConstraintFactory();
                // var constraint = jointConstraintFactory.create(obj, this.parts);
                // this.constraints.push(constraint);
                this.joints.push(obj.userData);
                this.objects.push(obj);
            }

            if (obj.userData.type === 'p') {
                this.parts.push(obj.userData);
                this.objects.push(obj);
            }
        }
    };

    this.remove = function (obj) {
        if (obj instanceof THREE.Mesh) {
            if (obj.userData.type === 'j') {
                this.joints = this.joints.filter(function(_obj) { return _obj != obj; });
                //this.constraints = this.constraints.filter(function(_obj) { return _obj != obj._constraint; });
            }
            if (obj.userData.type === 'p') {
                this.parts = this.parts.filter(function(_obj) { return _obj != obj; });
            }
        }

        this.objects = this.objects.filter(function(_obj) { return _obj != obj; });
    };

    this.updateModel = function (model) {
        this.model = model;

        this.objects = [];
        //this.constraints = [];
        this.parts = [];
        this.joints = [];

        var partMeshFactory = new PartMeshFactory(partShapes);
        var pc = model.getPartCount();
        for (var i = 0; i < pc; ++i) {
            var mesh = partMeshFactory.create(model.getPart(i));
            this.add(mesh);
        }

        var jointMeshFactory = new JointMeshFactory(jointShapes);
        var jc = model.getJointCount();
        for (var i = 0; i < jc; ++i) {
            var mesh = jointMeshFactory.create(model.getJoint(i), this.parts);
            this.add(mesh);
        }
    };

    this.updateConstraints = function(){
        this.constraints = [];
        var jointConstraintFactory = new JointConstraintFactory();
        for (var i = 0; i <  this.objects.length; ++i) {
            var obj = this.objects[i];
            if (obj.userData.type === 'j') {
                var constraint = jointConstraintFactory.create(obj, this.parts);
                this.constraints.push(constraint);
            }
        }
    };

    this.update = function () {
        for (var i = 0; i < this.objects.length; ++i) {
            var mesh = this.objects[i];
            if (mesh instanceof THREE.Mesh && mesh.userData.type === 'j') {
                var firstPartPosVec = mesh._part1.position;
                var secondPartPosVec = mesh._part2.position;

                var orientation = new THREE.Matrix4();
                orientation.lookAt(firstPartPosVec, secondPartPosVec, new THREE.Object3D().up);
                orientation.multiply(new THREE.Matrix4().set(1, 0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1));
                mesh.setRotationFromMatrix(orientation);
                mesh.position.x = (secondPartPosVec.x + firstPartPosVec.x) / 2;
                mesh.position.y = (secondPartPosVec.y + firstPartPosVec.y) / 2;
                mesh.position.z = (secondPartPosVec.z + firstPartPosVec.z) / 2;
            }
        }
    };

    this.getVelocity = function () {
        if (this.objects.length) return this.objects[0].getLinearVelocity().length();
        else return 0;
    };

    this.getHeight = function () {
        if (this.objects.length) {
            var maxz = 0;
            for (var i = 0; i < this.objects.length; ++i) {
                var part = this.objects[i];
                if (maxz < part.position.z) maxz = part.position.z
            }
            return maxz;
        }
        else return 0;
    };

    this.isStable = function () {
        if (this.objects.length) {
            var stable = true;
            for (var i = 0; i < this.objects.length; ++i) {
                var part = this.objects[i];
                if (part instanceof Physijs.Mesh) {
                    var velocity = part.getLinearVelocity();
                    if (velocity.length() > config.stabilityThreshold) {
                        stable = false;
                        break;
                    }
                }
            }
            return stable;
        }
        return true;
    };
};