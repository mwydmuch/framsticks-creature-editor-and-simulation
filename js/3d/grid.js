function Grid( size, interval, centerLineColor, secondColor, displayAxes ) {

    var halfSize = size / 2;
    var vertices = [];
    var colors = [];

    for ( var i = 0, j = 0, pos = -halfSize; i <= size; ++i, pos += interval ) {

        var lineEnd = ( i === halfSize && displayAxes ) ? 0 : halfSize;

        vertices.push( -halfSize, pos, 0, lineEnd, pos, 0 );
        vertices.push( pos, -halfSize, 0, pos, lineEnd, 0 );

        var color = ( i === halfSize ) ? new THREE.Color( centerLineColor ) : new THREE.Color( secondColor );

        for( var k=0; k<4; ++k ) {
            color.toArray( colors, j );
            j += 3;
        }

    }

    var geometry = new THREE.BufferGeometry();
    geometry.addAttribute( 'position', new THREE.Float32Attribute( vertices, 3 ) );
    geometry.addAttribute( 'color', new THREE.Float32Attribute( colors, 3 ) );

    var material = new THREE.LineBasicMaterial( { vertexColors: THREE.VertexColors } );

    THREE.LineSegments.call( this, geometry, material );

    if( displayAxes ) {
        var axis = new THREE.AxisHelper( halfSize );
        this.children.push( axis );
    }

}

Grid.prototype = Object.create( THREE.LineSegments.prototype );
Grid.prototype.constructor = Grid;