function Raycaster(config, scene, camera)
{
  var raycaster = new THREE.Raycaster();
  var focusedBodyElement = null;
  var selectedBodyElement = null;

  function updateImpl( position ) {

    raycaster.setFromCamera( position, camera );
    var intersects = raycaster.intersectObjects( scene.getChildren() );
    var closestBodyElement = null;
    for ( var i = 0; i < intersects.length && closestBodyElement == null && selectedBodyElement !== intersects[i].object; i++ ) {
      if(intersects[i].object.userData.isBodyElement && intersects[i].object.material) {
        intersects[i].object.material.transparent = false;
        intersects[i].object.material.emissive.set( config.focusedElement.color );

        closestBodyElement = intersects[i].object;
      }
    }

    if( closestBodyElement != focusedBodyElement ) {
      clearFocusedHighlight();
      focusedBodyElement = closestBodyElement;
    }

  }

  function clearFocusedHighlight() {
    if( focusedBodyElement && focusedBodyElement.material ) {
      focusedBodyElement.material.transparent = focusedBodyElement.userData.showTransparent;
      focusedBodyElement.material.emissive.set( 0x000000 );
    }
  }

  function clearFocusedElement() {
    clearFocusedHighlight();
    focusedBodyElement = null;
  }

  function update( mouseInput ) {

    if( !mouseInput.isDown ) {
      updateImpl( mouseInput.positionInDeviceCoords )
    } else {
      clearFocusedElement();
    }

  }

  function getFocusedBodyElementData() {
    if(focusedBodyElement && focusedBodyElement.userData) {
      return focusedBodyElement.userData;
    }
    return null;
  }

  function getSelectedBodyElementData() {
    if(selectedBodyElement && selectedBodyElement.userData) {
      return selectedBodyElement.userData;
    }
    return null;
  }

  function selectFocusedElement() {
      clearFocusedHighlight();
      selectedBodyElement = focusedBodyElement;
  }

  function getCurrentRay() {
    var ray = new THREE.Ray( );
    ray.copy( raycaster.ray )
    return ray;
  }

  function clear() {
    focusedBodyElement = null;
    selectedBodyElement = null;
  }

  function updateAfterMeshChange( ) {
    if( selectedBodyElement && selectedBodyElement.userData ) {
      selectedBodyElement = selectedBodyElement.userData.mesh;
    }
  }

  return {
    update: update,
    getFocusedBodyElementData: getFocusedBodyElementData,
    getSelectedBodyElementData: getSelectedBodyElementData,
    selectFocusedElement: selectFocusedElement,
    getCurrentRay: getCurrentRay,
    clear: clear,
    updateAfterMeshChange: updateAfterMeshChange
  };

}
