var Scene = function (config, partShapes, jointShapes) {

  var creature = new Creature(config, partShapes, jointShapes); // can be change to array in feature
  var frames = 0; // number of rendered frames
  var simulation = false;
  var simulationTimeStep = config.timeStep;
  var simulationSteps = 0; // number of simulation steps
  var simulationTime = 0; // time passed in simulation
  var simulationStartingSteps = config.startingSteps;
  var simulationTimeout = config.timeout;
  var simulationFloor = null;

  Physijs.scripts.worker = 'js/external/physijs/physijs_worker.js';
  Physijs.scripts.ammo = 'ammo.js';

  var scene = new Physijs.Scene(); //new THREE.Scene();
  scene.setGravity(new THREE.Vector3(config.gravity.x, config.gravity.y, config.gravity.z));
  scene.setFixedTimeStep(config.timeStep);
  var simulationOffset = new THREE.Vector3(config.offset.x, config.offset.y, config.offset.z);

  // scene object manipulation
  this.addConstraint = function(constraint) {
    scene.addConstraint( constraint );
    constraint.scene = scene;
    constraint.setAngularLowerLimit({ x: 0, y: 0, z: 0 });
    constraint.setAngularUpperLimit({ x: 0, y: 0, z: 0 });
    constraint.setLinearLowerLimit({ x: 0, y: 0, z: 0 });
    constraint.setLinearUpperLimit({ x: 0, y: 0, z: 0 });
  };

  this.add = function( obj ) {
    creature.add(obj);
    scene.add(obj);
    // if (obj instanceof THREE.Mesh && obj.userData.type === 'j') {
    //     this.addConstraint(obj._constraint);
    // }
  };

  this.remove = function( obj ) {
    scene.remove(obj);
    // if (obj instanceof THREE.Mesh && obj.userData.type === 'j') {
    //   scene.removeConstraint(obj._constraint);
    // }

    creature.remove(obj);

    if( obj.geometry ) {
      obj.geometry.dispose();
      obj.geometry = null;
    }

    if( obj.material ) {
      obj.material.dispose();
      obj.material = null;
    }
  };

  this.getChildren = function() {
    return scene.children;
  };

  this.getMeshes = function() {
    var meshes = [];
    for( var i=0; i < scene.children.length; ++i ) {
      var child = scene.children[ i ];
      if( child instanceof THREE.Mesh ) {
        meshes.push( child );
      }
    }
    return meshes;
  };

  this.clear = function() {
    if(simulationFloor) scene.remove(simulationFloor);

    var toRemove = [];
    for(var i=0; i < scene.children.length; ++i) {
      var obj = scene.children[i];
      if( obj.userData && obj.userData.isBodyElement ) {
        toRemove.push(obj);
      }
    }

    for(var i=0; i<toRemove.length; ++i) {
      this.remove(toRemove[i]);
    }
  };

  this.addCreature = function(creature){
    for( var i = 0; i < creature.objects.length; ++i ) {
      scene.add(creature.objects[i]);
    }
  };

  this.updateConstraints = function(){
    for(var i = 0; i < creature.constraints.length; ++i) {
      scene.removeConstraint(creature.constraints[i]);
    }

    creature.updateConstraints();
    for( var i = 0; i < creature.constraints.length; ++i ) {
      this.addConstraint(creature.constraints[i]);
    }
  };

  this.updateCreature = function(model){

    simulation = false;
    simulationSteps = 0;
    simulationTime = 0;

    this.clear();
    creature.updateModel(model);
    this.addCreature(creature);

    AppEvents.publish("simulationCreatureUpdated");
    return creature.objects;
  };

  this.render = function(renderer, camera) {
    ++frames;
    this.simulate();
    renderer.render(scene, camera);
  };

  // simulation
  this.simulate = function(){
    if(simulation) {
      ++simulationSteps;
      simulationTime += simulationTimeStep;
      scene.simulate();
      creature.update();

      if(simulationSteps > simulationStartingSteps && (simulationTime > simulationTimeout || creature.isStable())){
        Framsticks.getAllCreatures().currentCreature._score = creature.getHeight(); //TODO: do it better
        AppEvents.publish("simulationEnded");
      }
    }
  };

  this.startSimulation = function(){

    if(!simulationSteps) {
      this.updateCreature(creature.model);
      this.updateConstraints();

      for( var i=0; i < scene.children.length; ++i ) {
        var child = scene.children[ i ];
        if( child instanceof THREE.Mesh){
          child.position.add(simulationOffset);
        }
        if( child instanceof Physijs.Mesh ) {
          child.setLinearVelocity(new THREE.Vector3(0, 0, 0));
          child.setAngularVelocity(new THREE.Vector3(0, 0, 0));
          child.__dirtyPosition = true;
        }
      }

      simulationFloor = new Physijs.PlaneMesh(new THREE.PlaneGeometry(10, 10), Physijs.createMaterial(new THREE.MeshPhongMaterial()));
      simulationFloor.position.z = 0;
      simulationFloor.castShadow = true;
      simulationFloor.receiveShadow = true;
      scene.add(simulationFloor);
    }

    simulation = true;
    scene.onSimulationResume();
  };

  this.stopSimulation = function(){
    simulation = false;
  };

  this.resetSimulation = function(){
    this.updateCreature(creature.model);
  };

  // simulation setters and getters
  this.setSimulationGravity = function(force_x, force_y, force_z){
    scene.setGravity(new THREE.Vector3(force_x, force_y, force_z));
  };

  this.setSimulationTimeStep = function(timeStep){
    simulationTimeStep = timeStep;
    scene.setFixedTimeStep(timeStep);
  };

  this.setSimulationOffset = function(offset_x, offset_y, offset_z){
    simulationOffset = new THREE.Vector3(offset_x, offset_y, offset_z)
  };

  this.setSimulationTimeout = function(timeout){
    simulationOffset = new THREE.Vector3(offset_x, offset_y, offset_z)
  };

  this.isSimulationRunning = function(){ return simulation; }
  this.getSimulationSteps = function(){ return simulationSteps; };
  this.getSimulationTime = function(){ return simulationTime; };
  this.getCreatureHeight = function(){ return creature.getHeight(); };
  this.getCreatureVelocity = function(){ return creature.getVelocity(); };
  this.isSimulationStable = function(){ return creature.isStable()};
};
