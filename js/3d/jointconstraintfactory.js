function JointConstraintFactory()
{
    var config = FredConfig.editor.geometry.joint;

    function addConstraint( jointMesh, partMeshes ) {

        var constraint = null;
        var joint = jointMesh.userData;
        var p1 = joint.data.get_part1();
        var p2 = joint.data.get_part2();
        var mesh_p1 = null;
        var mesh_p2 = null;
        var count = 0;
        for(var i=0; i < partMeshes.length && count < 2; ++i) {

            if( partMeshes[i].data === p1){
                mesh_p1 = partMeshes[i].mesh;
                ++count;
            }
            if( partMeshes[i].data === p2){
                mesh_p2 = partMeshes[i].mesh;
                ++count;
            }
        }

        if(mesh_p1 && mesh_p2) {
            jointMesh._part1 = mesh_p1;
            jointMesh._part2 = mesh_p2;
            constraint = new Physijs.DOFConstraint(mesh_p1, mesh_p2, mesh_p1.position);
            jointMesh._constraint = constraint;
        }

        return constraint;
    }

    function create(jointMesh, partMeshes) {
        var result;
        if( partMeshes ) result = addConstraint(jointMesh, partMeshes );
        return result;
    }

    return {
        create: create
    };
}
