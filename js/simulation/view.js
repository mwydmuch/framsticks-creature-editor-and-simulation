var SimulationView = (function(config, controller, partShapes, jointShapes)
{

  var container = document.getElementById("editor-container");
  var renderer = new THREE.WebGLRenderer( {
    antialias: true,
    devicePixelRatio: window.devicePixelRatio || 1
  } );
  renderer.setSize( window.innerWidth, window.innerHeight );
  container.appendChild( renderer.domElement );
  renderer.domElement.id = config.canvas.id;

  var scene = new Scene(config.simulation, partShapes, jointShapes);
  var camera = new Camera( config.camera, renderer.domElement, AppParams["autorotate"] );
  var cameraMenu = new CameraMenu( "editor-controls-container", "camera-menu", config.canvas.id, camera, scene );
  var simulationMenu = new SimulationMenu( "editor-controls-container", "simulation-menu", config.canvas.id, scene, this );
  var sceneMouseInput = new SceneMouseInput( renderer );
  var transformControl = new THREE.TransformControls( camera.getPerspectiveCamera(), renderer.domElement );

  camera.getCameraControl().addEventListener( 'change', function() {
    transformControl.update();
  } );

  if( !AppParams['camera-input'] ) {
    camera.getCameraControl().enabled = false;
  }

  if( AppParams['show-grid'] ) {
    var grid = new Grid( config.grid.size, config.grid.interval, config.grid.centerLineColor, config.grid.color, AppParams['show-axes'] );
    scene.add( grid );
  }

  var ambientLight = new THREE.AmbientLight( config.light.ambient.color );
  scene.add( ambientLight );

  var topDirectionalLight = new THREE.DirectionalLight( config.light.directional.top.color, config.light.directional.top.intensity );
  topDirectionalLight.position.set( 0, 0, config.light.directional.top.positionZ );
  topDirectionalLight.castShadow = config.light.shadow;
  topDirectionalLight.shadow.mapSize.width = 2048;
  topDirectionalLight.shadow.mapSize.height = 2048;
  scene.add( topDirectionalLight );

  var bottomDirectionalLight = new THREE.DirectionalLight( config.light.directional.bottom.color, config.light.directional.bottom.intensity );
  bottomDirectionalLight.position.set( 0, 0, config.light.directional.bottom.positionZ );
  scene.add( bottomDirectionalLight );

  renderer.shadowMap.type = THREE.PCFSoftShadowMap;
  renderer.shadowMap.enabled = true;

  var resizeInEditorMode;

  if( !AppParams['viewer-mode'] ) {

    resizeInEditorMode = function( width, height, offsetTop ) {
      renderer.context.canvas.width = width;
      renderer.context.canvas.height = height;
      camera.getPerspectiveCamera().aspect = width / height;
      camera.getPerspectiveCamera().updateProjectionMatrix();
      renderer.setSize( width, height );
      renderer.setPixelRatio( window.devicePixelRatio || 1 );

      sceneMouseInput.setOffsetTop( offsetTop );
      cameraMenu.setPosition( height );
      simulationMenu.setPosition(height);
    }

  } else {
    function resizeWindow ( event ) {
      renderer.context.canvas.width = window.innerWidth;
      renderer.context.canvas.height = window.innerHeight;
      camera.getPerspectiveCamera().aspect = window.innerWidth / window.innerHeight;
      camera.getPerspectiveCamera().updateProjectionMatrix();
      renderer.setSize( window.innerWidth, window.innerHeight );
      renderer.setPixelRatio( window.devicePixelRatio || 1 );
    }

    $( window ).resize( resizeWindow );
  }

  function render() {
    camera.getCameraControl().update();
    scene.render(renderer, camera.getPerspectiveCamera());
    simulationMenu.update();

    setTimeout( function() {
      requestAnimationFrame(render);
    }, 1000 / config.fps );
  }

  function clearView() {

  }

  this.clearView = clearView;

  function updateBody( model ) {

    clearView();
    var meshes = scene.updateCreature(model);

    if( model.isNewGenotype ) {
      camera.zoomAll( meshes );
    }

    if( meshes.length > 0 ) {
      cameraMenu.showZoomAllBtn();
      simulationMenu.showBtn();
    } else {
      cameraMenu.hideZoomAllBtn();
      simulationMenu.hideBtn();
    }
  }

  if( !AppParams['viewer-mode'] ) {

    controller( scene
      , sceneMouseInput
      , cameraMenu );
  }

  function startSimulation(){
    scene.startSimulation();
  }

  function stopSimulation(){
    scene.stopSimulation();
  }

  function resetSimulation(){
    scene.resetSimulation();
  }

  AppEvents.register( "startSimulation", startSimulation );
  AppEvents.register( "stopSimulation", stopSimulation );
  AppEvents.register( "resetSimulation", resetSimulation );
  AppEvents.register( "genotypeChanged_bodyData", updateBody );
  render();

  return {
    resizeInEditorMode: resizeInEditorMode
  }

})(FredConfig.editor, FramstickSimulationController, Framsticks.getPartShapes(), Framsticks.getJointShapes());
