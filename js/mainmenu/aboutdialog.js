var AboutDialog = function( containerId ) {

  var dialog;

  function init() {

    dialog = $( '#'+containerId ).dialog({
      title: AppResources.get( "aboutDialog_title" ),
      autoOpen: false,
      height: 350,
      width: 550,
      modal: true,
      buttons: {
        Close: function() {
          dialog.dialog("close");
        }
      }
    });

  $( '#about-content' ).append( AppResources.get( "aboutDialog_content" ) );

  }

  function generateContactData() {
    var author = document.getElementById( "about-author" );
    if( author && !author.firstChild ) {
      var data = [ String( ["o","t","l","i","a","m"].reverse().join('') + String.fromCharCode( 58 ) ),
                   ["6","8","i","k","s","l","a","h","c","i","m","b"].reverse().join(''),
                   String.fromCharCode( 32*2 ),
                   ['l','i','a','m','g'].reverse().join('') + String.fromCharCode( 46 ),
                   ['m','o','c'].reverse().join('') ];

      var link = document.createElement( 'a' );
      link.href = data[0] + data[1] + data[2] + data[3] + data[4];
      link.textContent = "email";
      author.appendChild( link );
    }
  }

  function open() {
    generateContactData();
    dialog.dialog( "open" );
  }

  init();

  return {
    open: open
  }

};
