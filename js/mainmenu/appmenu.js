var AppMenu = function( containerId, id, items ) {

  var fileInputsByContentId = [];

  var handlersByOptionId = [];
  var handlersByClickElementId = [];

  function onMenuOptionSelected( e ) {
    if( typeof handlersByClickElementId[ e.currentTarget.id ].onmenuitemaction ) {
      handlersByClickElementId[ e.currentTarget.id ].onmenuitemaction( e );
    }
  }

  function onFileInputItemContentClick( e ) {
    fileInputsByContentId[ e.currentTarget.id ].click();
  }

	function init() {

		var mainDiv = document.getElementById( containerId );
    if( mainDiv ) {

      var mainList = document.createElement( 'ul' );
      mainList.id = String( id );

      for( var i=0; i<items.length; ++i ) {

        var mainItem = document.createElement( 'li' );
        mainItem.className = "appmenu-item";

        var mainItemContent = document.createElement( 'div' );
        mainItemContent.classList.add( mainItem.className + "-content" );
        mainItemContent.classList.add( "non-selectable-text-content" );
        mainItemContent.textContent = items[i].header;

        mainItem.appendChild( mainItemContent );
        mainList.appendChild( mainItem );

        var optionsList = document.createElement( 'ul' );
        optionsList.className = "appmenu-options-list";

        for( var j=0; j<items[i].options.length; ++j ) {

          var option = document.createElement( 'li' );
          option.id =  String( items[i].options[j].id );
          option.className = "appmenu-option-item";

          var handlers = {};
          handlersByOptionId[ option.id ] = handlers;

          var optionContent = document.createElement( 'div' );
          optionContent.textContent = items[i].options[j].name;
          optionContent.id = option.id + "-content";
          optionContent.classList.add( option.className + "-content" );
          optionContent.classList.add( "non-selectable-text-content" );

          if( items[i].options[j].isFileInput ) {
            var input = document.createElement( 'input' );
            input.id = optionContent.id + "-input";
            input.type = 'file';
            input.multiple = false;
            input.addEventListener('change', onMenuOptionSelected, false);
            handlersByClickElementId[ input.id ] = handlers;

            optionContent.appendChild( input );

            fileInputsByContentId[ optionContent.id ] = input;
            optionContent.addEventListener('click', onFileInputItemContentClick, false);
          } else {
            optionContent.addEventListener('click', onMenuOptionSelected, false);
            handlersByClickElementId[ optionContent.id ] = handlers;
          }

          option.appendChild( optionContent );
          optionsList.appendChild( option );
        }

        mainItem.appendChild( optionsList )

      }

      mainDiv.appendChild( mainList );

    }

	}

	init();

  return {
    getHandlerByOptionId: function ( optionId ) {
      return handlersByOptionId[ optionId ];
    }
  }

};
