function ReceptorDrawableNeuron ( x, y, w, h, neuroData, config ) {

  BaseDrawableNeuron.call( this, x, y, w, h, neuroData, config );

  this.drawNeuron = function ( context, connCoords ) {
      context.beginPath();
      context.moveTo( connCoords.outputBeginLineX, this.drawCoords.y + this.drawCoords.h * 0.25 );
      context.lineTo( connCoords.outputBeginLineX, this.drawCoords.y + this.drawCoords.h * 0.75 );
      context.lineTo( this.drawCoords.x + this.drawCoords.w * 0.25, this.drawCoords.y + this.drawCoords.h * 0.75 );
      context.lineTo( this.drawCoords.x + this.drawCoords.w * 0.5, this.drawCoords.y + this.drawCoords.h * 0.5 );
      context.lineTo( this.drawCoords.x + this.drawCoords.w * 0.25, this.drawCoords.y + this.drawCoords.h * 0.25 );
      context.lineTo( connCoords.outputBeginLineX, this.drawCoords.y + this.drawCoords.h * 0.25 );

      context.moveTo( connCoords.outputBeginLineX, connCoords.outputLineY );
      context.lineTo( this.drawCoords.x + this.drawCoords.w, connCoords.outputLineY );
      context.stroke();
  }

};

ReceptorDrawableNeuron.prototype = Object.create( BaseDrawableNeuron.prototype );
ReceptorDrawableNeuron.prototype.constructor = ReceptorDrawableNeuron;