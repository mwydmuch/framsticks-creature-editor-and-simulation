function GenericDrawableNeuron ( x, y, w, h, neuroData, config ) {

  BaseDrawableNeuron.call( this, x, y, w, h, neuroData, config );

  this.drawNeuron = function ( context, connCoords ) {

    context.beginPath();
    context.moveTo( connCoords.outputBeginLineX, connCoords.outputLineY );
    context.lineTo( connCoords.inputEndLineX, this.drawCoords.y );
    context.lineTo( connCoords.inputEndLineX, this.drawCoords.y + this.drawCoords.h );
    context.lineTo( connCoords.outputBeginLineX, connCoords.outputLineY );
    context.lineTo( this.drawCoords.x + this.drawCoords.w, connCoords.outputLineY );
    context.stroke();

  }

};

GenericDrawableNeuron.prototype = Object.create( BaseDrawableNeuron.prototype );
GenericDrawableNeuron.prototype.constructor = GenericDrawableNeuron;