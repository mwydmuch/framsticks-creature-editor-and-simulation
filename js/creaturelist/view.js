var CreatureListView = (function() {

    var creatureListDialog = new CreatureListDialog( "creaturelist-dialog-container", "creaturelist", 500, 300 );

    var controller = new CreatureListController( creatureListDialog );

})();
