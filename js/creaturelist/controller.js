var CreatureListController = function ( creatureListDialog ) {

  function openDialog( ) {
    var creaturesData = Framsticks.getAllCreatures();
    creatureListDialog.open( creaturesData );
  }

  creatureListDialog.getHandlers().onAddNewCreature = function () {
    var newCreature = Framsticks.addNewGenotype();
    creatureListDialog.loadData( Framsticks.getAllCreatures() );
  }

  creatureListDialog.getHandlers().onOpenCreature = function ( creatureID ) {
    Framsticks.selectCreature( creatureID );
  }

  creatureListDialog.getHandlers().onDeleteCreature = function ( creatureID ) {
    var confirmed = confirm( AppResources.get( "creatureDelete_question" ) );
    if( confirmed ) {
      Framsticks.deleteCreature( creatureID );
      creatureListDialog.loadData( Framsticks.getAllCreatures() );
    }
  }

  AppEvents.register( 'openCreatureListDialog', openDialog );

};
