function FileHandler() {

  this.fileName = FredConfig.fileHandler.defaultFileName;

  this.readDataFromFile = function( event, selectCreature ) {
    selectCreature = (typeof selectCreature !== 'undefined') ?  selectCreature : true;

    if ( !event || !event.target || !event.target.value ) {
      //IE11 calls event second time after 'event.target.value = ""',
      //so it have to be silently ignored;
      return;
    }

    if ( !(window.File && window.FileReader && window.FileList && window.Blob) ) {
      alert( AppResources.get( "fileApiNotSupported_error" ) );
      event.target.value = "";
      return;
    }

    var file = event.target.files[ 0 ];
    if ( !file ) {
      alert( AppResources.get( "fileLoading_error" ) );
      event.target.value = "";
      return;
    }

    if ( file.size > (FredConfig.fileHandler.maxSizeInMB * 1024 * 1024) ) {
      alert( AppResources.get( "fileTooLarge_error", [ FredConfig.fileHandler.maxSizeInMB ] ) );
      event.target.value = "";
      return;
    }

    var reader = new FileReader();
    reader.onload = function ( e ) {
      if ( !Framsticks.loadCreaturesFromFileData( e.target.result, selectCreature ) ) {
        alert( AppResources.get( "invalidFileData_error" ) );
      }
      event.target.value = "";
    };

    reader.readAsText( file );
    this.fileName = file.name;
  };

  this.downloadFile = function( ) {
    var data = Framsticks.generateFileData();
    var blob = new Blob( [ data ], { type: "text/plain;charset=utf-8" } );
    saveAs( blob, this.fileName, true );
  };

}