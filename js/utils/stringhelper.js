/**
 * Simple implementation of string formating function based on String.Format() method from C#.
 * @param text String value that has to be formated.
 * @param args Array of objects used in formated text.
 * */
function stringFormat ( text, args ) {
  if( typeof text === "string" ) {
    if( toString.call( args ) === "[object Array]" ) {
     for( var i=0; i<args.length; ++i ) {
       text = text.replace( "{" + String(i) + "}", String( args[ i ] ) );
     }
    }
   return text;
  }
}
