var AppEvents = (function () {

  var events = {};

  function register(name, handler) {
    if(typeof name == "string" && typeof handler == "function") {
      if(!events[name]) {
        events[name] = [];
      }
      events[name].push(handler);
    }
  };

  function publish(name, data) {
    if(typeof name == "string" && events[name]) {
      for(var i=0; i<events[name].length; ++i) {
        if( typeof data !== "undefined" ) {
          events[name][i](data);
        } else {
          events[name][i]({});
        }
      }
    }
  };

  return {
    register: register,
    publish: publish
  };
}());
