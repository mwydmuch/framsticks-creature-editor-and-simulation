console.log("***** update rotation *****");
/* Init */
var logger = new Module.LoggerToMemory(Module.LoggerBase["Enable"] + Module.LoggerToMemory["StoreAllMessages"]);
var validators = new Module.Validators();
var g1 = new Module.Geno().useValidators(validators);
var modelValidator = new Module.ModelGenoValidator();
validators.append(modelValidator);
Module.destroy(g1);
/* End Init*/

var resultGeno = new Module.Geno();
var testGeno = "//0\np:sh=3,sx=0.05,sy=0.3,sz=0.3\np:sh=3,sx=0.05,sy=0.3,sz=0.3\nj:0,1,sh=1,dx=-0.5,dy=0.35,dz=0.1,ry=-1.0,rx=1.3\n";
//var testGeno = "//0\np:3,sh=3,sx=0.05,sy=0.3,sz=0.3\np:sh=3,sx=0.05,sy=0.3,sz=0.3\np:sh=3,sx=0.05,sy=0.3,sz=0.3\nj:0,1,sh=1,ry=-1.0,rx=1.3\nj:1,2,sh=1,dx=-0.5,dy=0.35,dz=0.1,ry=-1.0,rx=1.3\nj:0,2,sh=1,dx=-0.5,dy=0.35,dz=0.1,ry=-1.0,rx=1.3";

var str = new Module.SString();
str.set(testGeno);

var g2 = new Module.Geno(str);
var m = new Module.Model(g2, true);

console.log( "Output after model load." );
m.makeGeno(resultGeno); console.log(resultGeno.getGenes().c_str());
m.validate();           console.log( logger.getMessages().c_str() );
logger.reset();

m.open(); console.log( "m.open()" );

var p0 = m.getPart(0);
var p1 = m.getPart(1);
var j0 = m.getJoint(0);

var angles =  p0.get_o().getAngles();
console.log( "p0 angles = [ " + String(angles.get_x()) + "; " + String(angles.get_y()) + "; " + String(angles.get_z()) + " ]" );
angles =  p1.get_o().getAngles();
console.log( "p1 angles = [ " + String(angles.get_x()) + "; " + String(angles.get_y()) + "; " + String(angles.get_z()) + " ]" );

var rot = p0.get_rot();
rot.set_x(0.5);
p0.setRot( rot );
m.validate();

angles =  p0.get_o().getAngles();
console.log( "p0 angles = [ " + String(angles.get_x()) + "; " + String(angles.get_y()) + "; " + String(angles.get_z()) + " ]" );
angles =  p1.get_o().getAngles();
console.log( "p1 angles = [ " + String(angles.get_x()) + "; " + String(angles.get_y()) + "; " + String(angles.get_z()) + " ]" );
m.validate();

m.makeGeno(resultGeno);
console.log(resultGeno.getGenes().c_str());

m.close();

angles =  p0.get_o().getAngles();
console.log( "p0 angles = [ " + String(angles.get_x()) + "; " + String(angles.get_y()) + "; " + String(angles.get_z()) + " ]" );
angles =  p1.get_o().getAngles();
console.log( "p1 angles = [ " + String(angles.get_x()) + "; " + String(angles.get_y()) + "; " + String(angles.get_z()) + " ]" );

Module.destroy(m);
