var gcm = new Module.DefaultGenoConvManager();
gcm.addDefaultConverters();
var dummyGeno = new Module.Geno().useConverters(gcm);
Module.destroy(dummyGeno);

var validators = new Module.Validators();
var dummyGeno = new Module.Geno().useValidators(validators);
Module.destroy(dummyGeno);
var modelValidator = new Module.ModelGenoValidator();
validators.append(modelValidator);

var framsFileSystem = new Module.StdioFileSystem_autoselect();
var saveFileHelper = new Module.SaveFileHelper();

var f = saveFileHelper.Vfopen("/test.gen", "w");
var miniGeno = new Module.MiniGenotype();
var param = new Module.Param(saveFileHelper.getMinigenotype_paramtab(), miniGeno);
miniGeno.clear();

for (var i = 1; i <= 2; i++)
{
  miniGeno.get_name().set('Genotype#'+String(i));
  miniGeno.get_genotype().set('/0\np:');
  miniGeno.get_info().set('Test save.');
  param.save(f, "org");
}

Module.destroy(f);

var fileData = FS.readFile('/test.gen', { encoding: 'utf8' } ); //problem with polish characters 'ąćęłńóśżź'
console.log(fileData);
