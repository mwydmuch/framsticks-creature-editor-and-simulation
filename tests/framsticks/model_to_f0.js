console.log("***** model to f0 conversion *****");
/* Init */
var validators = new Module.Validators();
var g1 = new Module.Geno().useValidators(validators);
var modelValidator = new Module.ModelGenoValidator();
validators.append(modelValidator);
Module.destroy(g1);
/* End Init*/

var m = new Module.Model();
m.open();
var p = new Module.Part();

p.get_p().set_x(1);
p.get_p().set_y(2);
p.get_p().set_z(3);
m.addPart(p);
m.close();
console.log(m.getF0Geno().getGenes().c_str());

Module.destroy(m);