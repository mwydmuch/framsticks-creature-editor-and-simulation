console.log("***** f0 to model conversion *****");

/* Init */
var validators = new Module.Validators();
var g1 = new Module.Geno().useValidators(validators);
var modelValidator = new Module.ModelGenoValidator();
validators.append(modelValidator);
Module.destroy(g1);
/* End Init*/

var str = new Module.SString();
str.set("//0\nm:\np:\np:1,0,1\nj:0,1\nn:");

var g2 = new Module.Geno(str);
console.log("Format: " + String(g2.getFormat()-48)); // ASCI '0' == 48
console.log("Is geno valid?: " + (g2.isValid() ? "True" : "False") );

var m = new Module.Model(g2, true);
console.log("Is model valid?: " + (m.isValid() ? "True" : "False"));

console.log("Part count: " + String(m.getPartCount()));
console.log("Joint count: " + String(m.getJointCount()));
console.log("Neuro count: " + String(m.getNeuroCount()));

for(var i=0; i<m.getPartCount(); ++i)
{
	var p = m.getPart(i);
	console.log("Part " + String(i) + " position: " + String(p.get_p().get_x()) + ", " + String(p.get_p().get_y()) + ", " + String(p.get_p().get_z()) + ".");
}

Module.destroy(str);
Module.destroy(g2);
Module.destroy(m);