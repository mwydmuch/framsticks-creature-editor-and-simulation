function output (data) {
  console.log(data);
}

function noData (data) {
  console.log("noData handler called");
}

AppEvents.register("event1", output);
AppEvents.register("event1", output);
//bad input
AppEvents.register("event1", {});
AppEvents.register("event1", "bad handler");
AppEvents.register("event1", 1)

AppEvents.publish("event1", "event1 handler called"); //2

AppEvents.register("event2", output);
AppEvents.publish("event2", "event2 handler called"); //1

AppEvents.register("event1", output);
AppEvents.publish("event1", "event1 handler called"); //3

AppEvents.register("noData", noData);
AppEvents.publish("noData"); //1

