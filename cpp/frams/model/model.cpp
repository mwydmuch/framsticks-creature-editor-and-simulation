// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

#include <common/nonstd_math.h>
#include "model.h"
#include <common/log.h>
#include <frams/util/multimap.h>
#include <common/loggers/loggers.h>

Model::Model()
{
	autobuildmaps = false;
	init();
}

void Model::init()
{
	partmappingchanged = 0;
	buildstatus = empty;
	modelfromgenotype = 0;
	startenergy = 1.0;
	checklevel = 1;
#ifdef MODEL_V1_COMPATIBLE
	oldneurocount=-1; // == unknown
	oldconnections=0;
#endif
	map = 0;
	f0map = 0;
	f0genoknown = 1;
	shape = SHAPE_UNKNOWN;
}

void Model::moveElementsFrom(Model &source)
{
	int i;
	open();
	for (i = 0; i < source.getPartCount(); i++)
		addPart(source.getPart(i));
	for (i = 0; i < source.getJointCount(); i++)
		addJoint(source.getJoint(i));
	for (i = 0; i < source.getNeuroCount(); i++)
		addNeuro(source.getNeuro(i));
	source.parts.clear(); source.joints.clear(); source.neurons.clear();
	source.clear();
}

void Model::internalCopy(const Model &mod)
{
	geno = mod.geno;
	f0genoknown = 0;
	startenergy = mod.startenergy;
	if (mod.getStatus() == valid)
	{
		modelfromgenotype = mod.modelfromgenotype;
		{for (int i = 0; i < mod.getPartCount(); i++)
			addPart(new Part(*mod.getPart(i))); }
	{for (int i = 0; i < mod.getJointCount(); i++)
	{
		Joint *oldj = mod.getJoint(i);
		Joint *j = new Joint(*oldj);
		addJoint(j);
		j->attachToParts(oldj->part1->refno, oldj->part2->refno);
	}}
	{for (int i = 0; i < mod.getNeuroCount(); i++)
	{
		Neuro *oldn = mod.getNeuro(i);
		Neuro *n = new Neuro(*oldn);
		addNeuro(n);
		if (oldn->part_refno >= 0) n->attachToPart(oldn->part_refno);
		else n->attachToJoint(oldn->joint_refno);
	}}
		for (int i = 0; i < mod.getNeuroCount(); i++)
		{
			Neuro *oldn = mod.getNeuro(i);
			Neuro *n = getNeuro(i);
			for (int ni = 0; ni < oldn->getInputCount(); ni++)
			{
				double w;
				Neuro *oldinput = oldn->getInput(ni, w);
				SString info = n->getInputInfo(ni);
				n->addInput(getNeuro(oldinput->refno), w, &info);
			}
		}
	}
}


Model::Model(const Geno &src, bool buildmaps)
	:autobuildmaps(buildmaps)
{
	init(src);
}

void Model::operator=(const Model &mod)
{
	clear();
	open();
	internalCopy(mod);
	buildstatus = mod.buildstatus;
}

Model::Model(const Model &mod, bool buildmaps)
	:autobuildmaps(buildmaps)
{
	init();
	open();
	internalCopy(mod);
	buildstatus = mod.buildstatus;
}

void Model::init(const Geno &src)
{
	init();
	modelfromgenotype = 1;
	geno = src;
	build();
}

void Model::resetAllDelta()
{
	for (int i = 0; i < getJointCount(); i++)
		getJoint(i)->resetDelta();
}

void Model::useAllDelta(bool yesno)
{
	for (int i = 0; i < getJointCount(); i++)
		getJoint(i)->useDelta(yesno);
}

Model::~Model()
{
	delmodel_list.action((intptr_t)this);
	clear();
}

void Model::clear()
{
	FOREACH(Part*, p, parts)
		delete p;
	FOREACH(Joint*, j, joints)
		delete j;
	FOREACH(Neuro*, n, neurons)
		delete n;
	parts.clear(); joints.clear(); neurons.clear();
	delMap();
	delF0Map();
	init();
	geno = Geno();
	f0geno = Geno();
}

Part *Model::addPart(Part *p)
{
	p->owner = this;
	p->refno = parts.size();
	parts += p;
	return p;
}

Joint *Model::addJoint(Joint *j)
{
	j->owner = this;
	j->refno = joints.size();
	joints += j;
	return j;
}

Neuro *Model::addNeuro(Neuro *n)
{
	n->owner = this;
	n->refno = neurons.size();
	neurons += n;
	return n;
}

void Model::removeNeuros(SList &nlist)
{
	FOREACH(Neuro*, nu, nlist)
	{
		int i = findNeuro(nu);
		if (i >= 0) removeNeuro(i);
	}
}

void Model::removePart(int partindex, int removeattachedjoints, int removeattachedneurons)
{
	Part *p = getPart(partindex);
	if (removeattachedjoints)
	{
		SList jlist;
		findJoints(jlist, p);
		FOREACH(Joint*, j, jlist)
		{
			int i = findJoint(j);
			if (i >= 0) removeJoint(i, removeattachedneurons);
		}
	}
	if (removeattachedneurons)
	{
		SList nlist;
		findNeuros(nlist, 0, p);
		removeNeuros(nlist);
	}
	parts -= partindex;
	delete p;
}

void Model::removeJoint(int jointindex, int removeattachedneurons)
{
	Joint *j = getJoint(jointindex);
	if (removeattachedneurons)
	{
		SList nlist;
		findNeuros(nlist, 0, 0, j);
		removeNeuros(nlist);
	}
	joints -= jointindex;
	delete j;
}

void Model::removeNeuro(int neuroindex, bool removereferences)
{
	Neuro* thisN = getNeuro(neuroindex);

	if (removereferences)
	{
		Neuro* n;
		// remove all references to thisN
		for (int i = 0; n = (Neuro*)neurons(i); i++)
		{
			Neuro *inp;
			for (int j = 0; inp = n->getInput(j); j++)
				if (inp == thisN)
				{
				n->removeInput(j);
				j--;
				}
		}
	}

	neurons -= neuroindex;
	delete thisN;
}

MultiMap& Model::getMap()
{
	if (!map) map = new MultiMap();
	return *map;
}

void Model::delMap()
{
	if (map) { delete map; map = 0; }
}
void Model::delF0Map()
{
	if (f0map) { delete f0map; f0map = 0; }
}

void Model::makeGenToGenMap(MultiMap& result, const MultiMap& gen1tomodel, const MultiMap& gen2tomodel)
{
	result.clear();
	MultiMap m;
	m.addReversed(gen2tomodel);
	result.addCombined(gen1tomodel, m);
}

void Model::getCurrentToF0Map(MultiMap& result)
{
	result.clear();
	if (!map) return;
	const MultiMap& f0m = getF0Map();
	makeGenToGenMap(result, *map, f0m);
}

void Model::rebuild(bool buildm)
{
	autobuildmaps = buildm;
	clear();
	build();
}

void Model::initMap()
{
	if (!map) map = new MultiMap();
	else map->clear();
}

void Model::initF0Map()
{
	if (!f0map) f0map = new MultiMap();
	else f0map->clear();
}

void Model::build()
{
	f0errorposition = -1;
	f0warnposition = -1;
	MultiMap *convmap = autobuildmaps ? new MultiMap() : NULL;
	f0geno = (geno.getFormat() == '0') ? geno : geno.getConverted('0', convmap);
	f0genoknown = 1;
	if (f0geno.isInvalid())
	{
		buildstatus = invalid;
		if (convmap) delete convmap;
		return;
	}
	SString f0txt = f0geno.getGenes();
	buildstatus = building; // was: open();
	if (autobuildmaps)
	{
		partmappingchanged = 0;
		initMap();
		initF0Map();
	}
	int pos = 0, lnum = 1, lastpos = 0;
	SString line;
	MultiRange frommap;
	LoggerToMemory mh(LoggerBase::Enable | LoggerBase::DontBlock);
	for (; f0txt.getNextToken(pos, line, '\n'); lnum++)
	{
		if (autobuildmaps)
		{
			frommap.clear();
			frommap.add(lastpos, pos - 1);
		}
		mh.reset();
		if (singleStepBuild(line, lnum, autobuildmaps ? (&frommap) : 0) == -1)
		{
			buildstatus = invalid;
			f0errorposition = lastpos;
			if (convmap) delete convmap;
			return;
		}
		if (mh.getWarningCount())
		{
			if (f0warnposition < 0) f0warnposition = lastpos;
		}
		lastpos = pos;
	}
	mh.disable();
	close();
	if (convmap)
	{
		*f0map = *map;
		if (geno.getFormat() != '0')
		{
			MultiMap tmp;
			tmp.addCombined(*convmap, getMap());
			*map = tmp;
		}
		delete convmap;
	}
}

const MultiMap& Model::getF0Map()
{
	if (!f0map)
	{
		f0map = new MultiMap();
		makeGeno(f0geno, f0map);
		f0genoknown = 1;
	}
	return *f0map;
}

Geno Model::rawGeno()
{
	Geno tmpgen;
	makeGeno(tmpgen);
	return tmpgen;
}

void Model::makeGeno(Geno &g, MultiMap *map, bool handle_defaults)
{
	if ((buildstatus != valid) && (buildstatus != building))
	{
		g = Geno(0, 0, 0, "invalid model");
		return;
	}

	SString gen;

	Param modelparam(f0_model_paramtab);
	Param partparam(f0_part_paramtab);
	Param jointparam(f0_joint_paramtab);
	Param neuroparam(f0_neuro_paramtab);
	Param connparam(f0_neuroconn_paramtab);

	static Part defaultpart;
	static Joint defaultjoint;
	static Neuro defaultneuro;
	static Model defaultmodel;
	static NeuroConn defaultconn;
	//static NeuroItem defaultneuroitem;

	Part *p;
	Joint *j;
	Neuro *n;
	int i;
	int len;
	int a, b;
	//NeuroItem *ni;

	SString mod_props;
	modelparam.select(this);
	modelparam.save2(mod_props, handle_defaults ? &defaultmodel : NULL, true, !handle_defaults);
	if (mod_props.len() > 1) //are there any non-default values? ("\n" is empty)
	{
		gen += "m:";
		gen += mod_props;
	}

	for (i = 0; p = (Part*)parts(i); i++)
	{
		partparam.select(p);
		len = gen.len();
		gen += "p:";
		partparam.save2(gen, handle_defaults ? &defaultpart : NULL, true, !handle_defaults);
		if (map)
			map->add(len, gen.len() - 1, partToMap(i));
	}
	for (i = 0; j = (Joint*)joints(i); i++)
	{
		jointparam.select(j);
		len = gen.len();
		jointparam.setParamTab(j->usedelta ? f0_joint_paramtab : f0_nodeltajoint_paramtab);
		gen += "j:";
		jointparam.save2(gen, handle_defaults ? &defaultjoint : NULL, true, !handle_defaults);
		if (map)
			map->add(len, gen.len() - 1, jointToMap(i));
	}
	for (i = 0; n = (Neuro*)neurons(i); i++)
	{
		neuroparam.select(n);
		len = gen.len();
		gen += "n:";
		neuroparam.save2(gen, handle_defaults ? &defaultneuro : NULL, true, !handle_defaults);
		if (map)
			map->add(len, gen.len() - 1, neuroToMap(i));
	}
	for (a = 0; a < neurons.size(); a++)
	{ // inputs
		n = (Neuro*)neurons(a);
		//	if ((n->getInputCount()==1)&&(n->getInput(0).refno <= n->refno))
		//		continue; // already done with Neuro::conn_refno

		for (b = 0; b < n->getInputCount(); b++)
		{
			double w;
			NeuroConn nc;
			Neuro* n2 = n->getInput(b, w);
			//		if (((n2.parentcount==1)&&(n2.parent)&&(n2.parent->refno < n2.refno)) ^ 
			//		    (n2.neuro_refno>=0))
			//			printf("!!!! bad Neuro::neuro_refno ?!\n");

			//		if ((n2.parentcount==1)&&(n2.parent)&&(n2.parent->refno < n2.refno))
			//		if (n2.neuro_refno>=0)
			//			continue; // already done with Neuro::neuro_refno

			nc.n1_refno = n->refno; nc.n2_refno = n2->refno;
			nc.weight = w;
			SString **s = n->inputInfo(b);
			if ((s) && (*s))
				nc.info = **s;
			connparam.select(&nc);
			len = gen.len();
			gen += "c:";
			connparam.save2(gen, handle_defaults ? &defaultconn : NULL, true, !handle_defaults);
			if (map)
				map->add(len, gen.len() - 1, neuroToMap(n->refno));
		}
	}
	g = Geno(gen.c_str(), '0');
}

//////////////

void Model::open()
{
	if (buildstatus == building) return;
	buildstatus = building;
	modelfromgenotype = 0;
	partmappingchanged = 0;
	f0genoknown = 0;
	delMap();
}

void Model::checkpoint()
{}

void Model::setGeno(const Geno& newgeno)
{
	geno = newgeno;
}

void Model::clearMap()
{
	Part *p; Joint *j; Neuro *n;
	int i;
	delMap();
	delF0Map();
	for (i = 0; p = (Part*)parts(i); i++)
		p->clearMapping();
	for (i = 0; j = (Joint*)joints(i); i++)
		j->clearMapping();
	for (i = 0; n = (Neuro*)neurons(i); i++)
		n->clearMapping();
}

int Model::close(bool building_live_model)
{
	if (buildstatus != building)
		logPrintf("Model", "close", LOG_WARN, "Unexpected close() - no open()");
	if (internalcheck(building_live_model ? LIVE_CHECK : FINAL_CHECK) > 0)
	{
		buildstatus = valid;

		if (partmappingchanged)
		{
			getMap();
			Part *p; Joint *j; Neuro *n;
			int i;
			for (i = 0; p = (Part*)parts(i); i++)
				if (p->getMapping())
					map->add(*p->getMapping(), partToMap(i));
			for (i = 0; j = (Joint*)joints(i); i++)
				if (j->getMapping())
					map->add(*j->getMapping(), jointToMap(i));
			for (i = 0; n = (Neuro*)neurons(i); i++)
				if (n->getMapping())
					map->add(*n->getMapping(), neuroToMap(i));
		}
	}
	else
		buildstatus = invalid;

	return (buildstatus == valid);
}

int Model::validate()
{
	return internalcheck(EDITING_CHECK);
}

Pt3D Model::whereDelta(const Part& start, const Pt3D& rot, const Pt3D& delta)
{
	Orient roto;
	roto = rot;
	Orient o;
	roto.transform(o, start.o);
	//o.x=start.o/roto.x;
	//o.y=start.o/roto.y;
	//o.z=start.o/roto.z;
	return o.transform(delta) + start.p;
}

int Model::singleStepBuild(const SString &singleline, const MultiRange* srcrange)
{
	return singleStepBuild(singleline, 0, srcrange);
}

int Model::singleStepBuild(const SString &singleline, int line_num, const MultiRange* srcrange)
{
	SString error_message;
	int result = singleStepBuildNoLog(singleline, error_message, srcrange);
	if (result < 0)
	{
		if (error_message.len() == 0) // generic error when no detailed message is available
			error_message = "Invalid f0 code";
		if (line_num>0)
			error_message += SString::sprintf(", line #%d", line_num);
		error_message += nameForErrors();
		logPrintf("Model", "build", LOG_ERROR, "%s", error_message.c_str());
	}
	return result;
}

int Model::singleStepBuildNoLog(const SString &line, SString& error_message, const MultiRange* srcrange)
{
	error_message = SString::empty();
	int pos = 0; const char*t = line.c_str();
	for (; *t; t++, pos++)
		if (!strchr(" \r\t", *t)) break;
	if (*t == '#') return 0;
	if (!*t) return 0;
	if (!strncmp(t, "p:", 2))
	{
		Param partparam(f0_part_paramtab);
		Part *p = new Part();
		partparam.select(p);
		pos += 2;
		if (partparam.load2(line, pos) & ParamInterface::LOAD2_PARSE_FAILED) { delete p; error_message = "Invalid 'p:'"; return -1; }
		p->o.rotate(p->rot);
		parts += p;
		p->owner = this;
		if (srcrange) p->setMapping(*srcrange);
		return getPartCount() - 1;
	}
	if (!strncmp(t, "m:", 2))
	{
		Param modelparam(f0_model_paramtab);
		modelparam.select(this);
		pos += 2;
		if (modelparam.load2(line, pos) & ParamInterface::LOAD2_PARSE_FAILED) { error_message = "Invalid 'm:'"; return -1; }
		return 0;
	}
	else if (!strncmp(t, "j:", 2))
	{
		Param jointparam(f0_joint_paramtab);
		Joint *j = new Joint();
		jointparam.select(j);
		pos += 2;
		j->owner = this;
		if (jointparam.load2(line, pos) & ParamInterface::LOAD2_PARSE_FAILED) { delete j; error_message = "Invalid 'j:'"; return -1; }
		bool p1_ok = false, p2_ok = false;
		if ((p1_ok = ((j->p1_refno >= 0) && (j->p1_refno < getPartCount()))) &&
			(p2_ok = ((j->p2_refno >= 0) && (j->p2_refno < getPartCount()))))
		{
			addJoint(j);
			if ((j->d.x != JOINT_DELTA_MARKER) || (j->d.y != JOINT_DELTA_MARKER) || (j->d.z != JOINT_DELTA_MARKER))
			{
				j->useDelta(1);
				j->resetDeltaMarkers();
			}
			j->attachToParts(j->p1_refno, j->p2_refno);
			if (srcrange) j->setMapping(*srcrange);
			return j->refno;
		}
		else
		{
			error_message = SString::sprintf("Invalid reference to Part #%d", p1_ok ? j->p1_refno : j->p2_refno);
			delete j;
			return -1;
		}
	}
	else if (!strncmp(t, "n:", 2)) // neuro (or the old neuro object as the special case)
	{
		Param neuroparam(f0_neuro_paramtab);
		Neuro *nu = new Neuro();
		neuroparam.select(nu);
		pos += 2;
		if (neuroparam.load2(line, pos) & ParamInterface::LOAD2_PARSE_FAILED) { delete nu; error_message = "Invalid 'n:'"; return -1; }
#ifdef MODEL_V1_COMPATIBLE
		if (nu->neuro_refno>=0) // parent specified...
		{
			if (nu->neuro_refno >= getNeuroCount()) // and it's illegal
			{
				delete nu;
				return -1;
			}
			Neuro *parentNU=getNeuro(nu->neuro_refno);
			parentNU->addInput(nu,nu->weight);
			// default class for parented units: n-n link
			//if (nu->moredata.len()==0) nu->moredata="-";
		}
		else
#endif
		{
			// default class for unparented units: standard neuron
			if (nu->getClassName().len() == 0) nu->setClassName("N");
		}
		/*
			if (nu->conn_refno>=0) // input specified...
			{
			if (nu->conn_refno >= getNeuroCount()) // and it's illegal
			{
			delete nu;
			return -1;
			}
			Neuro *inputNU=getNeuro(nu->conn_refno);
			nu->addInput(inputNU,nu->weight);
			}
			*/
#ifdef MODEL_V1_COMPATIBLE
		nu->weight=1.0;
#endif
		nu->owner = this;
		// attach to part/joint
		if (nu->part_refno >= 0)
			{
			nu->attachToPart(nu->part_refno);
			if (nu->part==NULL)
				{ error_message = SString::sprintf("Invalid reference to Part #%d", nu->part_refno); delete nu; return -1; }
			}
		if (nu->joint_refno >= 0)
			{
			nu->attachToJoint(nu->joint_refno);
			if (nu->joint==NULL)
				{ error_message = SString::sprintf("Invalid reference to Joint #%d", nu->joint_refno); delete nu; return -1; }
			}
		if (srcrange) nu->setMapping(*srcrange);
		// todo: check part/joint ref# 
#ifdef MODEL_V1_COMPATIBLE
		if (hasOldNeuroLayout())
		{
			int count=old_getNeuroCount();
			neurons.insert(count,nu);
			oldneurocount=count+1;
			return oldneurocount-1;
		}
		else
#endif
		{
			neurons += nu;
			return neurons.size() - 1;
		}
	}
	else if (!strncmp(t, "c:", 2)) // add input
	{
		Param ncparam(f0_neuroconn_paramtab);
		NeuroConn c;
		ncparam.select(&c);
		pos += 2;
		if (ncparam.load2(line, pos) & ParamInterface::LOAD2_PARSE_FAILED) { error_message = "Invalid 'c:'"; return -1; }
		bool n1_ok = false, n2_ok = false;
		if ((n1_ok = ((c.n1_refno >= 0) && (c.n1_refno < getNeuroCount())))
			&& (n2_ok = ((c.n2_refno >= 0) && (c.n2_refno < getNeuroCount()))))
		{
			Neuro *na = getNeuro(c.n1_refno);
			Neuro *nb = getNeuro(c.n2_refno);
			na->addInput(nb, c.weight, &c.info);
			if (srcrange)
				na->addMapping(*srcrange);
			return 0;
		}
		error_message = SString::sprintf("Invalid reference to Neuro #%d", n1_ok ? c.n2_refno : c.n1_refno);
		return -1;
	}
#ifdef MODEL_V1_COMPATIBLE
	else if (!strncmp(t,"ni:",3)) // old neuroitem
	{
		// we always use old layout for "ni:"
		Param neuroitemparam(f0_neuroitem_paramtab);
		Neuro *nu=new Neuro();
		neuroitemparam.select(nu);
		pos+=3;
		if (neuroitemparam.load2(line,pos) & ParamInterface::LOAD2_PARSE_FAILED) {delete nu; return -1;}
		// illegal parent?
		if ((nu->neuro_refno<0)||(nu->neuro_refno>=old_getNeuroCount()))
		{
			delete nu;
			return -1;
		}
		Neuro *parentN=getNeuro(nu->neuro_refno);
		// copy part/joint refno from parent, if possible
		if ((nu->part_refno<0)&&(parentN->part_refno>=0))
			nu->part_refno=parentN->part_refno;
		if ((nu->joint_refno<0)&&(parentN->joint_refno>=0))
			nu->joint_refno=parentN->joint_refno;
		nu->owner=this;
		// attach to part/joint
		if (nu->part_refno>=0)
			nu->attachToPart(nu->part_refno);
		if (nu->joint_refno>=0)
			nu->attachToJoint(nu->joint_refno);
		if (srcrange)
			nu->setMapping(*srcrange);
		// special case: old muscles
		// PARENT neuron will be set up to be the CHILD of the current one (!)
		if (nu->isOldEffector())
		{
			nu->neuro_refno=parentN->refno;
			neurons+=nu;
			nu->owner=this;
			nu->addInput(parentN,nu->weight); // (!)
			nu->weight=1.0;
			parentN->invalidateOldItems();
			return 0; // !!! -> ...getItemCount()-1;
		}
		parentN->addInput(nu,nu->weight);
		neurons+=nu;
		parentN->invalidateOldItems();
		if (nu->getClassName().len()==0)
		{
			nu->setClassName("-");
			// for compatibility, "ni" can define a n-n connection
			// referring to non-existent neuron (which will be hopefully defined later)
			// internal check will add the proper input to this unit
			// if conn_refno >=0 and input count==0
			oldconnections=1;
			if (srcrange)
				parentN->addMapping(*srcrange);
		}
		else
			nu->weight=1.0;
		return 0; // !!! -> ...getItemCount()-1;
	}
#endif
	else return -1;
}

#ifdef MODEL_V1_COMPATIBLE
int Model::addOldConnectionsInputs()
{
	if (!oldconnections) return 1;
	Neuro* n;
	for (int i=0;i<neurons.size();i++)
	{
		n=(Neuro*)neurons(i);
		if (n->conn_refno>=0)
			if (n->isNNConnection())
				if (n->conn_refno < old_getNeuroCount())
				{ // good reference
			Neuro* parent=n->parent; // nn connection has exactly one parent
			int inp=parent->findInput(n);
			Neuro* target=getNeuro(n->conn_refno);
			parent->setInput(inp,target,n->weight);
			removeNeuro(i,0); // no need to remove references
			i--;
				}
				else
				{
					logPrintf("Model","internalCheck",LOG_ERROR,
						"illegal N-N connection #%d (reference to #%d)%s",
						i,n->conn_refno,nameForErrors().c_str());
					return 0;
				}
	}
	oldconnections=0;
	return 1;
}
#endif

/////////////

/** change the sequence of neuro units
	and fix references in "-" objects (n-n connections)  */
void Model::moveNeuro(int oldpos, int newpos)
{
	if (oldpos == newpos) return; // nop!
	Neuro *n = getNeuro(oldpos);
	neurons -= oldpos;
	neurons.insert(newpos, n);
	// conn_refno could be broken -> fix it
#ifdef MODEL_V1_COMPATIBLE
	for (int i=0;i<neurons.size();i++)
	{
		Neuro *n2=getNeuro(i);
		if (n2->isNNConnection())
			if (n2->conn_refno == oldpos) n2->conn_refno=newpos;
			else
			{ if (n2->conn_refno > oldpos) n2->conn_refno--;
		if (n2->conn_refno >= newpos) n2->conn_refno++; }
	}
	invalidateOldNeuroCount();
#endif
}

#ifdef MODEL_V1_COMPATIBLE
/** move all old neurons (class "N") to the front of the neurons list.
	@return number of old neurons
	*/
int Model::reorderToOldLayout()
{
	Neuro *n;
	int neurocount=0;
	for (int i=0;i<neurons.size();i++)
	{
		n=(Neuro*)neurons(i);
		if (n->isOldNeuron())
		{
			moveNeuro(i,neurocount);
			neurocount++;
			i=neurocount-1;
		}
	}
	return neurocount;
}
#endif
////////////

void Model::updateNeuroRefno()
{
	for (int i = 0; i < neurons.size(); i++)
	{
		Neuro* n = (Neuro*)neurons(i);
		n->refno = i;
	}
}

#define VALIDMINMAX(var,template,field) \
if (var -> field < getMin ## template () . field) \
	{ var->field= getMin ## template () . field; \
	logPrintf("Model","internalCheck",LOG_WARN,# field " too small in " # template " #%d (adjusted)",i);} \
else if (var -> field > getMax ## template () . field) \
	{ var->field= getMax ## template ()  . field; \
	logPrintf("Model","internalCheck",LOG_WARN,# field " too big in " # template " #%d (adjusted)",i);}

#define LINKFLAG 0x8000000

void Model::updateRefno()
{
	int i;
	for (i = 0; i < getPartCount(); i++) getPart(i)->refno = i;
	for (i = 0; i < getJointCount(); i++) getJoint(i)->refno = i;
	for (i = 0; i < getNeuroCount(); i++) getNeuro(i)->refno = i;
}

SString Model::nameForErrors() const
{
	if (geno.getName().len()>0)
		return SString::sprintf(" in '%s'", geno.getName().c_str());
	return SString::empty();
}

int Model::internalcheck(CheckType check)
{
	Part *p;
	Joint *j;
	Neuro *n;
	int i, k;
	int ret = 1;
	shape = SHAPE_UNKNOWN;
	if ((parts.size() == 0) && (neurons.size() == 0)) return 0;
	if (parts.size() == 0)
		size = Pt3D_0;
	else
	{
		Pt3D bbmin = ((Part*)parts(0))->p, bbmax = bbmin;
		for (i = 0; i < parts.size(); i++)
		{
			p = (Part*)parts(i);
			p->owner = this;
			p->refno = i;
			if (checklevel > 0)
				p->mass = 0.0;
			//VALIDMINMAX(p,part,mass);//mass is very special
			// VALIDMINMAX are managed manually when adding part properties in f0-def!
			// (could be made dynamic but not really worth the effort)
			VALIDMINMAX(p, Part, size);
			VALIDMINMAX(p, Part, scale.x);
			VALIDMINMAX(p, Part, scale.y);
			VALIDMINMAX(p, Part, scale.z);
			VALIDMINMAX(p, Part, hollow);
			VALIDMINMAX(p, Part, density);
			VALIDMINMAX(p, Part, friction);
			VALIDMINMAX(p, Part, ingest);
			VALIDMINMAX(p, Part, assim);
			VALIDMINMAX(p, Part, vsize);
			VALIDMINMAX(p, Part, vcolor.x);
			VALIDMINMAX(p, Part, vcolor.y);
			VALIDMINMAX(p, Part, vcolor.z);
			p->flags &= ~LINKFLAG; // for delta joint cycle detection
			if (p->p.x - p->size < bbmin.x) bbmin.x = p->p.x - p->size;
			if (p->p.y - p->size < bbmin.y) bbmin.y = p->p.y - p->size;
			if (p->p.z - p->size < bbmin.z) bbmin.z = p->p.z - p->size;
			if (p->p.x + p->size > bbmax.x) bbmax.x = p->p.x + p->size;
			if (p->p.y + p->size > bbmax.y) bbmax.y = p->p.y + p->size;
			if (p->p.z + p->size > bbmax.z) bbmax.z = p->p.z + p->size;
			if (shape == SHAPE_UNKNOWN)
				shape = (p->shape == Part::SHAPE_BALL_AND_STICK) ? SHAPE_BALL_AND_STICK : SHAPE_SOLIDS;
			else if (shape != SHAPE_ILLEGAL)
			{
				if ((p->shape == Part::SHAPE_BALL_AND_STICK) ^ (shape == SHAPE_BALL_AND_STICK))
				{
					shape = SHAPE_ILLEGAL;
					logPrintf("Model", "internalCheck", LOG_WARN, "Inconsistent part shapes (mixed ball-and-stick and solids shape types)%s", nameForErrors().c_str());
				}
			}
		}
		size = bbmax - bbmin;
		for (i = 0; i < joints.size(); i++)
		{
			j = (Joint*)joints(i);
			// VALIDMINMAX are managed manually when adding joint properties in f0-def!
			// (could be made dynamic but not really worth the effort)
			VALIDMINMAX(j, Joint, stamina);
			VALIDMINMAX(j, Joint, stif);
			VALIDMINMAX(j, Joint, rotstif);
			VALIDMINMAX(p, Part, vcolor.x);
			VALIDMINMAX(p, Part, vcolor.y);
			VALIDMINMAX(p, Part, vcolor.z);
			j->refno = i;
			j->owner = this;
			if (j->part1 && j->part2 && (j->part1 != j->part2))
			{
				j->p1_refno = j->part1->refno;
				j->p2_refno = j->part2->refno;
				if (checklevel > 0)
				{
					j->part1->mass += 1.0;
					j->part2->mass += 1.0;
				}
				if ((j->usedelta) && ((j->d.x != JOINT_DELTA_MARKER) || (j->d.y != JOINT_DELTA_MARKER) || (j->d.z != JOINT_DELTA_MARKER)))
				{ // delta positioning -> calc. orient.
					if (j->part2->flags & LINKFLAG)
					{
						ret = 0;
						logPrintf("Model", "internalCheck", LOG_ERROR,
							"Delta joint cycle detected at Joint #%d%s",
							i, nameForErrors().c_str());
					}
					j->resetDeltaMarkers();
					j->o = j->rot;
					j->part1->o.transform(j->part2->o, j->o);
					//			j->part2->o.x=j->part1->o/j->o.x;
					//			j->part2->o.y=j->part1->o/j->o.y;
					//			j->part2->o.z=j->part1->o/j->o.z;
					j->part2->p = j->part2->o.transform(j->d) + j->part1->p;
					j->part2->flags |= LINKFLAG; j->part1->flags |= LINKFLAG; // for delta joint cycle detection
				}
				else
				{ // abs.positioning -> calc. delta
					if (check != EDITING_CHECK)
					{
						// calc orient delta
						//			Orient tmpo(j->part2->o);
						//			tmpo*=j->part1->o;
						Orient tmpo;
						j->part1->o.revTransform(tmpo, j->part2->o);
						tmpo.getAngles(j->rot);
						j->o = j->rot;
						// calc position delta
						Pt3D tmpp(j->part2->p);
						tmpp -= j->part1->p;
						j->d = j->part2->o.revTransform(tmpp);
					}
				}
				if (check != LIVE_CHECK)
				{
					if (j->shape != Joint::SHAPE_FIXED)
					{
						if (j->d() > getMaxJoint().d.x)
						{
							ret = 0;
							logPrintf("Model", "internalCheck", LOG_ERROR, "Joint #%d too long (its length %g exceeds allowed %g)%s", i, j->d(), getMaxJoint().d.x, nameForErrors().c_str());
						}
					}
				}
			}
			else
			{
				logPrintf("Model", "internalCheck", LOG_ERROR, "Illegal part references in Joint #%d%s", i, nameForErrors().c_str());
				ret = 0;
			}
			if (shape != SHAPE_ILLEGAL)
			{
				if ((j->shape == Joint::SHAPE_BALL_AND_STICK) ^ (shape == SHAPE_BALL_AND_STICK))
				{
					shape = SHAPE_ILLEGAL;
					logPrintf("Model", "internalCheck", LOG_WARN, "Inconsistent joint shapes (mixed old and new shapes)%s", nameForErrors().c_str());
				}
			}
		}
	}
#ifdef MODEL_V1_COMPATIBLE
	if (!addOldConnectionsInputs())
		return 0;
#endif

	updateNeuroRefno(); // valid refno is important for n-n connections check (later)

	for (i = 0; i < neurons.size(); i++)
	{
		n = (Neuro*)neurons(i);
#ifdef MODEL_V1_COMPATIBLE
		VALIDMINMAX(n,Neuro,inertia);
		VALIDMINMAX(n,Neuro,force);
		VALIDMINMAX(n,Neuro,sigmo);
		n->conn_refno=-1;
		n->weight=1.0;
		n->neuro_refno=-1;
#endif
		n->part_refno = (n->part) ? n->part->refno : -1;
		n->joint_refno = (n->joint) ? n->joint->refno : -1;
	}

	if (parts.size() && (checklevel > 0))
	{
		for (i = 0; i < parts.size(); i++)
		{
			p = (Part*)parts(i);
			if (p->mass <= 0.001)
				p->mass = 1.0;
			p->flags &= ~LINKFLAG;
		}
		getPart(0)->flags |= LINKFLAG;
		int change = 1;
		while (change)
		{
			change = 0;
			for (i = 0; i < joints.size(); i++)
			{
				j = (Joint*)joints(i);
				if (j->part1->flags&LINKFLAG)
				{
					if (!(j->part2->flags&LINKFLAG))
					{
						change = 1;
						j->part2->flags |= LINKFLAG;
					}
				}
				else
					if (j->part2->flags&LINKFLAG)
					{
					if (!(j->part1->flags&LINKFLAG))
					{
						change = 1;
						j->part1->flags |= LINKFLAG;
					}
					}
			}
		}
		for (i = 0; i < parts.size(); i++)
		{
			p = (Part*)parts(i);
			if (!(p->flags&LINKFLAG))
			{
				logPrintf("Model", "internalCheck", LOG_ERROR, "Not all parts connected (eg. Part #0 and Part #%d)%s", i, nameForErrors().c_str());
				ret = 0;
				break;
			}
		}
	}

	for (i = 0; i < joints.size(); i++)
	{
		j = (Joint*)joints(i);
		if (j->p1_refno == j->p2_refno)
		{
			logPrintf("Model", "internalCheck", LOG_ERROR, "Illegal self connection, Joint #%d%s", i, nameForErrors().c_str());
			ret = 0;
			break;
		}
		for (k = i + 1; k < joints.size(); k++)
		{
			Joint* j2 = (Joint*)joints(k);
			if (((j->p1_refno == j2->p1_refno) && (j->p2_refno == j2->p2_refno))
				|| ((j->p1_refno == j2->p2_refno) && (j->p2_refno == j2->p1_refno)))
			{
				logPrintf("Model", "internalCheck", LOG_ERROR, "Illegal duplicate Joint #%d and Joint #%d%s", i, k, nameForErrors().c_str());
				ret = 0;
				break;
			}
		}
	}
	if (shape == SHAPE_ILLEGAL)
		ret = 0;
	return ret;
}

/////////////

int Model::getErrorPosition(bool includingwarnings)
{
	return includingwarnings ?
		((f0errorposition >= 0) ? f0errorposition : f0warnposition)
		:
		f0errorposition;
}

const Geno& Model::getGeno() const
{
	return geno;
}

const Geno Model::getF0Geno()
{
	if (buildstatus == building)
		logPrintf("Model", "getGeno", LOG_WARN, "Model was not completed - missing close()");
	if (buildstatus != valid)
		return Geno("", '0', "", "invalid");
	if (!f0genoknown)
	{
		if (autobuildmaps)
		{
			initF0Map();
			makeGeno(f0geno, f0map);
		}
		else
		{
			delF0Map();
			makeGeno(f0geno);
		}
		f0genoknown = 1;
	}
	return f0geno;
}

int Model::getPartCount() const
{
	return parts.size();
}

Part* Model::getPart(int i) const
{
	return ((Part*)parts(i));
}

int Model::getJointCount() const
{
	return joints.size();
}

Joint* Model::getJoint(int i) const
{
	return ((Joint*)joints(i));
}

int Model::findJoints(SList& result, const Part* part)
{
	Joint *j;
	int n0 = result.size();
	if (part)
		for (int i = 0; j = (Joint*)joints(i); i++)
			if ((j->part1 == part) || (j->part2 == part)) result += (void*)j;
	return result.size() - n0;
}

int Model::findNeuro(Neuro* n)
{
	return neurons.find(n);
}

int Model::findPart(Part* p)
{
	return parts.find(p);
}

int Model::findJoint(Joint* j)
{
	return joints.find(j);
}

int Model::findJoint(Part *p1, Part *p2)
{
	Joint* j;
	for (int i = 0; j = getJoint(i); i++)
		if ((j->part1 == p1) && (j->part2 == p2)) return i;
	return -1;
}


////////////////////

#ifdef MODEL_V1_COMPATIBLE
void Model::calcOldNeuroCount()
{
	if (oldneurocount>=0) return;
	oldneurocount=reorderToOldLayout();
}

int Model::old_getNeuroCount()
{ calcOldNeuroCount();
return oldneurocount;}

Neuro* Model::old_getNeuro(int i) 
{calcOldNeuroCount();
return (i<oldneurocount)? (Neuro*)getNeuro(i) : (Neuro*)0;
}

int Model::old_findNeuro(Neuro* n)
{calcOldNeuroCount();
return findNeuro(n);}

Neuro *Model::old_addNewNeuro()
{
	int count=old_getNeuroCount();
	Neuro *nu=addNewNeuro();
	nu->setClassName("N");
	moveNeuro(nu->refno,oldneurocount);
	oldneurocount=count+1;
	return (Neuro*)nu;
}
#endif

///////////////////////

int Model::getNeuroCount() const
{
	return neurons.size();
}

Neuro* Model::getNeuro(int i) const
{
	return (Neuro*)neurons(i);
}

int Model::getConnectionCount() const
{
	int n = 0;
	for (int i = 0; i < getNeuroCount(); i++)
		n += getNeuro(i)->getInputCount();
	return n;
}

int Model::findNeuros(SList& result,
	const char* classname, const Part* part, const Joint* joint)
{
	Neuro *nu;
	SString cn(classname);
	int n0 = result.size();
	for (int i = 0; nu = (Neuro*)neurons(i); i++)
	{
		if (part)
			if (nu->part != part) continue;
		if (joint)
			if (nu->joint != joint) continue;
		if (classname)
			if (nu->getClassName() != cn) continue;
		result += (void*)nu;
	}
	return result.size() - n0;
}

///////////////////

void Model::disturb(double amount)
{
	int i;
	if (amount <= 0) return;
	for (i = 0; i < parts.size(); i++)
	{
		Part *p = getPart(i);
		p->p.x += (rnd01 - 0.5)*amount;
		p->p.y += (rnd01 - 0.5)*amount;
		p->p.z += (rnd01 - 0.5)*amount;
	}
	for (i = 0; i < joints.size(); i++)
	{
		Joint *j = getJoint(i);
		Pt3D tmpp(j->part2->p);
		tmpp -= j->part1->p;
		j->d = j->part2->o.revTransform(tmpp);
	}
}

void Model::move(const Pt3D& shift)
{
	FOREACH(Part*, p, parts)
		p->p += shift;
}

void Model::rotate(const Orient& rotation)
{
	FOREACH(Part*, p, parts)
	{
		p->p = rotation.transform(p->p);
		p->setOrient(rotation.transform(p->o));
	}
}

void Model::buildUsingSolidShapeTypes(const Model& src_ballandstick_shapes, Part::Shape use_shape, float thickness)
{
	for (int i = 0; i < src_ballandstick_shapes.getJointCount(); i++)
	{
		Joint *oj = src_ballandstick_shapes.getJoint(i);
		Part *p = addNewPart(use_shape);
		p->p = (oj->part1->p + oj->part2->p) / 2;
		Orient o;
		o.lookAt(oj->part1->p - oj->part2->p);
		p->rot = o.getAngles();
		p->scale.x = oj->part1->p.distanceTo(oj->part2->p) / 2;
		p->scale.y = thickness;
		p->scale.z = thickness;
	}
	for (int i = 0; i < src_ballandstick_shapes.getPartCount(); i++)
	{
		Part *op = src_ballandstick_shapes.getPart(i);
		for (int j = 0; j < src_ballandstick_shapes.getJointCount(); j++)
		{
			Joint *oj = src_ballandstick_shapes.getJoint(j);
			if ((oj->part1 == op) || (oj->part2 == op))
			{
				for (int j2 = j + 1; j2 < src_ballandstick_shapes.getJointCount(); j2++)
				{
					Joint *oj2 = src_ballandstick_shapes.getJoint(j2);
					if ((oj2->part1 == op) || (oj2->part2 == op))
					{
						addNewJoint(getPart(j), getPart(j2), Joint::SHAPE_FIXED);
					}
				}
				break;
			}
		}
	}
}

SolidsShapeTypeModel::SolidsShapeTypeModel(const Model& m, Part::Shape use_shape, float thickness)
{
	using_model = converted_model = NULL;
	if (m.getShapeType() == Model::SHAPE_BALL_AND_STICK)
	{
		converted_model = new Model;
		converted_model->open();
		converted_model->buildUsingSolidShapeTypes(m, use_shape, thickness);
		converted_model->close();
		using_model = converted_model;
	}
	else
	{
		converted_model = NULL;
		using_model = &m;
	}
}

//////////////////////

class MinPart : public Part { public: MinPart() { Param par(f0_part_paramtab, this); par.setMin(); } };
class MaxPart : public Part { public: MaxPart() { Param par(f0_part_paramtab, this); par.setMax(); } };
class MinJoint : public Joint { public: MinJoint() { Param par(f0_joint_paramtab, this); par.setMin(); } };
class MaxJoint : public Joint { public: MaxJoint() { Param par(f0_joint_paramtab, this); par.setMax(); } };
class MinNeuro : public Neuro { public: MinNeuro() { Param par(f0_neuro_paramtab, this); par.setMin(); } };
class MaxNeuro : public Neuro { public: MaxNeuro() { Param par(f0_neuro_paramtab, this); par.setMax(); } };

Part& Model::getMinPart() { static MinPart part; return part; }
Part& Model::getMaxPart() { static MaxPart part; return part; }
Part& Model::getDefPart() { static Part part; return part; }
Joint& Model::getMinJoint() { static MinJoint joint; return joint; }
Joint& Model::getMaxJoint() { static MaxJoint joint; return joint; }
Joint& Model::getDefJoint() { static Joint joint; return joint; }
Neuro& Model::getMinNeuro() { static MinNeuro neuro; return neuro; }
Neuro& Model::getMaxNeuro() { static MaxNeuro neuro; return neuro; }
Neuro& Model::getDefNeuro() { static Neuro neuro; return neuro; }
