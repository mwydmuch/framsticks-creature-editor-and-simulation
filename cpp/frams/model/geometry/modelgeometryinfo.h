// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

#ifndef _MODELGEOMETRYINFO_H_
#define _MODELGEOMETRYINFO_H_

#include <frams/model/model.h>
#include <frams/util/3d.h>
#include <frams/util/list.h>

namespace ModelGeometryInfo
{
	void findSizesAndAxesOfModel(const Model &model, const double density, Pt3D &sizes,
		Orient &axes);
	bool boundingBox(const Model &model, Pt3D &lowerBoundary, Pt3D &upperBoundary);
	void boundingBox(const Part *part, Pt3D &lowerBoundary, Pt3D &upperBoundary);
	double volume(const Model &model, const double density);
	double area(const Model &model, const double density);
	double externalAreaOfPart(const Model &model, const int partIndex, const double density);
	double externalAreaOfEllipsoid(const Model &model, const int partIndex, const double density);
	double externalAreaOfCuboid(const Model &model, const int partIndex, const double density);
	double externalAreaOfCylinder(const Model &model, const int partIndex, const double density);
}

#endif
