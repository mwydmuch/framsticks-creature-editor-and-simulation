// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

#ifndef _AUTONAME_H_
#define _AUTONAME_H_

#include <frams/model/model.h>

/// generate human readable (and human like-able) name for the model
class AutoName
{
public:
	static SString makeName(Model&);
	static SString firstName(const SString& g);
	static SString firstName(Model&);
	static SString lastName(Model&);
};

#endif

