// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

#ifndef _FF_GENOTYPE_H_
#define _FF_GENOTYPE_H_

#include <stdio.h>
#include "common/stl-util.h"
#include <frams/param/param.h>

//Growth parameters; see http://www.framsticks.com/foraminifera
struct fF_growth_params
{
	int number_of_chambers;
	double scalex, scaley, scalez;
	double translation;
	double angle1, angle2;

	static ParamEntry paramtab[];
	Param param;

	fF_growth_params()
		:param(paramtab, this)
	{
		reset();
	}

	void reset()
	{
		param.setDefault();
	}

	bool load(const char* serialized)
	{
		SString s = serialized;
		int p = 0; //position in string
		return ((param.load2(s, p) == 7) && (p == s.len()));
	}

	string save()
	{
		SString tmp;
		param.save2(tmp, NULL/*object containing default values for comparison*/, false/*add CR*/, false/*force field names*/);
		return string(tmp.c_str());
	}
};

#endif
