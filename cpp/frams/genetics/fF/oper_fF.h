// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

#ifndef _GENOOPER_FF_H_
#define _GENOOPER_FF_H_

#include "../oper_fx.h"


class GenoOper_fF : public GenoOperators
{
public:
	GenoOper_fF();
	int checkValidity(const char *, const char *genoname);
	int validate(char *&, const char *genoname);
	int mutate(char *&g, float& chg, int &method);
	int crossOver(char *&g1, char *&g2, float& chg1, float& chg2);
	uint32_t style(const char *g, int pos);
	const char* getSimplest() { return "6, "/*"1, 1, 1, "*/"1.05, 1.05, 1.05, 0, 0, 0"; }

	double xover_proportion;
};

#endif

