// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

#include "fF_chamber3d.h"
#include "conv_fF.h"
#include <iostream>

fF_chamber3d::fF_chamber3d(float centerX, float centerY, float centerZ,
	float radius, float holeX, float holeY, float holeZ,
	float vectorTfX, float vectorTfY, float vectorTfZ, float beta, float phi)
{
	points = NULL;
	this->centerX = centerX;
	this->centerY = centerY;
	this->centerZ = centerZ;
	this->radius = radius;
	this->holeX = holeX;
	this->holeY = holeY;
	this->holeZ = holeZ;
	this->vectorTfX = vectorTfX;
	this->vectorTfY = vectorTfY;
	this->vectorTfZ = vectorTfZ;
	this->beta = beta;
	this->phi = phi;
}

fF_chamber3d::~fF_chamber3d()
{
	delete[] points;
}
