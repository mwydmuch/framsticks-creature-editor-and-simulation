// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

#ifndef CHAMBER3D_H
#define	CHAMBER3D_H

struct fF_point;

//Chamber parameters; see http://www.framsticks.com/foraminifera
class fF_chamber3d
{
public:
	float centerX;
	float centerY;
	float centerZ;
	float radius;
	float holeX;
	float holeY;
	float holeZ;
	float vectorTfX;
	float vectorTfY;
	float vectorTfZ;
	float beta;
	float phi;
	fF_point *points;

	fF_chamber3d(float centerX, float centerY, float centerZ,
		float radius, float holeX, float holeY, float holeZ,
		float vectorTfX, float vectorTfY, float vectorTfZ, float beta, float phi);
	~fF_chamber3d();
};

#endif	/* CHAMBER3D_H */
