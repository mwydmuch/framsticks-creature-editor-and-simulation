// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

// Copyright (C) 1999,2000  Adam Rotaru-Varga (adam_rotaru@yahoo.com), GNU LGPL
// Copyright (C) since 2001 Maciej Komosinski

#ifndef _CONV_F4_H_
#define _CONV_F4_H_

#include <frams/model/model.h>
#include <frams/model/modelparts.h>
#include <frams/genetics/genoconv.h>
#include "f4_general.h"


// The f4->f0 converter
class GenoConv_f40 : public GenoConverter
{
public:
	GenoConv_f40();
	SString convert(SString &in, MultiMap * map);
};


// a test-only f4->f1 converter, approximates only
class GenoConv_F41_TestOnly : public GenoConverter
{
public:
	GenoConv_F41_TestOnly();
	SString convert(SString &in, MultiMap * map);
};


// A Model descendant, which support build from an f4 genotype.
class f4_Model : public Model
{
public:
	f4_Model();
	~f4_Model();
	int      buildFromF4(SString &geno);
	void     toF1Geno(SString &out);       // output to f1 format, approximation
private:
	f4_Cells * cells;
	int        buildModelRec(f4_Cell * ndad);
	/**
	 * Get a cell which is a stick, by traversing dadlinks.
	 */
	f4_Cell *  getStick(f4_Cell * C);
	int        error;
	int        errorpos;
};


#endif
