// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

// Copyright (C) 1999,2000  Adam Rotaru-Varga (adam_rotaru@yahoo.com), GNU LGPL
// Copyright (C) since 2001 Maciej Komosinski

#ifndef _GENO_F4_H_
#define _GENO_F4_H_

#include <stdio.h>
#include "f4_general.h"
#include "common/nonstd.h"
#include "../oper_fx.h"
#include <frams/param/param.h>


#define F4_ADD           0
#define F4_DEL           1
#define F4_MOD           2
#define F4_COUNT         3

#define F4_ADD_DIV       0
#define F4_ADD_CONN      1
#define F4_ADD_NEUPAR    2
#define F4_ADD_REP       3
#define F4_ADD_SIMP      4
#define F4_ADD_COUNT     5


class Geno_f4 : public GenoOperators
{
public:
	Geno_f4();
	int checkValidity(const char *, const char *genoname);
	int validate(char *&, const char *genoname);
	int mutate(char *& g, float & chg, int &method);
	int crossOver(char *&g1, char *&g2, float& chg1, float& chg2);
	const char* getSimplest() { return "X"; }
	uint32_t style(const char *g, int pos);

	// mutation probabilities
	double prob[F4_COUNT], probadd[F4_ADD_COUNT];

protected:
	/* int MutateMany(char *& g, float & chg); // not used anymore */
	int  ValidateRec(f4_node * geno, int retrycount) const;
	int  MutateOne(f4_node *& g, int &method) const;
	void linkNodeMakeRandom(f4_node * nn) const;
	void linkNodeChangeRandom(f4_node * nn) const;
	void nparNodeMakeRandom(f4_node * nn) const;
	void repeatNodeChangeRandom(f4_node * nn) const;
	int  MutateOneValid(f4_node * &g, int &method) const;
	int  CrossOverOne(f4_node *g1, f4_node *g2, float chg) const;
	// returns GENOPER_OK or GENOPER_OPFAIL
	// chg: fraction of parent1 genes in child (in g1) (parent2 has the rest)
};


#endif

