// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

// Copyright (C) 1999,2000  Adam Rotaru-Varga (adam_rotaru@yahoo.com), GNU LGPL

#include "conv_f4.h"
#include <common/log.h>
#include "../oper_fx.h" //for GENOPER_OK constant

#ifdef DMALLOC
#include <dmalloc.h>
#endif


GenoConv_f40::GenoConv_f40()
{
	name = "Developmental encoding";
	in_format = '4';
	out_format = '0';
	mapsupport = 1;
}


SString GenoConv_f40::convert(SString &in, MultiMap * map)
{
	int res;
	f4_Model * model = new f4_Model();
	res = model->buildFromF4(in);
	if (GENOPER_OK != res) return SString();  // oops
	if (NULL != map)
		// generate to-f0 conversion map
		model->getCurrentToF0Map(*map);
	SString out = model->getF0Geno().getGenes();
	delete model;
	return out;
}


GenoConv_F41_TestOnly::GenoConv_F41_TestOnly()
{
	name = "test-only approximate f4 -> f1 converter";
	in_format = '4';
	out_format = '1';
	mapsupport = 0;
	info = "This is for testing.  Do not use in production! (adam)";
}


SString GenoConv_F41_TestOnly::convert(SString &in, MultiMap * map)
{
	int res;
	f4_Model * model = new f4_Model();
	res = model->buildFromF4(in);
	if (GENOPER_OK != res) return SString();  // oops
	SString out;
	model->toF1Geno(out);
	delete model;
	return out;
}


f4_Model::f4_Model() : Model()
{
	cells = NULL;
}

f4_Model::~f4_Model()
{
	if (cells) delete cells;
}

int f4_Model::buildFromF4(SString &geno)
{
	int i;

	error = GENOPER_OK;
	errorpos = -1;

	// build cells, and simulate
	if (cells) delete cells;
	cells = new f4_Cells(geno, 0);
	if (GENOPER_OK != cells->geterror())
	{
		error = cells->geterror();
		errorpos = cells->geterrorpos();
		//delete cells;
		return error;
	}

	cells->simulate();
	if (GENOPER_OK != cells->geterror())
	{
		error = cells->geterror();
		errorpos = cells->geterrorpos();
		return error;
	}

	// reset recursive traverse flags
	for (i = 0; i < cells->nc; i++)
		cells->C[i]->recProcessedFlag = 0;

	open(); // begin model build

	// process every cell
	int res;
	for (i = 0; i < cells->nc; i++)
	{
		res = buildModelRec(cells->C[i]);
		if (res)
		{
			logMessage("f4_Model", "buildModelRec", 2, "Error in building Model");
			error = res;
			break;
		}
	}

	res = close();
	if (0 == res) // invalid
		error = -10;

	return error;
}


f4_Cell * f4_Model::getStick(f4_Cell * C)
{
	if (T_STICK4 == C->type) return C;
	if (NULL != C->dadlink)
		return getStick(C->dadlink);
	// we have no more dadlinks, find any stick
	for (int i = 0; i < cells->nc; i++)
		if (cells->C[i]->type == T_STICK4)
			return cells->C[i];
	// none!
	logMessage("f4_Model", "getStick", 2, "Not a single stick");
	return NULL;
}


/// updated by Macko to follow new SDK standards (no more neuroitems)
int f4_Model::buildModelRec(f4_Cell * C)
{
	int partidx;
	int j, res;
	MultiRange range;

	if (C->recProcessedFlag)
		// already processed
		return 0;

	// mark it processed
	C->recProcessedFlag = 1;

	// make sure parent is a stick
	if (NULL != C->dadlink)
		if (C->dadlink->type != T_STICK4)
		{
			C->dadlink = getStick(C->dadlink);
		}

	// make sure its parent is processed first
	if (NULL != C->dadlink)
	{
		res = buildModelRec(C->dadlink);
		if (res) return res;
	}

	char tmpLine[100];

	range = C->genoRange;
	if (C->type == T_STICK4)
	{
		int jj_p1_refno;  // save for later
		// first end is connected to dad, or new
		if (C->dadlink == NULL)
		{
			// new part object for firstend
			// coordinates are left to be computed by Model
			sprintf(tmpLine, "p:fr=%g,ing=%g,as=%g",
				/*1.0/C->P.mass,*/ C->P.friction, C->P.ingest, C->P.assim
				//C->firstend.x, C->firstend.y, C->firstend.z
				);
			partidx = singleStepBuild(tmpLine, &range);
			if (partidx < 0) return -1;
			jj_p1_refno = partidx;
		}
		else {
			// adjust mass/vol of first endpoint
			jj_p1_refno = C->dadlink->p2_refno;
			Part * p1 = getPart(jj_p1_refno);
			p1->mass += 1.0;
			//      p1->volume += 1.0/C->P.mass;
		}
		// new part object for lastend
		sprintf(tmpLine, "p:fr=%g,ing=%g,as=%g",
			//C->lastend.x, C->lastend.y, C->lastend.z
			/*"vol=" 1.0/C->P.mass,*/ C->P.friction, C->P.ingest, C->P.assim
			);
		partidx = singleStepBuild(tmpLine, &range);
		if (partidx < 0) return -2;
		C->p2_refno = partidx;

		// new joint object
		// check that the part references are valid
		int jj_p2_refno = C->p2_refno;
		if ((jj_p1_refno < 0) || (jj_p1_refno >= getPartCount())) return -11;
		if ((jj_p2_refno < 0) || (jj_p2_refno >= getPartCount())) return -12;
		sprintf(tmpLine, "j:p1=%ld,p2=%ld,dx=%g,dy=0,dz=0,rx=%g,ry=0,rz=%g"\
			",stam=%g",
			jj_p1_refno, jj_p2_refno,
			// relative position -- always (len, 0, 0), along the stick
			// this is optional!
			C->P.len,
			// relative rotation
			C->xrot, C->zrot,
			//C->P.ruch,   // rotstif
			C->P.odpor
			);
		partidx = singleStepBuild(tmpLine, &range);
		if (partidx < 0) return -13;
		C->joint_refno = partidx;
	}

	if (C->type == T_NEURON4) ///<this case was updated by MacKo
	{
		// new neuron object
		// it is linked to a part (not a joint!)
		int p_refno = C->dadlink->p2_refno;
		if ((p_refno < 0) || (p_refno >= getPartCount())) return -21;
		// joint_refno is currently not used
		sprintf(tmpLine, "n:p=%ld,d=\"N:in=%g,fo=%g,si=%g\"",
			p_refno,
			C->inertia, C->force, C->sigmo);
		partidx = singleStepBuild(tmpLine, &range);
		if (partidx < 0) return -22;
		C->neuro_refno = partidx;
		int n_refno = C->neuro_refno;

		if (C->ctrl)
		{
			if (1 == C->ctrl)
				sprintf(tmpLine, "n:j=%d,d=\"@:p=%g\"", C->dadlink->joint_refno, C->P.ruch);
			else
				sprintf(tmpLine, "n:j=%d,d=\"|:p=%g,r=%g\"", C->dadlink->joint_refno, C->P.ruch, C->mz);
			partidx = singleStepBuild(tmpLine, &range);
			if (partidx < 0) return -32;
			sprintf(tmpLine, "c:%d,%d", partidx, n_refno);
			if (singleStepBuild(tmpLine, &range) < 0) return -33;
		}

		for (j = 0; j < C->nolink; j++)
		{
			if (NULL != C->links[j]->from)
				buildModelRec(C->links[j]->from);

			tmpLine[0] = 0;
			if (1 == C->links[j]->t) sprintf(tmpLine, "n:p=%d,d=\"*\"", p_refno);
			if (2 == C->links[j]->t) sprintf(tmpLine, "n:j=%d,d=\"G\"", C->dadlink->joint_refno);
			if (3 == C->links[j]->t) sprintf(tmpLine, "n:p=%d,d=\"T\"", p_refno);
			if (4 == C->links[j]->t) sprintf(tmpLine, "n:p=%d,d=\"S\"", p_refno);
			int from = -1;
			if (tmpLine[0]) //input from receptor
			{
				from = singleStepBuild(tmpLine, &range);
				if (from < 0) return -34;
			} /*could be 'else'...*/
			if (NULL != C->links[j]->from) // input from another neuron
				from = C->links[j]->from->neuro_refno;
			if (from >= 0)
			{
				sprintf(tmpLine, "c:%d,%d,%g", n_refno, from, C->links[j]->w);
				if (singleStepBuild(tmpLine, &range) < 0) return -35;
			}
		}
	}
	return 0;
}


void f4_Model::toF1Geno(SString &out)
{
	cells->toF1Geno(out);
}
