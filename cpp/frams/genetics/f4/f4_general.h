// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

// Copyright (C) 1999,2000  Adam Rotaru-Varga (adam_rotaru@yahoo.com), GNU LGPL

#ifndef _F4_GENERAL_H_
#define _F4_GENERAL_H_

//#include "f4_orientmat.h"
#include <frams/util/3d.h>
#include <frams/util/sstring.h>
#include <frams/util/multirange.h>

#ifdef DMALLOC
#include <dmalloc.h>
#endif


class f4_Props
{
public:
	// fill with default values
	f4_Props();
	// must sum to 1
	void normalizeBiol4();
	void executeModifier(char modif);
	void adjust();

	double len;      // length (dlug)
	double curv;     // curvedness (skr)
	double mass;
	double friction;
	double ruch;
	double assim;
	double odpor;
	double ingest;  // ingestion (wchl)
	double twist;
	double energ;
};

extern f4_Props stdProps;


// rolling (one-time)
void rolling_dec(double * v);
void rolling_inc(double * v);


class f4_node;   // later
class f4_Cell;   // later
class f4_Cells;  // later


// cell types
#define T_UNDIFF4 40
#define T_STICK4  41
#define T_NEURON4 42

int scanrec(const char * s, unsigned int slen, char stopchar);


class f4_CellLink;
#define MAXINPUTS 100

// an abstract cell type, extension of part/stick -- for developmental encoding
class f4_Cell
{
public:
	class repeat_ptr
	{
	public:
		repeat_ptr() : node(NULL), count(-1) { };
		repeat_ptr(f4_node * a, int b) : node(a), count(b) { };
		inline void null() { node = NULL; count = -1; };
		inline bool isNull() const { return ((node == NULL) || (count <= 0)); };
		inline void dec() { count--; };
		f4_node *  node;  // ptr to repetition code
		char       count; // repetition counter
	};

	class repeat_stack  // a stack of repet_ptr's
	{
	public:
		repeat_stack() { top = 0; };
		inline void null() { top = 0; };
		inline void push(repeat_ptr A) { if (top >= stackSize) return; ptr[top] = A; top++; };
		inline void pop() { if (top > 0) top--; };
		inline repeat_ptr * first() { return &(ptr[top - (top > 0)]); };
		static const int stackSize = 4;  // max 4 nested levels
		repeat_ptr ptr[stackSize];
		short int top;  // top of the stack
	};

	f4_Cell(int nname,
		f4_Cell * ndad, int nangle, f4_Props newP);
	f4_Cell(f4_Cells * nO, int nname, f4_node * ngeno, f4_node * ngcur,
		f4_Cell * ndad, int nangle, f4_Props newP);
	~f4_Cell();

	int onestep();	// execute one simulation step (till a division)

	int   addlink(f4_Cell * nfrom, double nw, int nt);
	void  adjustRec();

	int        name;     // name (number)
	int        type;     // type
	f4_Cell *  dadlink;
	f4_Cells * org;	// uplink to organism

	f4_node *  genot;	  // genotype
	f4_node *  gcur;        // current genotype execution pointer
	int        active;      // whether development is still active
	repeat_stack repeat;
	int        recProcessedFlag;  // used during recursive traverse
	// remember the genotype codes affecting this cell so far
	MultiRange genoRange;

	f4_Props     P;             // properties
	int          anglepos;      // number of position within dad's children (,)
	int          childcount;    // number of children
	int          commacount;    // number of postitions at lastend (>=childcount)
	double       rolling;       // rolling angle ('R') (around x)
	double       xrot;
	double       zrot;          // horizontal rotation angle due to
	// branching (around z)
	//Pt3D         firstend;      // coord.s of first end (connects to parent)
	//Pt3D         lastend;       // last end
	//f4_OrientMat OM;
	double       mz;            // freedom in z
	int          p2_refno;   // number of last end part object, used in f0
	int          joint_refno;   // number of the joint object, used in f0
	int          neuro_refno;   // number of the neuro object, used in f0

	int          ctrl;  // neuron type
	double       state;
	double       inertia;
	double       force;
	double       sigmo;
	f4_CellLink* links[MAXINPUTS];
	int          nolink;
};


// an input link to a neuron
class f4_CellLink
{
public:
	f4_CellLink(f4_Cell * nfrom, double nw, int nt);
	f4_Cell *    from;
	// type: 0: input, 1 '*', 2 'G', 3 'T', 4 'S'
	int          t;
	double       w;
};


// a collection of cells, like Organism, for developmental encoding
#define MAX4CELLS 100
class f4_Cells
{
public:
	f4_Cells(f4_node * genome, int nrepair);
	f4_Cells(SString &genome, int nrepair);
	~f4_Cells();
	void addCell(f4_Cell * newcell);
	void toF1Geno(SString &out);       // output to f1 format, approximation
	int  onestep();       // simulate all parts for one step
	int  simulate();      // simulate development, return error (0 for ok)
	// for error reporting / genotype fixing
	int  geterror() { return error; };
	int  geterrorpos() { return errorpos; };
	void setError(int nerrpos);
	void setRepairRemove(int nerrpos, f4_node * rem);
	int  setRepairInsert(int nerrpos, f4_node * parent, f4_node * insert);
	void repairGeno(f4_node * geno, int whichchild);

	// the cells
	f4_Cell * C[MAX4CELLS];
	int       nc;

private:
	// for error reporting / genotype fixing
	int repair;
	int error;
	int errorpos;
	f4_node * repair_remove;
	f4_node * repair_parent;
	f4_node * repair_insert;
	void toF1GenoRec(int curc, SString &out);
	f4_Cell * tmpcel;		// needed by toF1Geno
	f4_node * f4rootnode;          // used by constructor
};


/**
 * Class to organize a f4 genotype in a tree structure.
 */
class f4_node
{
public:
	char      name;	// one-letter 'name'
	f4_node * parent;	// parent link, or NULL
	f4_node * child;	// child, or NULL
	f4_node * child2;	// second child, or NULL
	int       pos;        // original position in string
	int       i1;		// internal int  parameter1
	int       l1;		// internal long parameter1
	double    f1;		// internal double parameter1

	f4_node();
	f4_node(char nname, f4_node * nparent, int npos);
	~f4_node();
	int       addChild(f4_node * nchi);
	int       removeChild(f4_node * nchi);
	int       childCount();	// return no of children, 0, 1, or 2
	int       count();	// return no of nodes (recursive)
	f4_node * ordNode(int n);	// returns the nth subnode (0-)
	f4_node * randomNode();	// returns a random subnode
	f4_node * randomNodeWithSize(int min, int max);	// returns a random subnode with given size
	void      sprintAdj(char *& buf);	// print recursively
	f4_node * duplicate();         // create duplicate copy. recursive.
	void      destroy();	// release memory. recursive.
private:
	void     sprint(SString & out);	// print recursively
};

// convert f4 geno string to tree structure (internal)
f4_node * f4_processtree(const char * geno);
int f4_processrec(const char * genot, unsigned pos0, f4_node * parent);


#endif
