// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.


/**
 gen-config.h

 USE_GENCONV_xxx - configure converters
 USE_GENMAN_xxx - configure genman operators

 */

#define USE_GENCONV_f10
//#define USE_GENCONV_f21
//#define USE_GENCONV_f30
#define USE_GENCONV_f40
//#define USE_GENCONV_f41_TEST
//#define USE_GENCONV_f50
#define USE_GENCONV_f90
#define USE_GENCONV_fF0

//#define USE_GENMAN_f0
//#define USE_GENMAN_f1
//#define USE_GENMAN_f2
//#define USE_GENMAN_f3
#define USE_GENMAN_f4
//#define USE_GENMAN_f5
#define USE_GENMAN_f9
#define USE_GENMAN_fF
#define USE_GENMAN_fT

