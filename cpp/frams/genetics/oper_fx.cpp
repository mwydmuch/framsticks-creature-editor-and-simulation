// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

#include <ctype.h>  //isupper()
#include "oper_fx.h"
#include <common/log.h>
#include <common/nonstd_math.h>
#include <frams/util/rndutil.h>

static double distrib_force[] =   // for '!'
{
	3,             // distribution 0 -__/ +1
	0.001, 0.2,    // "slow" neurons
	0.001, 1,
	1, 1,          // "fast" neurons
};
static double distrib_inertia[] =  // for '='
{
	2,             // distribution 0 |..- +1
	0, 0,          // "fast" neurons
	0.7, 0.98,
};
static double distrib_sigmo[] =  // for '/'
{
	5,             // distribution -999 -..-^-..- +999
	-999, -999,    //"perceptron"
	999, 999,
	-5, -1,        // nonlinear
	1, 5,
	-1, 1,         // ~linear
};


int GenoOperators::roulette(const double *probtab, const int count)
{
	double sum = 0;
	int i;
	for (i = 0; i < count; i++) sum += probtab[i];
	double sel = rnd01*sum;
	for (sum = 0, i = 0; i < count; i++) { sum += probtab[i]; if (sel < sum) return i; }
	return -1;
}

bool GenoOperators::getMinMaxDef(ParamInterface *p, int i, double &mn, double &mx, double &def)
{
	mn = mx = def = 0;
	int defined = 0;
	if (p->type(i)[0] == 'f')
	{
		double _mn = 0, _mx = 1, _def = 0.5;
		defined = p->getMinMax(i, _mn, _mx, _def);
		if (defined == 1) _mx = _mn + 1.0;
		if (_mx < _mn && defined == 3) _mn = _mx = _def; //only default was defined, let's assume min=max=default
		if (defined < 3) _def = (_mn + _mx) / 2.0;
		mn = _mn; mx = _mx; def = _def;
	}
	if (p->type(i)[0] == 'd')
	{
		paInt _mn = 0, _mx = 1, _def = 0;
		defined = p->getMinMax(i, _mn, _mx, _def);
		if (defined == 1) _mx = _mn + 1;
		if (_mx < _mn && defined == 3) _mn = _mx = _def; //only default was defined, let's assume min=max=default
		if (defined < 3) _def = (_mn + _mx) / 2;
		mn = _mn; mx = _mx; def = _def;
	}
	return defined == 3;
}

int GenoOperators::selectRandomProperty(Neuro* n)
{
	int neuext = n->extraProperties().getPropCount(),
		neucls = n->getClass() == NULL ? 0 : n->getClass()->getProperties().getPropCount();
	if (neuext + neucls == 0) return -1; //no properties in this neuron
	int index = randomN(neuext + neucls);
	if (index >= neuext) index = index - neuext + 100;
	return index;
}

double GenoOperators::mutateNeuProperty(double current, Neuro *n, int i)
{
	if (i == -1) return mutateCreepNoLimit('f', current, -10, 10); //i==-1: mutating weight of neural connection
	Param p;
	if (i >= 100) { i -= 100; p = n->getClass()->getProperties(); }
	else p = n->extraProperties();
	double newval = current;
	/*bool ok=*/getMutatedProperty(p, i, current, newval);
	return newval;
}

bool GenoOperators::mutatePropertyNaive(ParamInterface &p, int i)
{
	double mn, mx, df;
	if (p.type(i)[0] != 'f' && p.type(i)[0] != 'd') return false; //don't know how to mutate
	getMinMaxDef(&p, i, mn, mx, df);

	ExtValue ev;
	p.get(i, ev);
	ev.setDouble(mutateCreep(p.type(i)[0], ev.getDouble(), mn, mx));
	p.set(i, ev);
	return true;
}

bool GenoOperators::mutateProperty(ParamInterface &p, int i)
{
	double newval;
	ExtValue ev;
	p.get(i, ev);
	bool ok = getMutatedProperty(p, i, ev.getDouble(), newval);
	if (ok) { ev.setDouble(newval); p.set(i, ev); }
	return ok;
}

bool GenoOperators::getMutatedProperty(ParamInterface &p, int i, double oldval, double &newval)
{
	newval = 0;
	if (p.type(i)[0] != 'f' && p.type(i)[0] != 'd') return false; //don't know how to mutate
	const char *n = p.id(i), *na = p.name(i);
	if (strcmp(n, "si") == 0 && strcmp(na, "Sigmoid") == 0) newval = CustomRnd(distrib_sigmo); else
		if (strcmp(n, "in") == 0 && strcmp(na, "Inertia") == 0) newval = CustomRnd(distrib_inertia); else
			if (strcmp(n, "fo") == 0 && strcmp(na, "Force") == 0) newval = CustomRnd(distrib_force); else
			{
				double mn, mx, df;
				getMinMaxDef(&p, i, mn, mx, df);
				newval = mutateCreep(p.type(i)[0], oldval, mn, mx);
			}
	return true;
}

double GenoOperators::mutateCreepNoLimit(char type, double current, double mn, double mx)
{
	double result = RndGen.Gauss(current, (mx - mn) / 2 / 5); // /halfinterval, 5 times narrower
	if (type == 'd') { result = int(result + 0.5); if (result == current) result += randomN(2) * 2 - 1; }
	else result = floor(result * 1000 + 0.5) / 1000.0; //round
	return result;
}

double GenoOperators::mutateCreep(char type, double current, double mn, double mx)
{
	double result = mutateCreepNoLimit(type, current, mn, mx); //TODO consider that when boundary is touched (reflect/absorb below), the default precision (3 digits) may change. Is it good or bad?
	//reflect:
	if (result > mx) result = mx - (result - mx); else
		if (result<mn) result = mn + (mn - result);
	//absorb (just in case 'result' exceeded the allowed range so much):
	if (result>mx) result = mx; else
		if (result < mn) result = mn;
	return result;
}

void GenoOperators::setIntFromDoubleWithProbabilisticDithering(ParamInterface &p, int index, double value) //TODO
{
	p.setInt(index, (paInt)value); //TODO value=2.5 will result in 2 but we want it to be 2 or 3 with equal probability. value=2.1 would be mostly 2, rarely 3. Careful with negative values (test it!)
}

void GenoOperators::linearMix(ParamInterface &p1, int i1, ParamInterface &p2, int i2, double proportion)
{
	char type1 = p1.type(i1)[0];
	char type2 = p2.type(i2)[0];
	if (type1 == 'f' && type2 == 'f')
	{
		double v1 = p1.getDouble(i1);
		double v2 = p2.getDouble(i2);
		p1.setDouble(i1, v1*proportion + v2*(1 - proportion));
		p2.setDouble(i2, v2*proportion + v1*(1 - proportion));
	}
	else
		if (type1 == 'd' && type2 == 'd')
		{
			int v1 = p1.getInt(i1);
			int v2 = p2.getInt(i2);
			setIntFromDoubleWithProbabilisticDithering(p1, i1, v1*proportion + v2*(1 - proportion));
			setIntFromDoubleWithProbabilisticDithering(p2, i2, v2*proportion + v1*(1 - proportion));
		}
		else
			logPrintf("GenoOperators", "linearMix", LOG_WARN, "Cannot mix values of types '%c' and '%c'", type1, type2);
}


NeuroClass* GenoOperators::getRandomNeuroClass()
{
	SListTempl<NeuroClass*> active;
	for (int i = 0; i < Neuro::getClassCount(); i++)
		if (Neuro::getClass(i)->genactive) active += Neuro::getClass(i);
	if (active.size() == 0) return NULL; else return active(randomN(active.size()));
}

NeuroClass* GenoOperators::parseNeuroClass(char*& s)
{
	int len = (int)strlen(s);
	int Len = 0;
	NeuroClass *I = NULL;
	for (int i = 0; i<Neuro::getClassCount(); i++)
	{
		const char *n = Neuro::getClass(i)->name.c_str();
		int l = (int)strlen(n);
		if (len >= l && l>Len && (strncmp(s, n, l) == 0)) { I = Neuro::getClass(i); Len = l; }
	}
	s += Len;
	return I;
}

Neuro* GenoOperators::findNeuro(const Model *m, const NeuroClass *nc)
{
	if (!m) return NULL;
	for (int i = 0; i < m->getNeuroCount(); i++)
		if (m->getNeuro(i)->getClass() == nc) return m->getNeuro(i);
	return NULL; //neuron of class 'nc' was not found
}

int GenoOperators::neuroClassProp(char*& s, NeuroClass *nc, bool also_v1_N_props)
{
	int len = (int)strlen(s);
	int Len = 0, I = -1;
	if (nc)
	{
		Param p = nc->getProperties();
		for (int i = 0; i<p.getPropCount(); i++)
		{
			const char *n = p.id(i);
			int l = (int)strlen(n);
			if (len >= l && l>Len && (strncmp(s, n, l) == 0)) { I = 100 + i; Len = l; }
			if (also_v1_N_props) //recognize old properties symbols /=!
			{
				if (strcmp(n, "si") == 0) n = "/"; else
					if (strcmp(n, "in") == 0) n = "="; else
						if (strcmp(n, "fo") == 0) n = "!";
				l = (int)strlen(n);
				if (len >= l && l > Len && (strncmp(s, n, l) == 0)) { I = 100 + i; Len = l; }
			}
		}
	}
	Neuro n;
	Param p = n.extraProperties();
	for (int i = 0; i<p.getPropCount(); i++)
	{
		const char *n = p.id(i);
		int l = (int)strlen(n);
		if (len >= l && l>Len && (strncmp(s, n, l) == 0)) { I = i; Len = l; }
	}
	s += Len;
	return I;
}

bool GenoOperators::isWS(const char c)
{
	return c == ' ' || c == '\n' || c == '\t' || c == '\r';
}

void GenoOperators::skipWS(char *&s)
{
	if (s == NULL)
		logMessage("GenoOperators", "skipWS", LOG_WARN, "NULL reference!");
	else
	while (isWS(*s)) s++;
}

bool GenoOperators::areAlike(char *g1, char *g2)
{
	while (*g1 || *g2)
	{
		skipWS(g1);
		skipWS(g2);
		if (*g1 != *g2) return false; //when difference
		if (!*g1 && !*g2) break; //both end
		g1++;
		g2++;
	}
	return true; //equal
}

char* GenoOperators::strchrn0(const char *str, char ch)
{
	return ch == 0 ? NULL : strchr((char*)str, ch);
}

bool GenoOperators::isNeuroClassName(const char firstchar)
{
	return isupper(firstchar) || firstchar == '|' || firstchar == '@' || firstchar == '*';
}

