
// Bindings utilities

function WrapperObject() {
}
WrapperObject.prototype = Object.create(WrapperObject.prototype);
WrapperObject.prototype.constructor = WrapperObject;
WrapperObject.prototype.__class__ = WrapperObject;
WrapperObject.__cache__ = {};
Module['WrapperObject'] = WrapperObject;

function getCache(__class__) {
  return (__class__ || WrapperObject).__cache__;
}
Module['getCache'] = getCache;

function wrapPointer(ptr, __class__) {
  var cache = getCache(__class__);
  var ret = cache[ptr];
  if (ret) return ret;
  ret = Object.create((__class__ || WrapperObject).prototype);
  ret.ptr = ptr;
  return cache[ptr] = ret;
}
Module['wrapPointer'] = wrapPointer;

function castObject(obj, __class__) {
  return wrapPointer(obj.ptr, __class__);
}
Module['castObject'] = castObject;

Module['NULL'] = wrapPointer(0);

function destroy(obj) {
  if (!obj['__destroy__']) throw 'Error: Cannot destroy object. (Did you create it yourself?)';
  obj['__destroy__']();
  // Remove from cache, so the object can be GC'd and refs added onto it released
  delete getCache(obj.__class__)[obj.ptr];
}
Module['destroy'] = destroy;

function compare(obj1, obj2) {
  return obj1.ptr === obj2.ptr;
}
Module['compare'] = compare;

function getPointer(obj) {
  return obj.ptr;
}
Module['getPointer'] = getPointer;

function getClass(obj) {
  return obj.__class__;
}
Module['getClass'] = getClass;

// Converts big (string or array) values into a C-style storage, in temporary space

var ensureCache = {
  buffer: 0,  // the main buffer of temporary storage
  size: 0,   // the size of buffer
  pos: 0,    // the next free offset in buffer
  temps: [], // extra allocations
  needed: 0, // the total size we need next time

  prepare: function() {
    if (this.needed) {
      // clear the temps
      for (var i = 0; i < this.temps.length; i++) {
        Module['_free'](this.temps[i]);
      }
      this.temps.length = 0;
      // prepare to allocate a bigger buffer
      Module['_free'](this.buffer);
      this.buffer = 0;
      this.size += this.needed;
      // clean up
      this.needed = 0;
    }
    if (!this.buffer) { // happens first time, or when we need to grow
      this.size += 128; // heuristic, avoid many small grow events
      this.buffer = Module['_malloc'](this.size);
      assert(this.buffer);
    }
    this.pos = 0;
  },
  alloc: function(array, view) {
    assert(this.buffer);
    var bytes = view.BYTES_PER_ELEMENT;
    var len = array.length * bytes;
    len = (len + 7) & -8; // keep things aligned to 8 byte boundaries
    var ret;
    if (this.pos + len >= this.size) {
      // we failed to allocate in the buffer, this time around :(
      assert(len > 0); // null terminator, at least
      this.needed += len;
      ret = Module['_malloc'](len);
      this.temps.push(ret);
    } else {
      // we can allocate in the buffer
      ret = this.buffer + this.pos;
      this.pos += len;
    }
    var retShifted = ret;
    switch (bytes) {
      case 2: retShifted >>= 1; break;
      case 4: retShifted >>= 2; break;
      case 8: retShifted >>= 3; break;
    }
    for (var i = 0; i < array.length; i++) {
      view[retShifted + i] = array[i];
    }
    return ret;
  },
};

function ensureString(value) {
  if (typeof value === 'string') return ensureCache.alloc(intArrayFromString(value), HEAP8);
  return value;
}
function ensureInt8(value) {
  if (typeof value === 'object') return ensureCache.alloc(value, HEAP8);
  return value;
}
function ensureInt16(value) {
  if (typeof value === 'object') return ensureCache.alloc(value, HEAP16);
  return value;
}
function ensureInt32(value) {
  if (typeof value === 'object') return ensureCache.alloc(value, HEAP32);
  return value;
}
function ensureFloat32(value) {
  if (typeof value === 'object') return ensureCache.alloc(value, HEAPF32);
  return value;
}
function ensureFloat64(value) {
  if (typeof value === 'object') return ensureCache.alloc(value, HEAPF64);
  return value;
}


// SList
function SList() {
  this.ptr = _emscripten_bind_SList_SList_0();
  getCache(SList)[this.ptr] = this;
};;
SList.prototype = Object.create(WrapperObject.prototype);
SList.prototype.constructor = SList;
SList.prototype.__class__ = SList;
SList.__cache__ = {};
Module['SList'] = SList;

SList.prototype['get'] = SList.prototype.get = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return wrapPointer(_emscripten_bind_SList_get_1(self, arg0), VoidPtr);
};;

SList.prototype['size'] = SList.prototype.size = function() {
  var self = this.ptr;
  return _emscripten_bind_SList_size_0(self);
};;

  SList.prototype['__destroy__'] = SList.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_SList___destroy___0(self);
};
// GenoOperators
function GenoOperators() {
  this.ptr = _emscripten_bind_GenoOperators_GenoOperators_0();
  getCache(GenoOperators)[this.ptr] = this;
};;
GenoOperators.prototype = Object.create(WrapperObject.prototype);
GenoOperators.prototype.constructor = GenoOperators;
GenoOperators.prototype.__class__ = GenoOperators;
GenoOperators.__cache__ = {};
Module['GenoOperators'] = GenoOperators;

GenoOperators.prototype['getSimplest'] = GenoOperators.prototype.getSimplest = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_GenoOperators_getSimplest_0(self), SString);
};;

  GenoOperators.prototype['__destroy__'] = GenoOperators.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_GenoOperators___destroy___0(self);
};
// Param
function Param(arg0, arg1) {
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  this.ptr = _emscripten_bind_Param_Param_2(arg0, arg1);
  getCache(Param)[this.ptr] = this;
};;
Param.prototype = Object.create(WrapperObject.prototype);
Param.prototype.constructor = Param;
Param.prototype.__class__ = Param;
Param.__cache__ = {};
Module['Param'] = Param;

Param.prototype['save'] = Param.prototype.save = function(arg0, arg1, arg2) {
  var self = this.ptr;
  ensureCache.prepare();
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  else arg1 = ensureString(arg1);
  if (arg2 && typeof arg2 === 'object') arg2 = arg2.ptr;
  if (arg2 === undefined) { return _emscripten_bind_Param_save_2(self, arg0, arg1) }
  return _emscripten_bind_Param_save_3(self, arg0, arg1, arg2);
};;

  Param.prototype['__destroy__'] = Param.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_Param___destroy___0(self);
};
// GenoOper_fF
function GenoOper_fF() {
  this.ptr = _emscripten_bind_GenoOper_fF_GenoOper_fF_0();
  getCache(GenoOper_fF)[this.ptr] = this;
};;
GenoOper_fF.prototype = Object.create(WrapperObject.prototype);
GenoOper_fF.prototype.constructor = GenoOper_fF;
GenoOper_fF.prototype.__class__ = GenoOper_fF;
GenoOper_fF.__cache__ = {};
Module['GenoOper_fF'] = GenoOper_fF;

  GenoOper_fF.prototype['__destroy__'] = GenoOper_fF.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_GenoOper_fF___destroy___0(self);
};
// SString
function SString() {
  this.ptr = _emscripten_bind_SString_SString_0();
  getCache(SString)[this.ptr] = this;
};;
SString.prototype = Object.create(WrapperObject.prototype);
SString.prototype.constructor = SString;
SString.prototype.__class__ = SString;
SString.__cache__ = {};
Module['SString'] = SString;

SString.prototype['set'] = SString.prototype.set = function(arg0) {
  var self = this.ptr;
  ensureCache.prepare();
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  else arg0 = ensureString(arg0);
  _emscripten_bind_SString_set_1(self, arg0);
};;

SString.prototype['c_str'] = SString.prototype.c_str = function() {
  var self = this.ptr;
  return Pointer_stringify(_emscripten_bind_SString_c_str_0(self));
};;

  SString.prototype['__destroy__'] = SString.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_SString___destroy___0(self);
};
// GenoValidator
function GenoValidator() { throw "cannot construct a GenoValidator, no constructor in IDL" }
GenoValidator.prototype = Object.create(WrapperObject.prototype);
GenoValidator.prototype.constructor = GenoValidator;
GenoValidator.prototype.__class__ = GenoValidator;
GenoValidator.__cache__ = {};
Module['GenoValidator'] = GenoValidator;

  GenoValidator.prototype['__destroy__'] = GenoValidator.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_GenoValidator___destroy___0(self);
};
// ParamEntry
function ParamEntry() { throw "cannot construct a ParamEntry, no constructor in IDL" }
ParamEntry.prototype = Object.create(WrapperObject.prototype);
ParamEntry.prototype.constructor = ParamEntry;
ParamEntry.prototype.__class__ = ParamEntry;
ParamEntry.__cache__ = {};
Module['ParamEntry'] = ParamEntry;

  ParamEntry.prototype['__destroy__'] = ParamEntry.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_ParamEntry___destroy___0(self);
};
// VoidPtr
function VoidPtr() { throw "cannot construct a VoidPtr, no constructor in IDL" }
VoidPtr.prototype = Object.create(WrapperObject.prototype);
VoidPtr.prototype.constructor = VoidPtr;
VoidPtr.prototype.__class__ = VoidPtr;
VoidPtr.__cache__ = {};
Module['VoidPtr'] = VoidPtr;

  VoidPtr.prototype['__destroy__'] = VoidPtr.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_VoidPtr___destroy___0(self);
};
// ModelGenoValidator
function ModelGenoValidator() {
  this.ptr = _emscripten_bind_ModelGenoValidator_ModelGenoValidator_0();
  getCache(ModelGenoValidator)[this.ptr] = this;
};;
ModelGenoValidator.prototype = Object.create(WrapperObject.prototype);
ModelGenoValidator.prototype.constructor = ModelGenoValidator;
ModelGenoValidator.prototype.__class__ = ModelGenoValidator;
ModelGenoValidator.__cache__ = {};
Module['ModelGenoValidator'] = ModelGenoValidator;

  ModelGenoValidator.prototype['__destroy__'] = ModelGenoValidator.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_ModelGenoValidator___destroy___0(self);
};
// NeuroClass
function NeuroClass() {
  this.ptr = _emscripten_bind_NeuroClass_NeuroClass_0();
  getCache(NeuroClass)[this.ptr] = this;
};;
NeuroClass.prototype = Object.create(WrapperObject.prototype);
NeuroClass.prototype.constructor = NeuroClass;
NeuroClass.prototype.__class__ = NeuroClass;
NeuroClass.__cache__ = {};
Module['NeuroClass'] = NeuroClass;

NeuroClass.prototype['getVisualHints'] = NeuroClass.prototype.getVisualHints = function() {
  var self = this.ptr;
  return _emscripten_bind_NeuroClass_getVisualHints_0(self);
};;

  NeuroClass.prototype['__destroy__'] = NeuroClass.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_NeuroClass___destroy___0(self);
};
// NNLayoutFunctionHelper
function NNLayoutFunctionHelper() {
  this.ptr = _emscripten_bind_NNLayoutFunctionHelper_NNLayoutFunctionHelper_0();
  getCache(NNLayoutFunctionHelper)[this.ptr] = this;
};;
NNLayoutFunctionHelper.prototype = Object.create(WrapperObject.prototype);
NNLayoutFunctionHelper.prototype.constructor = NNLayoutFunctionHelper;
NNLayoutFunctionHelper.prototype.__class__ = NNLayoutFunctionHelper;
NNLayoutFunctionHelper.__cache__ = {};
Module['NNLayoutFunctionHelper'] = NNLayoutFunctionHelper;

NNLayoutFunctionHelper.prototype['doLayout'] = NNLayoutFunctionHelper.prototype.doLayout = function(arg0, arg1) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  _emscripten_bind_NNLayoutFunctionHelper_doLayout_2(self, arg0, arg1);
};;

  NNLayoutFunctionHelper.prototype['__destroy__'] = NNLayoutFunctionHelper.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_NNLayoutFunctionHelper___destroy___0(self);
};
// Joint
function Joint() {
  this.ptr = _emscripten_bind_Joint_Joint_0();
  getCache(Joint)[this.ptr] = this;
};;
Joint.prototype = Object.create(WrapperObject.prototype);
Joint.prototype.constructor = Joint;
Joint.prototype.__class__ = Joint;
Joint.__cache__ = {};
Module['Joint'] = Joint;

Joint.prototype['attachToParts'] = Joint.prototype.attachToParts = function(arg0, arg1) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  _emscripten_bind_Joint_attachToParts_2(self, arg0, arg1);
};;

Joint.prototype['resetDelta'] = Joint.prototype.resetDelta = function() {
  var self = this.ptr;
  _emscripten_bind_Joint_resetDelta_0(self);
};;

Joint.prototype['useDelta'] = Joint.prototype.useDelta = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Joint_useDelta_1(self, arg0);
};;

Joint.prototype['isDelta'] = Joint.prototype.isDelta = function() {
  var self = this.ptr;
  return !!(_emscripten_bind_Joint_isDelta_0(self));
};;

  Joint.prototype['get_part1'] = Joint.prototype.get_part1 = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Joint_get_part1_0(self), Part);
};
    Joint.prototype['set_part1'] = Joint.prototype.set_part1 = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Joint_set_part1_1(self, arg0);
};
  Joint.prototype['get_part2'] = Joint.prototype.get_part2 = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Joint_get_part2_0(self), Part);
};
    Joint.prototype['set_part2'] = Joint.prototype.set_part2 = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Joint_set_part2_1(self, arg0);
};
  Joint.prototype['get_d'] = Joint.prototype.get_d = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Joint_get_d_0(self), Pt3D);
};
    Joint.prototype['set_d'] = Joint.prototype.set_d = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Joint_set_d_1(self, arg0);
};
  Joint.prototype['get_rot'] = Joint.prototype.get_rot = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Joint_get_rot_0(self), Pt3D);
};
    Joint.prototype['set_rot'] = Joint.prototype.set_rot = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Joint_set_rot_1(self, arg0);
};
  Joint.prototype['get_refno'] = Joint.prototype.get_refno = function() {
  var self = this.ptr;
  return _emscripten_bind_Joint_get_refno_0(self);
};
    Joint.prototype['set_refno'] = Joint.prototype.set_refno = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Joint_set_refno_1(self, arg0);
};
  Joint.prototype['get_stamina'] = Joint.prototype.get_stamina = function() {
  var self = this.ptr;
  return _emscripten_bind_Joint_get_stamina_0(self);
};
    Joint.prototype['set_stamina'] = Joint.prototype.set_stamina = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Joint_set_stamina_1(self, arg0);
};
  Joint.prototype['get_stif'] = Joint.prototype.get_stif = function() {
  var self = this.ptr;
  return _emscripten_bind_Joint_get_stif_0(self);
};
    Joint.prototype['set_stif'] = Joint.prototype.set_stif = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Joint_set_stif_1(self, arg0);
};
  Joint.prototype['get_rotstif'] = Joint.prototype.get_rotstif = function() {
  var self = this.ptr;
  return _emscripten_bind_Joint_get_rotstif_0(self);
};
    Joint.prototype['set_rotstif'] = Joint.prototype.set_rotstif = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Joint_set_rotstif_1(self, arg0);
};
  Joint.prototype['get_o'] = Joint.prototype.get_o = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Joint_get_o_0(self), Orient);
};
    Joint.prototype['set_o'] = Joint.prototype.set_o = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Joint_set_o_1(self, arg0);
};
  Joint.prototype['get_usedelta'] = Joint.prototype.get_usedelta = function() {
  var self = this.ptr;
  return !!(_emscripten_bind_Joint_get_usedelta_0(self));
};
    Joint.prototype['set_usedelta'] = Joint.prototype.set_usedelta = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Joint_set_usedelta_1(self, arg0);
};
  Joint.prototype['get_vcolor'] = Joint.prototype.get_vcolor = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Joint_get_vcolor_0(self), Pt3D);
};
    Joint.prototype['set_vcolor'] = Joint.prototype.set_vcolor = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Joint_set_vcolor_1(self, arg0);
};
  Joint.prototype['get_shape'] = Joint.prototype.get_shape = function() {
  var self = this.ptr;
  return _emscripten_bind_Joint_get_shape_0(self);
};
    Joint.prototype['set_shape'] = Joint.prototype.set_shape = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Joint_set_shape_1(self, arg0);
};
  Joint.prototype['__destroy__'] = Joint.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_Joint___destroy___0(self);
};
// Part
function Part(arg0) {
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg0 === undefined) { this.ptr = _emscripten_bind_Part_Part_0(); getCache(Part)[this.ptr] = this;return }
  this.ptr = _emscripten_bind_Part_Part_1(arg0);
  getCache(Part)[this.ptr] = this;
};;
Part.prototype = Object.create(WrapperObject.prototype);
Part.prototype.constructor = Part;
Part.prototype.__class__ = Part;
Part.__cache__ = {};
Module['Part'] = Part;

Part.prototype['setPositionAndRotationFromAxis'] = Part.prototype.setPositionAndRotationFromAxis = function(arg0, arg1) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  _emscripten_bind_Part_setPositionAndRotationFromAxis_2(self, arg0, arg1);
};;

Part.prototype['setOrient'] = Part.prototype.setOrient = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Part_setOrient_1(self, arg0);
};;

Part.prototype['setRot'] = Part.prototype.setRot = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Part_setRot_1(self, arg0);
};;

  Part.prototype['get_refno'] = Part.prototype.get_refno = function() {
  var self = this.ptr;
  return _emscripten_bind_Part_get_refno_0(self);
};
    Part.prototype['set_refno'] = Part.prototype.set_refno = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Part_set_refno_1(self, arg0);
};
  Part.prototype['get_p'] = Part.prototype.get_p = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Part_get_p_0(self), Pt3D);
};
    Part.prototype['set_p'] = Part.prototype.set_p = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Part_set_p_1(self, arg0);
};
  Part.prototype['get_o'] = Part.prototype.get_o = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Part_get_o_0(self), Orient);
};
    Part.prototype['set_o'] = Part.prototype.set_o = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Part_set_o_1(self, arg0);
};
  Part.prototype['get_rot'] = Part.prototype.get_rot = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Part_get_rot_0(self), Pt3D);
};
    Part.prototype['set_rot'] = Part.prototype.set_rot = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Part_set_rot_1(self, arg0);
};
  Part.prototype['get_mass'] = Part.prototype.get_mass = function() {
  var self = this.ptr;
  return _emscripten_bind_Part_get_mass_0(self);
};
    Part.prototype['set_mass'] = Part.prototype.set_mass = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Part_set_mass_1(self, arg0);
};
  Part.prototype['get_size'] = Part.prototype.get_size = function() {
  var self = this.ptr;
  return _emscripten_bind_Part_get_size_0(self);
};
    Part.prototype['set_size'] = Part.prototype.set_size = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Part_set_size_1(self, arg0);
};
  Part.prototype['get_density'] = Part.prototype.get_density = function() {
  var self = this.ptr;
  return _emscripten_bind_Part_get_density_0(self);
};
    Part.prototype['set_density'] = Part.prototype.set_density = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Part_set_density_1(self, arg0);
};
  Part.prototype['get_friction'] = Part.prototype.get_friction = function() {
  var self = this.ptr;
  return _emscripten_bind_Part_get_friction_0(self);
};
    Part.prototype['set_friction'] = Part.prototype.set_friction = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Part_set_friction_1(self, arg0);
};
  Part.prototype['get_ingest'] = Part.prototype.get_ingest = function() {
  var self = this.ptr;
  return _emscripten_bind_Part_get_ingest_0(self);
};
    Part.prototype['set_ingest'] = Part.prototype.set_ingest = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Part_set_ingest_1(self, arg0);
};
  Part.prototype['get_assim'] = Part.prototype.get_assim = function() {
  var self = this.ptr;
  return _emscripten_bind_Part_get_assim_0(self);
};
    Part.prototype['set_assim'] = Part.prototype.set_assim = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Part_set_assim_1(self, arg0);
};
  Part.prototype['get_hollow'] = Part.prototype.get_hollow = function() {
  var self = this.ptr;
  return _emscripten_bind_Part_get_hollow_0(self);
};
    Part.prototype['set_hollow'] = Part.prototype.set_hollow = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Part_set_hollow_1(self, arg0);
};
  Part.prototype['get_scale'] = Part.prototype.get_scale = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Part_get_scale_0(self), Pt3D);
};
    Part.prototype['set_scale'] = Part.prototype.set_scale = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Part_set_scale_1(self, arg0);
};
  Part.prototype['get_vcolor'] = Part.prototype.get_vcolor = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Part_get_vcolor_0(self), Pt3D);
};
    Part.prototype['set_vcolor'] = Part.prototype.set_vcolor = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Part_set_vcolor_1(self, arg0);
};
  Part.prototype['get_vsize'] = Part.prototype.get_vsize = function() {
  var self = this.ptr;
  return _emscripten_bind_Part_get_vsize_0(self);
};
    Part.prototype['set_vsize'] = Part.prototype.set_vsize = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Part_set_vsize_1(self, arg0);
};
  Part.prototype['get_shape'] = Part.prototype.get_shape = function() {
  var self = this.ptr;
  return _emscripten_bind_Part_get_shape_0(self);
};
    Part.prototype['set_shape'] = Part.prototype.set_shape = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Part_set_shape_1(self, arg0);
};
  Part.prototype['__destroy__'] = Part.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_Part___destroy___0(self);
};
// NNLayoutState_Model
function NNLayoutState_Model() { throw "cannot construct a NNLayoutState_Model, no constructor in IDL" }
NNLayoutState_Model.prototype = Object.create(WrapperObject.prototype);
NNLayoutState_Model.prototype.constructor = NNLayoutState_Model;
NNLayoutState_Model.prototype.__class__ = NNLayoutState_Model;
NNLayoutState_Model.__cache__ = {};
Module['NNLayoutState_Model'] = NNLayoutState_Model;

  NNLayoutState_Model.prototype['__destroy__'] = NNLayoutState_Model.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_NNLayoutState_Model___destroy___0(self);
};
// GenoOperatorsHelper
function GenoOperatorsHelper(arg0) {
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  this.ptr = _emscripten_bind_GenoOperatorsHelper_GenoOperatorsHelper_1(arg0);
  getCache(GenoOperatorsHelper)[this.ptr] = this;
};;
GenoOperatorsHelper.prototype = Object.create(WrapperObject.prototype);
GenoOperatorsHelper.prototype.constructor = GenoOperatorsHelper;
GenoOperatorsHelper.prototype.__class__ = GenoOperatorsHelper;
GenoOperatorsHelper.__cache__ = {};
Module['GenoOperatorsHelper'] = GenoOperatorsHelper;

GenoOperatorsHelper.prototype['mutate'] = GenoOperatorsHelper.prototype.mutate = function(arg0) {
  var self = this.ptr;
  ensureCache.prepare();
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  else arg0 = ensureString(arg0);
  return _emscripten_bind_GenoOperatorsHelper_mutate_1(self, arg0);
};;

GenoOperatorsHelper.prototype['crossOver'] = GenoOperatorsHelper.prototype.crossOver = function(arg0, arg1) {
  var self = this.ptr;
  ensureCache.prepare();
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  else arg0 = ensureString(arg0);
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  else arg1 = ensureString(arg1);
  return _emscripten_bind_GenoOperatorsHelper_crossOver_2(self, arg0, arg1);
};;

GenoOperatorsHelper.prototype['getLastMutateGeno'] = GenoOperatorsHelper.prototype.getLastMutateGeno = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_GenoOperatorsHelper_getLastMutateGeno_0(self), SString);
};;

GenoOperatorsHelper.prototype['getLastCrossGeno1'] = GenoOperatorsHelper.prototype.getLastCrossGeno1 = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_GenoOperatorsHelper_getLastCrossGeno1_0(self), SString);
};;

GenoOperatorsHelper.prototype['getLastCrossGeno2'] = GenoOperatorsHelper.prototype.getLastCrossGeno2 = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_GenoOperatorsHelper_getLastCrossGeno2_0(self), SString);
};;

  GenoOperatorsHelper.prototype['__destroy__'] = GenoOperatorsHelper.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_GenoOperatorsHelper___destroy___0(self);
};
// Pt3D
function Pt3D() {
  this.ptr = _emscripten_bind_Pt3D_Pt3D_0();
  getCache(Pt3D)[this.ptr] = this;
};;
Pt3D.prototype = Object.create(WrapperObject.prototype);
Pt3D.prototype.constructor = Pt3D;
Pt3D.prototype.__class__ = Pt3D;
Pt3D.__cache__ = {};
Module['Pt3D'] = Pt3D;

  Pt3D.prototype['get_x'] = Pt3D.prototype.get_x = function() {
  var self = this.ptr;
  return _emscripten_bind_Pt3D_get_x_0(self);
};
    Pt3D.prototype['set_x'] = Pt3D.prototype.set_x = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Pt3D_set_x_1(self, arg0);
};
  Pt3D.prototype['get_y'] = Pt3D.prototype.get_y = function() {
  var self = this.ptr;
  return _emscripten_bind_Pt3D_get_y_0(self);
};
    Pt3D.prototype['set_y'] = Pt3D.prototype.set_y = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Pt3D_set_y_1(self, arg0);
};
  Pt3D.prototype['get_z'] = Pt3D.prototype.get_z = function() {
  var self = this.ptr;
  return _emscripten_bind_Pt3D_get_z_0(self);
};
    Pt3D.prototype['set_z'] = Pt3D.prototype.set_z = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Pt3D_set_z_1(self, arg0);
};
  Pt3D.prototype['__destroy__'] = Pt3D.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_Pt3D___destroy___0(self);
};
// XYWH
function XYWH(arg0, arg1, arg2, arg3) {
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  if (arg2 && typeof arg2 === 'object') arg2 = arg2.ptr;
  if (arg3 && typeof arg3 === 'object') arg3 = arg3.ptr;
  this.ptr = _emscripten_bind_XYWH_XYWH_4(arg0, arg1, arg2, arg3);
  getCache(XYWH)[this.ptr] = this;
};;
XYWH.prototype = Object.create(WrapperObject.prototype);
XYWH.prototype.constructor = XYWH;
XYWH.prototype.__class__ = XYWH;
XYWH.__cache__ = {};
Module['XYWH'] = XYWH;

  XYWH.prototype['get_x'] = XYWH.prototype.get_x = function() {
  var self = this.ptr;
  return _emscripten_bind_XYWH_get_x_0(self);
};
    XYWH.prototype['set_x'] = XYWH.prototype.set_x = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_XYWH_set_x_1(self, arg0);
};
  XYWH.prototype['get_y'] = XYWH.prototype.get_y = function() {
  var self = this.ptr;
  return _emscripten_bind_XYWH_get_y_0(self);
};
    XYWH.prototype['set_y'] = XYWH.prototype.set_y = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_XYWH_set_y_1(self, arg0);
};
  XYWH.prototype['get_w'] = XYWH.prototype.get_w = function() {
  var self = this.ptr;
  return _emscripten_bind_XYWH_get_w_0(self);
};
    XYWH.prototype['set_w'] = XYWH.prototype.set_w = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_XYWH_set_w_1(self, arg0);
};
  XYWH.prototype['get_h'] = XYWH.prototype.get_h = function() {
  var self = this.ptr;
  return _emscripten_bind_XYWH_get_h_0(self);
};
    XYWH.prototype['set_h'] = XYWH.prototype.set_h = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_XYWH_set_h_1(self, arg0);
};
  XYWH.prototype['__destroy__'] = XYWH.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_XYWH___destroy___0(self);
};
// Neuro
function Neuro(arg0, arg1, arg2, arg3) {
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  if (arg2 && typeof arg2 === 'object') arg2 = arg2.ptr;
  if (arg3 && typeof arg3 === 'object') arg3 = arg3.ptr;
  if (arg0 === undefined) { this.ptr = _emscripten_bind_Neuro_Neuro_0(); getCache(Neuro)[this.ptr] = this;return }
  if (arg1 === undefined) { this.ptr = _emscripten_bind_Neuro_Neuro_1(arg0); getCache(Neuro)[this.ptr] = this;return }
  if (arg2 === undefined) { this.ptr = _emscripten_bind_Neuro_Neuro_2(arg0, arg1); getCache(Neuro)[this.ptr] = this;return }
  if (arg3 === undefined) { this.ptr = _emscripten_bind_Neuro_Neuro_3(arg0, arg1, arg2); getCache(Neuro)[this.ptr] = this;return }
  this.ptr = _emscripten_bind_Neuro_Neuro_4(arg0, arg1, arg2, arg3);
  getCache(Neuro)[this.ptr] = this;
};;
Neuro.prototype = Object.create(WrapperObject.prototype);
Neuro.prototype.constructor = Neuro;
Neuro.prototype.__class__ = Neuro;
Neuro.__cache__ = {};
Module['Neuro'] = Neuro;

Neuro.prototype['setInputInfo'] = Neuro.prototype.setInputInfo = function(arg0, arg1, arg2) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  if (arg2 && typeof arg2 === 'object') arg2 = arg2.ptr;
  _emscripten_bind_Neuro_setInputInfo_3(self, arg0, arg1, arg2);
};;

Neuro.prototype['getInputInfo'] = Neuro.prototype.getInputInfo = function(arg0, arg1) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  if (arg1 === undefined) { return wrapPointer(_emscripten_bind_Neuro_getInputInfo_1(self, arg0), SString) }
  return wrapPointer(_emscripten_bind_Neuro_getInputInfo_2(self, arg0, arg1), SString);
};;

Neuro.prototype['getClass'] = Neuro.prototype.getClass = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Neuro_getClass_0(self), NeuroClass);
};;

Neuro.prototype['setClass'] = Neuro.prototype.setClass = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Neuro_setClass_1(self, arg0);
};;

Neuro.prototype['getClassParams'] = Neuro.prototype.getClassParams = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Neuro_getClassParams_0(self), SString);
};;

Neuro.prototype['setClassParams'] = Neuro.prototype.setClassParams = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Neuro_setClassParams_1(self, arg0);
};;

Neuro.prototype['getClassName'] = Neuro.prototype.getClassName = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Neuro_getClassName_0(self), SString);
};;

Neuro.prototype['setClassName'] = Neuro.prototype.setClassName = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Neuro_setClassName_1(self, arg0);
};;

Neuro.prototype['getDetails'] = Neuro.prototype.getDetails = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Neuro_getDetails_0(self), SString);
};;

Neuro.prototype['setDetails'] = Neuro.prototype.setDetails = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Neuro_setDetails_1(self, arg0);
};;

Neuro.prototype['attachToPart'] = Neuro.prototype.attachToPart = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Neuro_attachToPart_1(self, arg0);
};;

Neuro.prototype['attachToJoint'] = Neuro.prototype.attachToJoint = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Neuro_attachToJoint_1(self, arg0);
};;

Neuro.prototype['getPart'] = Neuro.prototype.getPart = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Neuro_getPart_0(self), Part);
};;

Neuro.prototype['getJoint'] = Neuro.prototype.getJoint = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Neuro_getJoint_0(self), Joint);
};;

Neuro.prototype['isOldEffector'] = Neuro.prototype.isOldEffector = function() {
  var self = this.ptr;
  return !!(_emscripten_bind_Neuro_isOldEffector_0(self));
};;

Neuro.prototype['isOldReceptor'] = Neuro.prototype.isOldReceptor = function() {
  var self = this.ptr;
  return !!(_emscripten_bind_Neuro_isOldReceptor_0(self));
};;

Neuro.prototype['isOldNeuron'] = Neuro.prototype.isOldNeuron = function() {
  var self = this.ptr;
  return !!(_emscripten_bind_Neuro_isOldNeuron_0(self));
};;

Neuro.prototype['isNNConnection'] = Neuro.prototype.isNNConnection = function() {
  var self = this.ptr;
  return !!(_emscripten_bind_Neuro_isNNConnection_0(self));
};;

Neuro.prototype['getInputCount'] = Neuro.prototype.getInputCount = function() {
  var self = this.ptr;
  return _emscripten_bind_Neuro_getInputCount_0(self);
};;

Neuro.prototype['getOutputsCount'] = Neuro.prototype.getOutputsCount = function() {
  var self = this.ptr;
  return _emscripten_bind_Neuro_getOutputsCount_0(self);
};;

Neuro.prototype['getInput'] = Neuro.prototype.getInput = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return wrapPointer(_emscripten_bind_Neuro_getInput_1(self, arg0), Neuro);
};;

Neuro.prototype['getInputWeight'] = Neuro.prototype.getInputWeight = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return _emscripten_bind_Neuro_getInputWeight_1(self, arg0);
};;

Neuro.prototype['setInputWeight'] = Neuro.prototype.setInputWeight = function(arg0, arg1) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  _emscripten_bind_Neuro_setInputWeight_2(self, arg0, arg1);
};;

Neuro.prototype['setInput'] = Neuro.prototype.setInput = function(arg0, arg1, arg2) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  if (arg2 && typeof arg2 === 'object') arg2 = arg2.ptr;
  if (arg2 === undefined) { _emscripten_bind_Neuro_setInput_2(self, arg0, arg1);  return }
  _emscripten_bind_Neuro_setInput_3(self, arg0, arg1, arg2);
};;

Neuro.prototype['addInput'] = Neuro.prototype.addInput = function(arg0, arg1, arg2) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  if (arg2 && typeof arg2 === 'object') arg2 = arg2.ptr;
  return _emscripten_bind_Neuro_addInput_3(self, arg0, arg1, arg2);
};;

Neuro.prototype['findInput'] = Neuro.prototype.findInput = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return _emscripten_bind_Neuro_findInput_1(self, arg0);
};;

Neuro.prototype['removeInput'] = Neuro.prototype.removeInput = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Neuro_removeInput_1(self, arg0);
};;

Neuro.prototype['findInputs'] = Neuro.prototype.findInputs = function(arg0, arg1, arg2, arg3) {
  var self = this.ptr;
  ensureCache.prepare();
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  else arg1 = ensureString(arg1);
  if (arg2 && typeof arg2 === 'object') arg2 = arg2.ptr;
  if (arg3 && typeof arg3 === 'object') arg3 = arg3.ptr;
  return _emscripten_bind_Neuro_findInputs_4(self, arg0, arg1, arg2, arg3);
};;

Neuro.prototype['findOutputs'] = Neuro.prototype.findOutputs = function(arg0, arg1, arg2, arg3) {
  var self = this.ptr;
  ensureCache.prepare();
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  else arg1 = ensureString(arg1);
  if (arg2 && typeof arg2 === 'object') arg2 = arg2.ptr;
  if (arg3 && typeof arg3 === 'object') arg3 = arg3.ptr;
  return _emscripten_bind_Neuro_findOutputs_4(self, arg0, arg1, arg2, arg3);
};;

  Neuro.prototype['get_refno'] = Neuro.prototype.get_refno = function() {
  var self = this.ptr;
  return _emscripten_bind_Neuro_get_refno_0(self);
};
    Neuro.prototype['set_refno'] = Neuro.prototype.set_refno = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Neuro_set_refno_1(self, arg0);
};
  Neuro.prototype['__destroy__'] = Neuro.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_Neuro___destroy___0(self);
};
// string
function string() {
  this.ptr = _emscripten_bind_string_string_0();
  getCache(string)[this.ptr] = this;
};;
string.prototype = Object.create(WrapperObject.prototype);
string.prototype.constructor = string;
string.prototype.__class__ = string;
string.__cache__ = {};
Module['string'] = string;

string.prototype['c_str'] = string.prototype.c_str = function() {
  var self = this.ptr;
  return Pointer_stringify(_emscripten_bind_string_c_str_0(self));
};;

  string.prototype['__destroy__'] = string.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_string___destroy___0(self);
};
// Geno_f4
function Geno_f4() {
  this.ptr = _emscripten_bind_Geno_f4_Geno_f4_0();
  getCache(Geno_f4)[this.ptr] = this;
};;
Geno_f4.prototype = Object.create(WrapperObject.prototype);
Geno_f4.prototype.constructor = Geno_f4;
Geno_f4.prototype.__class__ = Geno_f4;
Geno_f4.__cache__ = {};
Module['Geno_f4'] = Geno_f4;

  Geno_f4.prototype['__destroy__'] = Geno_f4.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_Geno_f4___destroy___0(self);
};
// Validators
function Validators() {
  this.ptr = _emscripten_bind_Validators_Validators_0();
  getCache(Validators)[this.ptr] = this;
};;
Validators.prototype = Object.create(WrapperObject.prototype);
Validators.prototype.constructor = Validators;
Validators.prototype.__class__ = Validators;
Validators.__cache__ = {};
Module['Validators'] = Validators;

Validators.prototype['append'] = Validators.prototype.append = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Validators_append_1(self, arg0);
};;

  Validators.prototype['__destroy__'] = Validators.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_Validators___destroy___0(self);
};
// StdioFileSystem_autoselect
function StdioFileSystem_autoselect() {
  this.ptr = _emscripten_bind_StdioFileSystem_autoselect_StdioFileSystem_autoselect_0();
  getCache(StdioFileSystem_autoselect)[this.ptr] = this;
};;
StdioFileSystem_autoselect.prototype = Object.create(WrapperObject.prototype);
StdioFileSystem_autoselect.prototype.constructor = StdioFileSystem_autoselect;
StdioFileSystem_autoselect.prototype.__class__ = StdioFileSystem_autoselect;
StdioFileSystem_autoselect.__cache__ = {};
Module['StdioFileSystem_autoselect'] = StdioFileSystem_autoselect;

  StdioFileSystem_autoselect.prototype['__destroy__'] = StdioFileSystem_autoselect.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_StdioFileSystem_autoselect___destroy___0(self);
};
// GenoConvManager
function GenoConvManager() { throw "cannot construct a GenoConvManager, no constructor in IDL" }
GenoConvManager.prototype = Object.create(WrapperObject.prototype);
GenoConvManager.prototype.constructor = GenoConvManager;
GenoConvManager.prototype.__class__ = GenoConvManager;
GenoConvManager.__cache__ = {};
Module['GenoConvManager'] = GenoConvManager;

  GenoConvManager.prototype['__destroy__'] = GenoConvManager.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_GenoConvManager___destroy___0(self);
};
// NNLayoutState
function NNLayoutState() { throw "cannot construct a NNLayoutState, no constructor in IDL" }
NNLayoutState.prototype = Object.create(WrapperObject.prototype);
NNLayoutState.prototype.constructor = NNLayoutState;
NNLayoutState.prototype.__class__ = NNLayoutState;
NNLayoutState.__cache__ = {};
Module['NNLayoutState'] = NNLayoutState;

  NNLayoutState.prototype['__destroy__'] = NNLayoutState.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_NNLayoutState___destroy___0(self);
};
// Geno
function Geno(arg0, arg1, arg2, arg3) {
  ensureCache.prepare();
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  else arg0 = ensureString(arg0);
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  if (arg2 && typeof arg2 === 'object') arg2 = arg2.ptr;
  else arg2 = ensureString(arg2);
  if (arg3 && typeof arg3 === 'object') arg3 = arg3.ptr;
  else arg3 = ensureString(arg3);
  if (arg0 === undefined) { this.ptr = _emscripten_bind_Geno_Geno_0(); getCache(Geno)[this.ptr] = this;return }
  if (arg1 === undefined) { this.ptr = _emscripten_bind_Geno_Geno_1(arg0); getCache(Geno)[this.ptr] = this;return }
  if (arg2 === undefined) { this.ptr = _emscripten_bind_Geno_Geno_2(arg0, arg1); getCache(Geno)[this.ptr] = this;return }
  if (arg3 === undefined) { this.ptr = _emscripten_bind_Geno_Geno_3(arg0, arg1, arg2); getCache(Geno)[this.ptr] = this;return }
  this.ptr = _emscripten_bind_Geno_Geno_4(arg0, arg1, arg2, arg3);
  getCache(Geno)[this.ptr] = this;
};;
Geno.prototype = Object.create(WrapperObject.prototype);
Geno.prototype.constructor = Geno;
Geno.prototype.__class__ = Geno;
Geno.__cache__ = {};
Module['Geno'] = Geno;

Geno.prototype['isValid'] = Geno.prototype.isValid = function() {
  var self = this.ptr;
  return !!(_emscripten_bind_Geno_isValid_0(self));
};;

Geno.prototype['getFormat'] = Geno.prototype.getFormat = function() {
  var self = this.ptr;
  return _emscripten_bind_Geno_getFormat_0(self);
};;

Geno.prototype['getGenes'] = Geno.prototype.getGenes = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Geno_getGenes_0(self), SString);
};;

Geno.prototype['useValidators'] = Geno.prototype.useValidators = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return wrapPointer(_emscripten_bind_Geno_useValidators_1(self, arg0), Validators);
};;

Geno.prototype['useConverters'] = Geno.prototype.useConverters = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return wrapPointer(_emscripten_bind_Geno_useConverters_1(self, arg0), GenoConvManager);
};;

  Geno.prototype['__destroy__'] = Geno.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_Geno___destroy___0(self);
};
// XY
function XY(arg0, arg1) {
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  this.ptr = _emscripten_bind_XY_XY_2(arg0, arg1);
  getCache(XY)[this.ptr] = this;
};;
XY.prototype = Object.create(WrapperObject.prototype);
XY.prototype.constructor = XY;
XY.prototype.__class__ = XY;
XY.__cache__ = {};
Module['XY'] = XY;

  XY.prototype['get_x'] = XY.prototype.get_x = function() {
  var self = this.ptr;
  return _emscripten_bind_XY_get_x_0(self);
};
    XY.prototype['set_x'] = XY.prototype.set_x = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_XY_set_x_1(self, arg0);
};
  XY.prototype['get_y'] = XY.prototype.get_y = function() {
  var self = this.ptr;
  return _emscripten_bind_XY_get_y_0(self);
};
    XY.prototype['set_y'] = XY.prototype.set_y = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_XY_set_y_1(self, arg0);
};
  XY.prototype['__destroy__'] = XY.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_XY___destroy___0(self);
};
// Model
function Model(arg0, arg1) {
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  if (arg0 === undefined) { this.ptr = _emscripten_bind_Model_Model_0(); getCache(Model)[this.ptr] = this;return }
  if (arg1 === undefined) { this.ptr = _emscripten_bind_Model_Model_1(arg0); getCache(Model)[this.ptr] = this;return }
  this.ptr = _emscripten_bind_Model_Model_2(arg0, arg1);
  getCache(Model)[this.ptr] = this;
};;
Model.prototype = Object.create(WrapperObject.prototype);
Model.prototype.constructor = Model;
Model.prototype.__class__ = Model;
Model.__cache__ = {};
Module['Model'] = Model;

Model.prototype['getStatus'] = Model.prototype.getStatus = function() {
  var self = this.ptr;
  return _emscripten_bind_Model_getStatus_0(self);
};;

Model.prototype['isValid'] = Model.prototype.isValid = function() {
  var self = this.ptr;
  return !!(_emscripten_bind_Model_isValid_0(self));
};;

Model.prototype['getErrorPosition'] = Model.prototype.getErrorPosition = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return _emscripten_bind_Model_getErrorPosition_1(self, arg0);
};;

Model.prototype['getShapeType'] = Model.prototype.getShapeType = function() {
  var self = this.ptr;
  return _emscripten_bind_Model_getShapeType_0(self);
};;

Model.prototype['updateRefno'] = Model.prototype.updateRefno = function() {
  var self = this.ptr;
  _emscripten_bind_Model_updateRefno_0(self);
};;

Model.prototype['getF0Geno'] = Model.prototype.getF0Geno = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Model_getF0Geno_0(self), Geno);
};;

Model.prototype['makeGeno'] = Model.prototype.makeGeno = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Model_makeGeno_1(self, arg0);
};;

Model.prototype['setGeno'] = Model.prototype.setGeno = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Model_setGeno_1(self, arg0);
};;

Model.prototype['setValidationLevel'] = Model.prototype.setValidationLevel = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Model_setValidationLevel_1(self, arg0);
};;

Model.prototype['whereDelta'] = Model.prototype.whereDelta = function(arg0, arg1, arg2) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  if (arg2 && typeof arg2 === 'object') arg2 = arg2.ptr;
  return wrapPointer(_emscripten_bind_Model_whereDelta_3(self, arg0, arg1, arg2), Pt3D);
};;

Model.prototype['resetAllDelta'] = Model.prototype.resetAllDelta = function() {
  var self = this.ptr;
  _emscripten_bind_Model_resetAllDelta_0(self);
};;

Model.prototype['useAllDelta'] = Model.prototype.useAllDelta = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Model_useAllDelta_1(self, arg0);
};;

Model.prototype['validate'] = Model.prototype.validate = function() {
  var self = this.ptr;
  return !!(_emscripten_bind_Model_validate_0(self));
};;

Model.prototype['getPartCount'] = Model.prototype.getPartCount = function() {
  var self = this.ptr;
  return _emscripten_bind_Model_getPartCount_0(self);
};;

Model.prototype['getPart'] = Model.prototype.getPart = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return wrapPointer(_emscripten_bind_Model_getPart_1(self, arg0), Part);
};;

Model.prototype['getJointCount'] = Model.prototype.getJointCount = function() {
  var self = this.ptr;
  return _emscripten_bind_Model_getJointCount_0(self);
};;

Model.prototype['getJoint'] = Model.prototype.getJoint = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return wrapPointer(_emscripten_bind_Model_getJoint_1(self, arg0), Joint);
};;

Model.prototype['getNeuroCount'] = Model.prototype.getNeuroCount = function() {
  var self = this.ptr;
  return _emscripten_bind_Model_getNeuroCount_0(self);
};;

Model.prototype['getConnectionCount'] = Model.prototype.getConnectionCount = function() {
  var self = this.ptr;
  return _emscripten_bind_Model_getConnectionCount_0(self);
};;

Model.prototype['getNeuro'] = Model.prototype.getNeuro = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return wrapPointer(_emscripten_bind_Model_getNeuro_1(self, arg0), Neuro);
};;

Model.prototype['addPart'] = Model.prototype.addPart = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return wrapPointer(_emscripten_bind_Model_addPart_1(self, arg0), Part);
};;

Model.prototype['addJoint'] = Model.prototype.addJoint = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return wrapPointer(_emscripten_bind_Model_addJoint_1(self, arg0), Joint);
};;

Model.prototype['addNeuro'] = Model.prototype.addNeuro = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return wrapPointer(_emscripten_bind_Model_addNeuro_1(self, arg0), Neuro);
};;

Model.prototype['addNewPart'] = Model.prototype.addNewPart = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return wrapPointer(_emscripten_bind_Model_addNewPart_1(self, arg0), Part);
};;

Model.prototype['addNewJoint'] = Model.prototype.addNewJoint = function(arg0, arg1, arg2) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  if (arg2 && typeof arg2 === 'object') arg2 = arg2.ptr;
  return wrapPointer(_emscripten_bind_Model_addNewJoint_3(self, arg0, arg1, arg2), Joint);
};;

Model.prototype['addNewNeuro'] = Model.prototype.addNewNeuro = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Model_addNewNeuro_0(self), Neuro);
};;

Model.prototype['removePart'] = Model.prototype.removePart = function(arg0, arg1, arg2) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  if (arg2 && typeof arg2 === 'object') arg2 = arg2.ptr;
  if (arg1 === undefined) { _emscripten_bind_Model_removePart_1(self, arg0);  return }
  if (arg2 === undefined) { _emscripten_bind_Model_removePart_2(self, arg0, arg1);  return }
  _emscripten_bind_Model_removePart_3(self, arg0, arg1, arg2);
};;

Model.prototype['removeJoint'] = Model.prototype.removeJoint = function(arg0, arg1) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  if (arg1 === undefined) { _emscripten_bind_Model_removeJoint_1(self, arg0);  return }
  _emscripten_bind_Model_removeJoint_2(self, arg0, arg1);
};;

Model.prototype['removeNeuro'] = Model.prototype.removeNeuro = function(arg0, arg1) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  if (arg1 === undefined) { _emscripten_bind_Model_removeNeuro_1(self, arg0);  return }
  _emscripten_bind_Model_removeNeuro_2(self, arg0, arg1);
};;

Model.prototype['removeNeuros'] = Model.prototype.removeNeuros = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Model_removeNeuros_1(self, arg0);
};;

Model.prototype['findPart'] = Model.prototype.findPart = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return _emscripten_bind_Model_findPart_1(self, arg0);
};;

Model.prototype['findJoint'] = Model.prototype.findJoint = function(arg0, arg1) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  if (arg1 === undefined) { return _emscripten_bind_Model_findJoint_1(self, arg0) }
  return _emscripten_bind_Model_findJoint_2(self, arg0, arg1);
};;

Model.prototype['findNeuro'] = Model.prototype.findNeuro = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return _emscripten_bind_Model_findNeuro_1(self, arg0);
};;

Model.prototype['findNeuros'] = Model.prototype.findNeuros = function(arg0, arg1, arg2, arg3) {
  var self = this.ptr;
  ensureCache.prepare();
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  else arg1 = ensureString(arg1);
  if (arg2 && typeof arg2 === 'object') arg2 = arg2.ptr;
  if (arg3 && typeof arg3 === 'object') arg3 = arg3.ptr;
  return _emscripten_bind_Model_findNeuros_4(self, arg0, arg1, arg2, arg3);
};;

Model.prototype['findJoints'] = Model.prototype.findJoints = function(arg0, arg1) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  return _emscripten_bind_Model_findJoints_2(self, arg0, arg1);
};;

Model.prototype['move'] = Model.prototype.move = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Model_move_1(self, arg0);
};;

Model.prototype['rotate'] = Model.prototype.rotate = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Model_rotate_1(self, arg0);
};;

Model.prototype['buildUsingSolidShapeTypes'] = Model.prototype.buildUsingSolidShapeTypes = function(arg0, arg1, arg2) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  if (arg2 && typeof arg2 === 'object') arg2 = arg2.ptr;
  if (arg1 === undefined) { _emscripten_bind_Model_buildUsingSolidShapeTypes_1(self, arg0);  return }
  if (arg2 === undefined) { _emscripten_bind_Model_buildUsingSolidShapeTypes_2(self, arg0, arg1);  return }
  _emscripten_bind_Model_buildUsingSolidShapeTypes_3(self, arg0, arg1, arg2);
};;

Model.prototype['open'] = Model.prototype.open = function() {
  var self = this.ptr;
  _emscripten_bind_Model_open_0(self);
};;

Model.prototype['close'] = Model.prototype.close = function() {
  var self = this.ptr;
  _emscripten_bind_Model_close_0(self);
};;

Model.prototype['rebuild'] = Model.prototype.rebuild = function(arg0, arg1) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  if (arg1 === undefined) { _emscripten_bind_Model_rebuild_1(self, arg0);  return }
  _emscripten_bind_Model_rebuild_2(self, arg0, arg1);
};;

Model.prototype['clear'] = Model.prototype.clear = function() {
  var self = this.ptr;
  _emscripten_bind_Model_clear_0(self);
};;

  Model.prototype['get_size'] = Model.prototype.get_size = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Model_get_size_0(self), Pt3D);
};
    Model.prototype['set_size'] = Model.prototype.set_size = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Model_set_size_1(self, arg0);
};
  Model.prototype['get_vis_style'] = Model.prototype.get_vis_style = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Model_get_vis_style_0(self), SString);
};
    Model.prototype['set_vis_style'] = Model.prototype.set_vis_style = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Model_set_vis_style_1(self, arg0);
};
  Model.prototype['get_startenergy'] = Model.prototype.get_startenergy = function() {
  var self = this.ptr;
  return _emscripten_bind_Model_get_startenergy_0(self);
};
    Model.prototype['set_startenergy'] = Model.prototype.set_startenergy = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Model_set_startenergy_1(self, arg0);
};
  Model.prototype['__destroy__'] = Model.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_Model___destroy___0(self);
};
// SaveFileHelper
function SaveFileHelper() {
  this.ptr = _emscripten_bind_SaveFileHelper_SaveFileHelper_0();
  getCache(SaveFileHelper)[this.ptr] = this;
};;
SaveFileHelper.prototype = Object.create(WrapperObject.prototype);
SaveFileHelper.prototype.constructor = SaveFileHelper;
SaveFileHelper.prototype.__class__ = SaveFileHelper;
SaveFileHelper.__cache__ = {};
Module['SaveFileHelper'] = SaveFileHelper;

SaveFileHelper.prototype['Vfopen'] = SaveFileHelper.prototype.Vfopen = function(arg0, arg1) {
  var self = this.ptr;
  ensureCache.prepare();
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  else arg0 = ensureString(arg0);
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  else arg1 = ensureString(arg1);
  return wrapPointer(_emscripten_bind_SaveFileHelper_Vfopen_2(self, arg0, arg1), VirtFILE);
};;

SaveFileHelper.prototype['getMinigenotype_paramtab'] = SaveFileHelper.prototype.getMinigenotype_paramtab = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_SaveFileHelper_getMinigenotype_paramtab_0(self), ParamEntry);
};;

  SaveFileHelper.prototype['__destroy__'] = SaveFileHelper.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_SaveFileHelper___destroy___0(self);
};
// MiniGenotype
function MiniGenotype() {
  this.ptr = _emscripten_bind_MiniGenotype_MiniGenotype_0();
  getCache(MiniGenotype)[this.ptr] = this;
};;
MiniGenotype.prototype = Object.create(WrapperObject.prototype);
MiniGenotype.prototype.constructor = MiniGenotype;
MiniGenotype.prototype.__class__ = MiniGenotype;
MiniGenotype.__cache__ = {};
Module['MiniGenotype'] = MiniGenotype;

MiniGenotype.prototype['clear'] = MiniGenotype.prototype.clear = function() {
  var self = this.ptr;
  _emscripten_bind_MiniGenotype_clear_0(self);
};;

  MiniGenotype.prototype['get_name'] = MiniGenotype.prototype.get_name = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_MiniGenotype_get_name_0(self), SString);
};
    MiniGenotype.prototype['set_name'] = MiniGenotype.prototype.set_name = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_MiniGenotype_set_name_1(self, arg0);
};
  MiniGenotype.prototype['get_genotype'] = MiniGenotype.prototype.get_genotype = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_MiniGenotype_get_genotype_0(self), SString);
};
    MiniGenotype.prototype['set_genotype'] = MiniGenotype.prototype.set_genotype = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_MiniGenotype_set_genotype_1(self, arg0);
};
  MiniGenotype.prototype['get_info'] = MiniGenotype.prototype.get_info = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_MiniGenotype_get_info_0(self), SString);
};
    MiniGenotype.prototype['set_info'] = MiniGenotype.prototype.set_info = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_MiniGenotype_set_info_1(self, arg0);
};
  MiniGenotype.prototype['__destroy__'] = MiniGenotype.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_MiniGenotype___destroy___0(self);
};
// LoggerBase
function LoggerBase(arg0) {
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg0 === undefined) { this.ptr = _emscripten_bind_LoggerBase_LoggerBase_0(); getCache(LoggerBase)[this.ptr] = this;return }
  this.ptr = _emscripten_bind_LoggerBase_LoggerBase_1(arg0);
  getCache(LoggerBase)[this.ptr] = this;
};;
LoggerBase.prototype = Object.create(WrapperObject.prototype);
LoggerBase.prototype.constructor = LoggerBase;
LoggerBase.prototype.__class__ = LoggerBase;
LoggerBase.__cache__ = {};
Module['LoggerBase'] = LoggerBase;

  LoggerBase.prototype['__destroy__'] = LoggerBase.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_LoggerBase___destroy___0(self);
};
// GenoOper_f9
function GenoOper_f9() {
  this.ptr = _emscripten_bind_GenoOper_f9_GenoOper_f9_0();
  getCache(GenoOper_f9)[this.ptr] = this;
};;
GenoOper_f9.prototype = Object.create(WrapperObject.prototype);
GenoOper_f9.prototype.constructor = GenoOper_f9;
GenoOper_f9.prototype.__class__ = GenoOper_f9;
GenoOper_f9.__cache__ = {};
Module['GenoOper_f9'] = GenoOper_f9;

  GenoOper_f9.prototype['__destroy__'] = GenoOper_f9.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_GenoOper_f9___destroy___0(self);
};
// LoggerToMemory
function LoggerToMemory(arg0, arg1) {
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  if (arg1 === undefined) { this.ptr = _emscripten_bind_LoggerToMemory_LoggerToMemory_1(arg0); getCache(LoggerToMemory)[this.ptr] = this;return }
  this.ptr = _emscripten_bind_LoggerToMemory_LoggerToMemory_2(arg0, arg1);
  getCache(LoggerToMemory)[this.ptr] = this;
};;
LoggerToMemory.prototype = Object.create(WrapperObject.prototype);
LoggerToMemory.prototype.constructor = LoggerToMemory;
LoggerToMemory.prototype.__class__ = LoggerToMemory;
LoggerToMemory.__cache__ = {};
Module['LoggerToMemory'] = LoggerToMemory;

LoggerToMemory.prototype['reset'] = LoggerToMemory.prototype.reset = function() {
  var self = this.ptr;
  _emscripten_bind_LoggerToMemory_reset_0(self);
};;

LoggerToMemory.prototype['getErrorCount'] = LoggerToMemory.prototype.getErrorCount = function() {
  var self = this.ptr;
  return _emscripten_bind_LoggerToMemory_getErrorCount_0(self);
};;

LoggerToMemory.prototype['getWarningCount'] = LoggerToMemory.prototype.getWarningCount = function() {
  var self = this.ptr;
  return _emscripten_bind_LoggerToMemory_getWarningCount_0(self);
};;

LoggerToMemory.prototype['getInfoCount'] = LoggerToMemory.prototype.getInfoCount = function() {
  var self = this.ptr;
  return _emscripten_bind_LoggerToMemory_getInfoCount_0(self);
};;

LoggerToMemory.prototype['getStoredCount'] = LoggerToMemory.prototype.getStoredCount = function() {
  var self = this.ptr;
  return _emscripten_bind_LoggerToMemory_getStoredCount_0(self);
};;

LoggerToMemory.prototype['getErrorLevel'] = LoggerToMemory.prototype.getErrorLevel = function() {
  var self = this.ptr;
  return _emscripten_bind_LoggerToMemory_getErrorLevel_0(self);
};;

LoggerToMemory.prototype['getMessages'] = LoggerToMemory.prototype.getMessages = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_LoggerToMemory_getMessages_0(self), string);
};;

LoggerToMemory.prototype['getCountSummary'] = LoggerToMemory.prototype.getCountSummary = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_LoggerToMemory_getCountSummary_0(self), string);
};;

LoggerToMemory.prototype['handle'] = LoggerToMemory.prototype.handle = function(arg0, arg1, arg2, arg3) {
  var self = this.ptr;
  ensureCache.prepare();
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  else arg0 = ensureString(arg0);
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  else arg1 = ensureString(arg1);
  if (arg2 && typeof arg2 === 'object') arg2 = arg2.ptr;
  if (arg3 && typeof arg3 === 'object') arg3 = arg3.ptr;
  else arg3 = ensureString(arg3);
  _emscripten_bind_LoggerToMemory_handle_4(self, arg0, arg1, arg2, arg3);
};;

  LoggerToMemory.prototype['__destroy__'] = LoggerToMemory.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_LoggerToMemory___destroy___0(self);
};
// NNLayoutState_Model_Fred
function NNLayoutState_Model_Fred(arg0) {
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  this.ptr = _emscripten_bind_NNLayoutState_Model_Fred_NNLayoutState_Model_Fred_1(arg0);
  getCache(NNLayoutState_Model_Fred)[this.ptr] = this;
};;
NNLayoutState_Model_Fred.prototype = Object.create(WrapperObject.prototype);
NNLayoutState_Model_Fred.prototype.constructor = NNLayoutState_Model_Fred;
NNLayoutState_Model_Fred.prototype.__class__ = NNLayoutState_Model_Fred;
NNLayoutState_Model_Fred.__cache__ = {};
Module['NNLayoutState_Model_Fred'] = NNLayoutState_Model_Fred;

NNLayoutState_Model_Fred.prototype['GetElements'] = NNLayoutState_Model_Fred.prototype.GetElements = function() {
  var self = this.ptr;
  return _emscripten_bind_NNLayoutState_Model_Fred_GetElements_0(self);
};;

NNLayoutState_Model_Fred.prototype['GetValueXYWH'] = NNLayoutState_Model_Fred.prototype.GetValueXYWH = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return wrapPointer(_emscripten_bind_NNLayoutState_Model_Fred_GetValueXYWH_1(self, arg0), XYWH);
};;

NNLayoutState_Model_Fred.prototype['SetXYWH'] = NNLayoutState_Model_Fred.prototype.SetXYWH = function(arg0, arg1, arg2, arg3, arg4) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  if (arg2 && typeof arg2 === 'object') arg2 = arg2.ptr;
  if (arg3 && typeof arg3 === 'object') arg3 = arg3.ptr;
  if (arg4 && typeof arg4 === 'object') arg4 = arg4.ptr;
  _emscripten_bind_NNLayoutState_Model_Fred_SetXYWH_5(self, arg0, arg1, arg2, arg3, arg4);
};;

NNLayoutState_Model_Fred.prototype['GetInputs'] = NNLayoutState_Model_Fred.prototype.GetInputs = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return _emscripten_bind_NNLayoutState_Model_Fred_GetInputs_1(self, arg0);
};;

NNLayoutState_Model_Fred.prototype['GetLink'] = NNLayoutState_Model_Fred.prototype.GetLink = function(arg0, arg1) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  return _emscripten_bind_NNLayoutState_Model_Fred_GetLink_2(self, arg0, arg1);
};;

NNLayoutState_Model_Fred.prototype['GetLinkValueXY'] = NNLayoutState_Model_Fred.prototype.GetLinkValueXY = function(arg0, arg1) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  return wrapPointer(_emscripten_bind_NNLayoutState_Model_Fred_GetLinkValueXY_2(self, arg0, arg1), XY);
};;

  NNLayoutState_Model_Fred.prototype['__destroy__'] = NNLayoutState_Model_Fred.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_NNLayoutState_Model_Fred___destroy___0(self);
};
// MiniGenotypeLoader
function MiniGenotypeLoader(arg0) {
  ensureCache.prepare();
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  else arg0 = ensureString(arg0);
  this.ptr = _emscripten_bind_MiniGenotypeLoader_MiniGenotypeLoader_1(arg0);
  getCache(MiniGenotypeLoader)[this.ptr] = this;
};;
MiniGenotypeLoader.prototype = Object.create(WrapperObject.prototype);
MiniGenotypeLoader.prototype.constructor = MiniGenotypeLoader;
MiniGenotypeLoader.prototype.__class__ = MiniGenotypeLoader;
MiniGenotypeLoader.__cache__ = {};
Module['MiniGenotypeLoader'] = MiniGenotypeLoader;

MiniGenotypeLoader.prototype['loadNextGenotype'] = MiniGenotypeLoader.prototype.loadNextGenotype = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_MiniGenotypeLoader_loadNextGenotype_0(self), MiniGenotype);
};;

  MiniGenotypeLoader.prototype['__destroy__'] = MiniGenotypeLoader.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_MiniGenotypeLoader___destroy___0(self);
};
// VirtFILE
function VirtFILE() { throw "cannot construct a VirtFILE, no constructor in IDL" }
VirtFILE.prototype = Object.create(WrapperObject.prototype);
VirtFILE.prototype.constructor = VirtFILE;
VirtFILE.prototype.__class__ = VirtFILE;
VirtFILE.__cache__ = {};
Module['VirtFILE'] = VirtFILE;

  VirtFILE.prototype['__destroy__'] = VirtFILE.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_VirtFILE___destroy___0(self);
};
// Orient
function Orient(arg0, arg1, arg2) {
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  if (arg2 && typeof arg2 === 'object') arg2 = arg2.ptr;
  if (arg0 === undefined) { this.ptr = _emscripten_bind_Orient_Orient_0(); getCache(Orient)[this.ptr] = this;return }
  if (arg1 === undefined) { this.ptr = _emscripten_bind_Orient_Orient_1(arg0); getCache(Orient)[this.ptr] = this;return }
  if (arg2 === undefined) { this.ptr = _emscripten_bind_Orient_Orient_2(arg0, arg1); getCache(Orient)[this.ptr] = this;return }
  this.ptr = _emscripten_bind_Orient_Orient_3(arg0, arg1, arg2);
  getCache(Orient)[this.ptr] = this;
};;
Orient.prototype = Object.create(WrapperObject.prototype);
Orient.prototype.constructor = Orient;
Orient.prototype.__class__ = Orient;
Orient.__cache__ = {};
Module['Orient'] = Orient;

Orient.prototype['rotate'] = Orient.prototype.rotate = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Orient_rotate_1(self, arg0);
};;

Orient.prototype['transform'] = Orient.prototype.transform = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return wrapPointer(_emscripten_bind_Orient_transform_1(self, arg0), Pt3D);
};;

Orient.prototype['revTransform'] = Orient.prototype.revTransform = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  return wrapPointer(_emscripten_bind_Orient_revTransform_1(self, arg0), Pt3D);
};;

Orient.prototype['transformSelf'] = Orient.prototype.transformSelf = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Orient_transformSelf_1(self, arg0);
};;

Orient.prototype['revTransformSelf'] = Orient.prototype.revTransformSelf = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Orient_revTransformSelf_1(self, arg0);
};;

Orient.prototype['getAngles'] = Orient.prototype.getAngles = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Orient_getAngles_0(self), Pt3D);
};;

Orient.prototype['lookAt'] = Orient.prototype.lookAt = function(arg0, arg1) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  if (arg1 && typeof arg1 === 'object') arg1 = arg1.ptr;
  _emscripten_bind_Orient_lookAt_2(self, arg0, arg1);
};;

Orient.prototype['normalize'] = Orient.prototype.normalize = function() {
  var self = this.ptr;
  return !!(_emscripten_bind_Orient_normalize_0(self));
};;

  Orient.prototype['get_x'] = Orient.prototype.get_x = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Orient_get_x_0(self), Pt3D);
};
    Orient.prototype['set_x'] = Orient.prototype.set_x = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Orient_set_x_1(self, arg0);
};
  Orient.prototype['get_y'] = Orient.prototype.get_y = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Orient_get_y_0(self), Pt3D);
};
    Orient.prototype['set_y'] = Orient.prototype.set_y = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Orient_set_y_1(self, arg0);
};
  Orient.prototype['get_z'] = Orient.prototype.get_z = function() {
  var self = this.ptr;
  return wrapPointer(_emscripten_bind_Orient_get_z_0(self), Pt3D);
};
    Orient.prototype['set_z'] = Orient.prototype.set_z = function(arg0) {
  var self = this.ptr;
  if (arg0 && typeof arg0 === 'object') arg0 = arg0.ptr;
  _emscripten_bind_Orient_set_z_1(self, arg0);
};
  Orient.prototype['__destroy__'] = Orient.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_Orient___destroy___0(self);
};
// DefaultGenoConvManager
function DefaultGenoConvManager() {
  this.ptr = _emscripten_bind_DefaultGenoConvManager_DefaultGenoConvManager_0();
  getCache(DefaultGenoConvManager)[this.ptr] = this;
};;
DefaultGenoConvManager.prototype = Object.create(WrapperObject.prototype);
DefaultGenoConvManager.prototype.constructor = DefaultGenoConvManager;
DefaultGenoConvManager.prototype.__class__ = DefaultGenoConvManager;
DefaultGenoConvManager.__cache__ = {};
Module['DefaultGenoConvManager'] = DefaultGenoConvManager;

DefaultGenoConvManager.prototype['addDefaultConverters'] = DefaultGenoConvManager.prototype.addDefaultConverters = function() {
  var self = this.ptr;
  _emscripten_bind_DefaultGenoConvManager_addDefaultConverters_0(self);
};;

  DefaultGenoConvManager.prototype['__destroy__'] = DefaultGenoConvManager.prototype.__destroy__ = function() {
  var self = this.ptr;
  _emscripten_bind_DefaultGenoConvManager___destroy___0(self);
};
(function() {
  function setupEnums() {
    

    // Part_Shape

    Module['Part']['SHAPE_BALL_AND_STICK'] = _emscripten_enum_Part_Shape_SHAPE_BALL_AND_STICK();

    Module['Part']['SHAPE_ELLIPSOID'] = _emscripten_enum_Part_Shape_SHAPE_ELLIPSOID();

    Module['Part']['SHAPE_CUBOID'] = _emscripten_enum_Part_Shape_SHAPE_CUBOID();

    Module['Part']['SHAPE_CYLINDER'] = _emscripten_enum_Part_Shape_SHAPE_CYLINDER();

    

    // NeuroClass_Hint

    Module['NeuroClass']['Invisible'] = _emscripten_enum_NeuroClass_Hint_Invisible();

    Module['NeuroClass']['DontShowClass'] = _emscripten_enum_NeuroClass_Hint_DontShowClass();

    Module['NeuroClass']['AtFirstPart'] = _emscripten_enum_NeuroClass_Hint_AtFirstPart();

    Module['NeuroClass']['AtSecondPart'] = _emscripten_enum_NeuroClass_Hint_AtSecondPart();

    Module['NeuroClass']['EffectorClass'] = _emscripten_enum_NeuroClass_Hint_EffectorClass();

    Module['NeuroClass']['ReceptorClass'] = _emscripten_enum_NeuroClass_Hint_ReceptorClass();

    Module['NeuroClass']['V1BendMuscle'] = _emscripten_enum_NeuroClass_Hint_V1BendMuscle();

    Module['NeuroClass']['V1RotMuscle'] = _emscripten_enum_NeuroClass_Hint_V1RotMuscle();

    Module['NeuroClass']['LinearMuscle'] = _emscripten_enum_NeuroClass_Hint_LinearMuscle();

    

    // LoggerToMemory_Options2

    Module['LoggerToMemory']['StoreFirstMessage'] = _emscripten_enum_LoggerToMemory_Options2_StoreFirstMessage();

    Module['LoggerToMemory']['StoreAllMessages'] = _emscripten_enum_LoggerToMemory_Options2_StoreAllMessages();

    

    // LoggerBase_LoggerOptions

    Module['LoggerBase']['DontBlock'] = _emscripten_enum_LoggerBase_LoggerOptions_DontBlock();

    Module['LoggerBase']['CannotBeBlocked'] = _emscripten_enum_LoggerBase_LoggerOptions_CannotBeBlocked();

    Module['LoggerBase']['Enable'] = _emscripten_enum_LoggerBase_LoggerOptions_Enable();

    Module['LoggerBase']['Paused'] = _emscripten_enum_LoggerBase_LoggerOptions_Paused();

    

    // Joint_Shape

    Module['Joint']['SHAPE_BALL_AND_STICK'] = _emscripten_enum_Joint_Shape_SHAPE_BALL_AND_STICK();

    Module['Joint']['SHAPE_FIXED'] = _emscripten_enum_Joint_Shape_SHAPE_FIXED();

    

    // Model_ShapeType

    Module['Model']['SHAPE_UNKNOWN'] = _emscripten_enum_Model_ShapeType_SHAPE_UNKNOWN();

    Module['Model']['SHAPE_ILLEGAL'] = _emscripten_enum_Model_ShapeType_SHAPE_ILLEGAL();

    Module['Model']['SHAPE_BALL_AND_STICK'] = _emscripten_enum_Model_ShapeType_SHAPE_BALL_AND_STICK();

    Module['Model']['SHAPE_SOLIDS'] = _emscripten_enum_Model_ShapeType_SHAPE_SOLIDS();

    

    // ModelBuildStatus

    Module['empty'] = _emscripten_enum_ModelBuildStatus_empty();

    Module['building'] = _emscripten_enum_ModelBuildStatus_building();

    Module['invalid'] = _emscripten_enum_ModelBuildStatus_invalid();

    Module['valid'] = _emscripten_enum_ModelBuildStatus_valid();

  }
  if (Module['calledRun']) setupEnums();
  else addOnPreMain(setupEnums);
})();
