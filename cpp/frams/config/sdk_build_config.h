// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

#ifndef _SDK_BUILD_CONFIG_H_
#define _SDK_BUILD_CONFIG_H_

#include "version.h"

#ifdef __APPLE__
  #define MACOS
#elif defined __linux || defined __CYGWIN__
  #define LINUX
#endif

#define NOCREATUREOBJECT
#define EASYMAPDEBUG
#define SDK_WITHOUT_FRAMS
#define NO_BARRIER
#define NO_VMACHINE

#define NEURO_IMPL_FILES "neuroimplfiles-SDK.h"
#define NEURO_CLS_FACTORY "neurocls-f0-SDK-factory.h"
#define NEURO_CLS_LIBRARY "neurocls-f0-SDK-library.h"
#define F0_DEFASSIGN_FILE "defassign-f0-SDK.h"
#define F0_CLASSES_FILE "f0-SDK-classes.h"
#define GEN_CONFIG_FILE "gen-config-SDK.h"

#endif
