// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2016  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

#include "genotypeloader.h"

#define FIELDSTRUCT MiniGenotype
ParamEntry minigenotype_paramtab[] =
{
	{ "Genotype", 1, 29, "org", },

	{ "name", 0, 0, "Name", "s 0 40", FIELD(name), },
	{ "genotype", 0, 0, "Genotype", "s 1", FIELD(genotype), "Genes as a string of characters.", },

	{ "info_timestamp", 1, 0, "Last modified", "ft 0 -1 0", FIELD(info_timestamp), },
	{ "info_author", 1, 0, "Author name", "s 0 100", FIELD(info_author), },
	{ "info_author_ispublic", 1, 0, "Author name is public", "d 0 1 1", FIELD(info_author_ispublic), },
	{ "info_email", 1, 0, "Author email", "s 0 100", FIELD(info_email), },
	{ "info_email_ispublic", 1, 0, "Author email is public", "d 0 1 0", FIELD(info_email_ispublic), },
	{ "info", 1, 0, "Description", "s 1 1000", FIELD(info), "Short description of key features of this creature.", },
	{ "info_origin", 1, 0, "Origin", "d 0 4 0 ~Unspecified~Designed~Designed and evolved~Evolved under various conditions~Evolved using single, constant setup", FIELD(info_origin), "Declaration of how this genotype originated." },
	{ "info_how_created", 1, 0, "How created", "s 1 1000", FIELD(info_how_created), "Description of the process of designing and/or evolving this genotype." },
	{ "info_performance", 1, 0, "Performance notes", "s 1 1000", FIELD(info_performance), "Description of why this genotype is special/interesting and how it performs." }, 

	{ "energy0", 0, 0, "Starting energy", "f 0 -1 0", FIELD(energy0), },
	{ "numparts", 0, 0, "Body parts", "d", FIELD(numparts), },
	{ "numjoints", 0, 0, "Body joints", "d", FIELD(numjoints), },
	{ "numneurons", 0, 0, "Brain size", "d", FIELD(numneurons), },
	{ "numconnections", 0, 0, "Brain connections", "d", FIELD(numconnections), },

	{ "num", 0, 0, "Ordinal number", "d", FIELD(ordnumber), },
	{ "gnum", 0, 0, "Generation", "d", FIELD(generation), },

	{ "instances", 0, 0, "Instances", "d", FIELD(instances), "Copies of this genotype", },

	{ "lifespan", 0, 0, "Life span", "f", FIELD(lifespan), "Average life span", },
	{ "velocity", 0, 0, "Velocity", "f", FIELD(velocity), "Average velocity", },
	{ "distance", 0, 0, "Distance", "f", FIELD(distance), },
	{ "vertvel", 0, 0, "Vertical velocity", "f", FIELD(vertvel), },
	{ "vertpos", 0, 0, "Vertical position", "f", FIELD(vertpos), },

	{ "user1", 0, 0, "User field 1", "x", FIELD(user1), },
	{ "user2", 0, 0, "User field 2", "x", FIELD(user2), },
	{ "user3", 0, 0, "User field 3", "x", FIELD(user3), },

	{ "is_valid", 0, 0, "Validity", "d -1 1 -1", FIELD(is_valid),
	"0 = invalid genotype\n"
	"1 = valid genotype\n"
	"-1 = validity is not known." },

	{ "uid", 0, 0, "#", "s", FIELD(uid), "Unique identifier" },

	{ 0, 0, 0, },
};
#undef FIELDSTRUCT

MiniGenotypeLoader::MiniGenotypeLoader() :genotype_param(minigenotype_paramtab, &genotype_object) { init(); }
MiniGenotypeLoader::MiniGenotypeLoader(VirtFILE *f) : MultiParamLoader(f), genotype_param(minigenotype_paramtab, &genotype_object) { init(); }
MiniGenotypeLoader::MiniGenotypeLoader(const char* filename) : MultiParamLoader(filename), genotype_param(minigenotype_paramtab, &genotype_object) { init(); }

void MiniGenotypeLoader::init()
{
	addObject(&genotype_param);
	breakOn(MultiParamLoader::OnError + MultiParamLoader::AfterObject);
}

MiniGenotype* MiniGenotypeLoader::loadNextGenotype()
{
	genotype_object.clear();
	if ((go() == AfterObject) && (getObject().matchesInterfaceName(&genotype_param)))
		return &genotype_object;
	else
		return 0;
}
