// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

#ifndef _PRINTCONVMAP_H_
#define _PRINTCONVMAP_H_

#include <frams/util/sstring.h>

class MultiMap;

void printConvMap(const SString& gen1,const SString& gen2,const MultiMap& map);
void printModelMap(const SString& gen1,const MultiMap& map);

#endif
