// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

#include "printconvmap.h"
#include <stdio.h>
#include <frams/util/multimap.h>

#define GEN1MAX 15

void printN(const char* t,int maxlen)
{
while(maxlen-- > 0)
	if (*t) putchar(*(t++));
	else putchar(' ');
}

void printN(char t,int maxlen)
{
while(maxlen-- > 0) putchar(t);
}

void printmapping(const char* gen1, int len1,const MultiRange &mr,const char* gen2,int len2)
{
printN(' ',GEN1MAX-len1);
printN(gen1,len1);
printf(" : ");
int i;
for(i=0;i<len2;i++)
	if (mr.contains(i))
		putchar(gen2[i]);
	else
		putchar('.');
putchar('\n');
}

void stripstring(SString &str)
{
char *t=str.directWrite();
for(;*t;t++)
	if (strchr("\n\r\t",*t)) *t=' ';
}

void printConvMap(const SString& gen1,const SString& gen2,const MultiMap& map)
{
int y,y2,len1;
int id=0;
if (map.isEmpty())
	{
	printf("{ empty }\n");
	return;
	}
len1=gen1.len();
SString g1=gen1;
stripstring(g1);
SString g2=gen2;
stripstring(g2);
const char* g=g1.c_str();
y=0;
MultiRange *mr;
MultiRange emptyrange;
printN(' ',GEN1MAX);
printf("   %s\n",g2.c_str());
int begin=map.getBegin();
int end=map.getEnd();
while(y<len1)
	{
	if (y<begin)
		{
		mr=&emptyrange;
		y2=begin;
		}
	else if (y>end)
		{
		mr=&emptyrange;
		y2=len1;
		}
	else	{
		id=map.findMappingId(y);
		mr=&map.getMapping(id)->to;
		y2=map.getMapping(id+1)->begin;
		}
	if ((y2-y) > GEN1MAX) y2=y+GEN1MAX;
	if (y2>(y+len1)) y2=y+len1;
	printmapping(g+y,y2-y,*mr,g2.c_str(),g2.len());
	y=y2;
	}
}

void printModelMap(const SString& gen1,const MultiMap& map)
{
SString g2("012345678901234567890123456789");
printN(' ',GEN1MAX);
printf("   Parts     Joints    Neurons\n");
printConvMap(gen1,g2,map);
}
