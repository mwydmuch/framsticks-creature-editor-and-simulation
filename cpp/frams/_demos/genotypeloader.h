// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2016  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

#ifndef _GENOTYPELOADER_H_
#define _GENOTYPELOADER_H_

#include <frams/util/sstring.h>
#include <frams/param/multiparamload.h>

/** Defines the association between "org:" object (found in genotype files)
	and the MiniGenotype fields. MiniGenotypeLoader uses this definition
	but you can also use it to make MultiParamLoader load genotype
	*/
extern ParamEntry minigenotype_paramtab[];

/** Helper class, mostly useful with MultiParamLoader
	or its specialized version: MiniGenotypeLoader.
	MiniGenotype stores the subset of Genotype fields (the ones normally saved in .gen files)
	*/
class MiniGenotype
{
public:
	SString name, genotype, info, uid;
	double info_timestamp;
	SString info_author, info_email;
	paInt info_author_ispublic, info_email_ispublic, info_origin;
	SString info_how_created, info_performance;
	double energy0, lifespan, velocity, distance, vertvel, vertpos;
	paInt numparts, numjoints, numneurons, numconnections, ordnumber, generation, instances, is_valid;
	ExtValue user1, user2, user3;
	void clear() { Param p(minigenotype_paramtab, this); p.setDefault(); }
};

/** In most simple cases this is the class you would use to load a series of genotypes from
	the Framsticks genotype file.

	Usage pattern: (see loader_test_geno.cpp for the working code)

	1.Initialize

	2.while(genotype=loadNextGenotype()) doSomethingWith(genotype);

	3.Done!

	MiniGenotypeLoader is simply the MultiParamLoader configured to load one kind of data: "org:" objects.
	The instance of this class can also be reconfigured to recognize more objects by using MultiParamLoader
	methods, or you can use it as a guide for creating your own specialized class.
	*/
class MiniGenotypeLoader : public MultiParamLoader
{
	MiniGenotype genotype_object;
	Param genotype_param;
	bool initialized;
	void init();
public:
	MiniGenotypeLoader();
	MiniGenotypeLoader(VirtFILE *f);
	MiniGenotypeLoader(const char* filename);

	/** @returns genotype object if one was loaded or NULL otherwise.

		Returned MiniGenotype pointer always references the the same object (MiniGenotypeLoader::genotype_object)
		which means you may need to copy the data from it before calling loadNextGenotype() again.
		In the default configuration (simple MiniGenotypeLoader) NULL is always final and should be used
		to finish processing.

		If the loader is configured to load other objects or stop on other conditions, NULL will also mean
		every condition other than "GenotypeLoaded". In such cases you need MultiParamLoader::getStatus(),
		MultiParamLoader::finished() and other methods to determine the real cause of NULL.
		*/
	MiniGenotype* loadNextGenotype();
};

#endif
