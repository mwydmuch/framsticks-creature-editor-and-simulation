// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

#include <ctype.h>
#include <frams/genetics/defgenoconv.h>
#include <frams/model/model.h>
#include <frams/util/multimap.h>
#include <common/virtfile/stdiofile.h>
#include "printconvmap.h"
#include <common/loggers/loggertostdout.h>

/**
 @file
 Sample code: Genotype converter class
*/

/// Sample Geno converter not using Model class.
/// (this converter generates the same output for each input).
/// such a converter is responsible for doing valid
/// f0 (or other format) output and storing temporary data
class GenoConv_Test: public GenoConverter
{
public:
GenoConv_Test()
	{
	name="Test Converter";
	in_format='x';
	}
SString convert(SString &i,MultiMap *map) {return SString("after conversion...");}
~GenoConv_Test() {}
};

/// Sample Geno converter using Model class.
/// (this converter generates the same output for each input).
class GenoConv_Test2: public GenoConverter
{
public:
GenoConv_Test2()
	{
	name="Test Converter #2";
	in_format='y';
	}
SString convert(SString &i,MultiMap *map)
{
Model mod;
mod.open();
mod.singleStepBuild("p:");
mod.singleStepBuild("p:");
mod.singleStepBuild("j:0,1");
mod.getPart(1)->p=Pt3D(0,0.2,-1);
mod.close();
return mod.getGeno().getGenes();
}
~GenoConv_Test2() {}
};

/// Sample Geno converter supporting conversion mapping.
/// the conversion is very simple: any sequence of <digit><character>
/// (but not inside neurons) is replaced by the repeated sequence of the character
class GenoConv_Test3: public GenoConverter
{
public:
GenoConv_Test3()
	{
	name="Test Converter #3";
	in_format='z';
	out_format='1';
	mapsupport=1;
	}
SString convert(SString &in,MultiMap *map);
~GenoConv_Test3() {}
};

/** main converting routine - most important: direct conversion map example */
SString GenoConv_Test3::convert(SString &in,MultiMap *map)
{
SString dst;
const char* src=in.c_str();
const char* t;
int insideneuron=0;
int n;
for(t=src;*t;t++)
	{
	if (insideneuron&&*t==']') insideneuron=0;
	if (*t=='[') insideneuron=1;
	if ((!insideneuron)&&isdigit(*t)&&t[1])
		{ // special sequence detected!
		n=*t-'0';
		t++; // *t will be repeated 'n' times
		for (int i=0;i<n;i++)
			dst+=*t;
		if (map) // fill in the map only if requested
			map->add(t-src, t-src, dst.len()-n, dst.len()-1);
		// meaning: source character (t-src) becomes (dst.len()-n ... dst.len()-1)
		}
	else
		{
		dst+=*t;
		if (map)
			map->add(t-src, t-src, dst.len()-1, dst.len()-1);
		// meaning: map single to single character: (t-src) into (dst.len()-1)
		}
	}
return dst;
}


///////////////////////////////////////////////

void printGen(Geno &g)
{
printf("Genotype:\n%s\nFormat: %c\nValid: %s\nComment: %s\n",
	g.getGenes().c_str(),g.getFormat(),g.isValid()?"yes":"no",g.getComment().c_str());
}

int main(int argc,char *argv[])
{
LoggerToStdout messages_to_stdout(LoggerBase::Enable);

DefaultGenoConvManager gcm;
gcm.addDefaultConverters();
gcm.addConverter(new GenoConv_Test());
gcm.addConverter(new GenoConv_Test2());
gcm.addConverter(new GenoConv_Test3());
Geno::useConverters(&gcm);

Geno::Validators validators;
ModelGenoValidator model_validator;
validators+=&model_validator;
Geno::useValidators(&validators);

SString src;
if (argc>1)
	{
	src=argv[1];
	if (src=="-")
		{
		StdioFILEDontClose in(stdin);
		src="";
		loadSString(&in,src,false);
		}
	}
else
	src="X";
char dst=(argc>2)?*argv[2]:'0';

printf("*** source genotype:\n");
Geno g1(src);
printGen(g1);
MultiMap m;
Geno g2=g1.getConverted(dst,&m);
printf("*** converted to f%c:\n",dst);
printGen(g2);
if (m.isEmpty())
	printf("(conversion map not available)\n");
else
	{
	printf("conversion map:\n");
	m.print();
	printConvMap(g1.getGenes(),g2.getGenes(),m);
	printf("reverse conversion map:\n");
	MultiMap rm;
	rm.addReversed(m);
	rm.print();
	printConvMap(g2.getGenes(),g1.getGenes(),rm);
	}

Model mod1(g1,1);
printf("\nmodel map for f%c genotype:\n",g1.getFormat());
printModelMap(g1.getGenes(),mod1.getMap());
mod1.getMap().print();
Model mod2(g2,1);
printf("\nmodel map for f%c genotype:\n",g2.getFormat());
printModelMap(g2.getGenes(),mod2.getMap());
mod2.getMap().print();
return 0;
}
