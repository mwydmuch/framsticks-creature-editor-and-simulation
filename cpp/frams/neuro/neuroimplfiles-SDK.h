// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

#include "impl/neuroimpl-simple.h"
#include "impl/neuroimpl-channels.h"
#include "impl/neuroimpl-fuzzy.h"
#ifdef SDK_WITHOUT_FRAMS
#include "impl/neuroimpl-body-sdk.h"
#else
#include "impl/neuroimpl-test.h"
#include "impl/neuroimpl-body.h"
#include "impl/neuroimpl-vectoreye.h"
#include "impl/neuroimpl-vmot.hpp"
#endif

