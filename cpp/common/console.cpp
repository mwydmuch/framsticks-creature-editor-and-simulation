#include "console.h"
#ifdef _WIN32

#include <windows.h>
#include "Convert.h"

#else

#include <stdio.h>
#include <termios.h>

bool guiStdoutIsConsole()
{
	static int in=-1;
	if (in<0)
	{
		struct termios t;
		in=(tcgetattr(1,&t)==0);
	}
	return in;
}
#endif


void guiSetConsoleTitle(const char* utf8title) //platform-dependent implementations
{
#ifdef _WIN32
	SetConsoleTitleW(Convert::utf8ToUtf16(utf8title).c_str());
#else
	if (guiStdoutIsConsole())
	{
		printf("\033]0;%s\007",utf8title);
		fflush(stdout);
	}
#endif
}

ConsoleColorMode console_color_mode = ConsoleColorNone;
