#ifndef _CONSOLE_H_
#define _CONSOLE_H_
#ifndef _WIN32
bool guiStdoutIsConsole();
#endif
void guiSetConsoleTitle(const char* utf8title);
enum ConsoleColorMode { ConsoleColorNone, ConsoleColorANSIDefault };
extern ConsoleColorMode console_color_mode;
#endif
