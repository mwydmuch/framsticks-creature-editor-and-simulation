// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

#ifndef _NONSTD_MATH_H_
#define _NONSTD_MATH_H_


#ifdef _MSC_VER
 #define _USE_MATH_DEFINES //after this is defined, the next #include <math.h> or <cmath> will define M_PI etc.
 #include <math.h> //w vc2008 dzia�a�o tu <cmath>, ale w vc2010 juz nie bo "co�" (jaki� inny .h stl'a?) includuje wcze�niej <cmath> bez _USE_MATH_DEFINES, a <cmath> includuje <math.h> (ale tylko raz bo ma "include guards" jak kazdy .h)
 #include <float.h>
 //#define isnan(x) _isnan(x) //since 2014 we use std::isnan()
 #define finite(x) _finite(x)
#else //m.in. __BORLANDC__
 #include <math.h>
#endif

#if (defined __BORLANDC__ || (_MSC_VER <= 1700)) & (!defined EMSCRIPTEN)
  double round(double val);  //http://stackoverflow.com/questions/2170385/c-math-functions
#endif


//random number generator:
#include "random.h"
RandomGenerator& rndGetInstance();

#define rnd01 (rndGetInstance().getDouble())
#define rnd0N(limit) (rndGetInstance().getDouble()*(limit))
#define randomN(limit) ((unsigned int)(rndGetInstance().getDouble()*(limit)))
#define setRandomSeed(seed) rndGetInstance().setSeed(seed)
#define setRandomRandomSeed() rndGetInstance().randomize()


//floating point specific numbers
#include "stdlib.h"

#ifdef __BORLANDC__
	#include <float.h>
	#define isnan(x) _isnan(x) //http://stackoverflow.com/questions/570669/checking-if-a-double-or-float-is-nan-in-c
	#define finite(x) _finite(x)
#endif

#ifdef LINUX
  #define _isnan(a) isnan(a)
#endif

#ifdef IPHONE
	#define finite(x) (!isinf(x))
  #define _isnan(a) isnan(a)
#endif


#if defined SHP
 //#define __assert_func(a,b,c,d) 0 //Currently, we are sorry to inform you that assert() is not yet supported. We have considered your request for internal discussion. Na szcz�cie jest w�asna (byle by by�a, bo i tak zak�adamy ze assert ktore przeciez dziala tylko w trybie debug nie jest potrzebne na bada) implementacja w "bada-assert.cpp"
 #define isnan(x) false //isnan() sie nie linkuje
 #define finite(x) true //j.w.
 //#include <cstdlib> //RAND_MAX defined incorrectly
 //#ifdef BADA_SIMULATOR //...but only in simulator libs
 // #undef RAND_MAX
 // #define RAND_MAX 32768 //...this is the actual value used by rand()
 //#endif
#endif


//handling floating point exceptions
void fpExceptInit(); //call once, before ...Enable/Disable
void fpExceptEnable();
void fpExceptDisable();

#endif

