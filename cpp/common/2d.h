// This file is a part of Framsticks SDK.  http://www.framsticks.com/
// Copyright (C) 1999-2015  Maciej Komosinski and Szymon Ulatowski.
// See LICENSE.txt for details.

#ifndef _2D_H_
#define _2D_H_

#include "nonstd_stl.h"
#include <math.h>

//unifikacja starych GUIXY i Pt2D
template <typename T> class XY
{
public:
T x,y;
XY() {}
XY(T _x,T _y):x(_x),y(_y) {}
template <typename Q> XY(const Q& other):x(other.x),y(other.y) {}
template <typename Q> const XY& operator=(const Q& other) {x=other.x; y=other.y; return *this;}
XY operator+(const XY&p) const  {return XY(x+p.x,y+p.y);}
XY operator-(const XY&p) const {return XY(x-p.x,y-p.y);}
XY operator+=(const XY&p) {x+=p.x; y+=p.y; return *this;}
XY operator-=(const XY&p) {x-=p.x; y-=p.y; return *this;}
XY operator-() const {return XY(-x,-y);}
XY operator*=(T q) {x*=q; y*=q; return *this;}
XY operator/=(T q) {x/=q; y/=q; return *this;}
XY operator/(T q) {return XY(x/q,y/q);}
XY operator*(T q) const  {return XY(q*x,q*y);}
void set(T _x,T _y) {x=_x; y=_y;}
void add(T _x,T _y) {x+=_x; y+=_y;}
void sub(T _x,T _y) {x-=_x; y-=_y;}
bool operator==(const XY& p) const {return (fabs(double(x-p.x))<1e-20)&&(fabs(double(y-p.y))<1e-20);}
T distanceTo(const XY& p) const {return sqrt(double((p.x-x)*(p.x-x)+(p.y-y)*(p.y-y)));}
T magnitude() const {return sqrt(x*x+y*y);}
T length() const {return sqrt(x*x+y*y);}
T lengthSq() const { return x*x + y*y; }
T dotProduct(const XY& v) const {return x*v.x + y*v.y;}
T crossProduct(const XY& v) const {return x*v.y - y*v.x;}
void normalize() { operator/=(length()); } // length becomes 1
static XY average(const XY& v1,const XY& v2) { return XY((v1.x+v2.x)*0.5,(v1.y+v2.y)*0.5); }
double getDirection() const {return atan2(y,x);}
static XY interpolate(const XY& v1, const XY& v2,double t) {return v1+(v2-v1)*t;}
XY toInt() const {return XY(int(x),int(y));}
static const XY& zero() {static XY t(0,0); return t;}
static const XY& one() {static XY t(1,1); return t;}
};

template <typename T> XY<T> xymin(const XY<T>& a, const XY<T>& b) {return XY<T>(min(a.x,b.x),min(a.y,b.y));}
template <typename T> XY<T> xymax(const XY<T>& a, const XY<T>& b) {return XY<T>(max(a.x,b.x),max(a.y,b.y));}

typedef XY<int> IntXY;

template <typename T>
class XYRect
{
public:
XY<T> p,size;
XYRect() {}
XYRect(const XY<T>& p1,const XY<T>& s):p(p1),size(s) {}
XYRect(T _x,T _y,T _w,T _h):p(_x,_y),size(_w,_h) {}
static XYRect<T> centeredAt(const XY<T>& p,XY<T> s) {return XYRect<T>(p-s*0.5,s);}

bool isEmpty() const {return (size.x<0)||(size.y<0);}
XYRect toInt() const {return XYRect(int(p.x),int(p.y),int(p.x+size.x)-int(p.x),int(p.y+size.y)-int(p.y));}
bool operator==(const XYRect& r) const {return (p==r.p) && (size==r.size);}

bool intersects(const XYRect& r) const
{
if (r.p.x >= (p.x+size.x)) return false;
if (r.p.y >= (p.y+size.y)) return false;
if ((r.p.x+r.size.x) <= p.x) return false;
if ((r.p.y+r.size.y) <= p.y) return false;
return true;
}

bool contains(const XY<T>& n) const
{
if (n.x<p.x) return false;
if (n.x>(p.x+size.x)) return false;
if (n.y<p.y) return false;
if (n.y>(p.y+size.y)) return false;
return true;
}

void add(const XY<T>& n)
{
if (n.x<p.x) {size.x+=p.x-n.x; p.x=n.x;}
else if (n.x>(p.x+size.x)) size.x=n.x-p.x;
if (n.y<p.y) {size.y+=p.y-n.y; p.y=n.y;}
else if (n.y>(p.y+size.y)) size.y=n.y-p.y;
}

void extend(const XY<T>& border_size)
{
size+=border_size*2; p-=border_size;
}

XYRect intersection(const XYRect& r) const
{
XYRect i;
XY<T> p2=p+size;
XY<T> rp2=r.p+r.size;
i.p.x=max(p.x,r.p.x);
i.p.y=max(p.y,r.p.y);
i.size.x=min(p2.x,rp2.x)-i.p.x;
i.size.y=min(p2.y,rp2.y)-i.p.y;
return i;
}

T distanceTo(const XY<T>& n) const
{
XY<T> tp=n;
if (n.x<p.x) tp.x=p.x; else if (n.x>=(p.x+size.x)) tp.x=p.x+size.x;
if (n.y<p.y) tp.y=p.y; else if (n.y>=(p.y+size.y)) tp.y=p.y+size.y;
 return tp.distanceTo(n);
}

static const XYRect& zero() {static XYRect t(0,0,0,0); return t;}
static const XYRect& one() {static XYRect t(0,0,1,1); return t;}
};


#endif
