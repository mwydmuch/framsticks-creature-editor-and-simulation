# Framsticks Creature Editor and Simulation #

This project extends [Framsticks Creature Editor](https://bitbucket.org/mack0/framsticks-creature-editor).
It adds basic physics simulation of creatures, mutation and crossing-over of creatures' genotypes. 

## Original README.md:
This application lets you visually edit [Framsticks](http://www.framsticks.com/) creatures. It is based on [Framsticks SDK](http://www.framsticks.com/sdk).

The editor is released under the Creative Commons [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/legalcode) license.
		
Contact [Framsticks support](mailto:support@framsticks.com) with questions about this project.